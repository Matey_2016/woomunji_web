$(document).ready(function(){
	$(".titBar a").on("click", function(){
		$(this).toggleClass("barBtnOn");
		if($(this).hasClass("barBtnOn")){
			$(".titBar").stop().animate({
				height:"311px"
			},300);
		}else{
			$(".titBar").stop().animate({
				height:"40px"
			},300);
		}
	});

	$(".btnConMore").on("click", function(){
		$(".mainCon").css("height", "1998px");
	});

	$("header .headerBox").mouseenter(function(){
		$("#gnb").stop().slideDown(200);
	})
	$("header, #gnb").mouseleave(function(){
		$("#gnb").stop().slideUp(200);
	});

	$(".login").on("click", function(){
		$(".loginBox").slideDown();
	});
	$(".close").on("click", function(){
		$(".loginBox").slideUp();
	});
});