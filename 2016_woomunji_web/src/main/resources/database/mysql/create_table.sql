CREATE TABLE `tb_banner` (
  `banner_id` int(9) unsigned NOT NULL AUTO_INCREMENT COMMENT '배너아이디',
  `title` varchar(256) DEFAULT NULL COMMENT '배너제목',
  `start_dt` datetime DEFAULT NULL COMMENT '배너표시 시작일',
  `end_dt` datetime DEFAULT NULL COMMENT '배너표시 종료일',
  `link_url` varchar(128) DEFAULT NULL COMMENT '링크경로',
  `target` varchar(1) DEFAULT NULL COMMENT '링크타겟(O:opener, B:blank)',
  `file_nm` varchar(96) DEFAULT NULL COMMENT '파일명',
  `system_file_nm` varchar(48) DEFAULT NULL COMMENT '시스템 파일명',
  `file_path` varchar(128) DEFAULT NULL COMMENT '파일경로',
  `position` varchar(1) DEFAULT NULL COMMENT '배너위치',
  `summary` varchar(2000) DEFAULT NULL COMMENT '요약',
  `use_yn` varchar(1) DEFAULT NULL COMMENT '사용여부',
  `reg_dt` datetime DEFAULT NULL COMMENT '등록일',
  `reg_id` varchar(12) DEFAULT NULL COMMENT '등록자 아이디',
  `ip` varchar(15) DEFAULT NULL COMMENT '등록아이피',
  PRIMARY KEY (`banner_id`),
  UNIQUE KEY `banner_id` (`banner_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='배너정보';



CREATE TABLE `tb_board` (
  `board_id` int(9) unsigned NOT NULL AUTO_INCREMENT COMMENT '게시판아이디',
  `board_nm` varchar(256) DEFAULT NULL COMMENT '게시판 명',
  `type_cd` varchar(10) DEFAULT NULL COMMENT '게시판 타입',
  `file_yn` varchar(1) DEFAULT NULL COMMENT '파일첨부 여부',
  `reply_yn` varchar(1) DEFAULT NULL COMMENT '답변 여부',
  `notice_yn` varchar(1) DEFAULT NULL COMMENT '대표공지 여부',
  `comment_yn` varchar(1) DEFAULT NULL COMMENT '댓글 여부',
  `pass_yn` varchar(1) DEFAULT NULL COMMENT '비밀글 사용여부',
  `language_cd` varchar(10) DEFAULT NULL COMMENT '언어코드',
  `new_day` int(3) DEFAULT NULL COMMENT '신규글 표시기간',
  `prenext_yn` varchar(1) DEFAULT NULL COMMENT '이전/다음글 사용여부',
  `thumb_yn` varchar(1) DEFAULT NULL COMMENT '섬네일 여부',
  `movie_yn` varchar(1) DEFAULT NULL COMMENT '동영상 여부',
  `summary_yn` varchar(1) DEFAULT NULL COMMENT '요약글 사용여부',
  `exlink_yn` varchar(1) DEFAULT NULL COMMENT '외부링크 사용여부',
  `urldeny_yn` varchar(1) DEFAULT NULL COMMENT 'url조작금지 여부',
  `use_code1` varchar(10) DEFAULT NULL COMMENT '구분코드1',
  `use_code2` varchar(10) DEFAULT NULL COMMENT '구분코드2',
  `code_relation` varchar(1) DEFAULT NULL COMMENT '구분코드 상관여부',
  `file_cnt` int(3) DEFAULT '1' COMMENT '첨부파일 갯수',
  `page_size` int(9) DEFAULT '10' COMMENT '목록갯수',
  `use_yn` varchar(1) DEFAULT NULL COMMENT '사용여부',
  `reg_dt` datetime DEFAULT NULL COMMENT '등록일',
  `reg_id` varchar(12) DEFAULT NULL COMMENT '등록자 아이디',
  `mod_dt` datetime DEFAULT NULL COMMENT '수정일',
  `mod_id` varchar(12) DEFAULT NULL COMMENT '수정자 아이디',
  `ip` varchar(15) DEFAULT NULL COMMENT '등록아이피',
  PRIMARY KEY (`board_id`),
  UNIQUE KEY `board_id` (`board_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='게시판 정보';


CREATE TABLE `tb_code` (
  `code_id` varchar(10) NOT NULL COMMENT '공통코드',
  `high_id` varchar(10) NOT NULL COMMENT '상위코드',
  `code_desc` varchar(256) DEFAULT NULL COMMENT '코드 상세 설명',
  `code_etc` varchar(256) DEFAULT NULL COMMENT '비고',
  `code_nm` varchar(96) DEFAULT NULL COMMENT '코드명',
  `use_yn` varchar(1) DEFAULT 'y' COMMENT '코드 사용 여부',
  PRIMARY KEY (`code_id`,`high_id`),
  KEY `high_id` (`high_id`,`use_yn`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='공통코드';


CREATE TABLE `tb_comment` (
  `comment_id` int(9) unsigned NOT NULL AUTO_INCREMENT COMMENT '댓글 아이디',
  `post_id` int(9) NOT NULL DEFAULT '0' COMMENT '게시물 아이디',
  `comment` text COMMENT '댓글내용',
  `passwd` varchar(48) DEFAULT NULL COMMENT '비밀번호',
  `user_id` varchar(12) DEFAULT NULL COMMENT '등록자 아이디',
  `user_nm` varchar(48) DEFAULT NULL COMMENT '등록자명',
  `del_yn` char(1) DEFAULT 'n' COMMENT '삭제여부',
  `reg_dt` datetime DEFAULT NULL COMMENT '등록일',
  PRIMARY KEY (`comment_id`),
  UNIQUE KEY `comment_id` (`comment_id`),
  UNIQUE KEY `comment_id_2` (`comment_id`),
  KEY `board_comment_ix01` (`post_id`,`del_yn`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='게시물 댓글정보';


CREATE TABLE `tb_file` (
  `file_id` int(9) unsigned NOT NULL AUTO_INCREMENT COMMENT '파일아이디',
  `parent_id` int(9) NOT NULL COMMENT '상위아이디',
  `file_flag` varchar(20) NOT NULL COMMENT '파일구분',
  `file_nm` varchar(96) DEFAULT NULL COMMENT '파일명',
  `system_file_nm` varchar(48) DEFAULT NULL COMMENT '시스템 파일명',
  `file_path` varchar(24) DEFAULT NULL COMMENT '파일경로',
  `file_type` varchar(8) DEFAULT NULL COMMENT '파일타입',
  `file_size` varchar(12) DEFAULT NULL COMMENT '파일크기',
  PRIMARY KEY (`file_id`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT=' 첨부파일 정보';



CREATE TABLE `tb_post` (
  `post_id` int(9) NOT NULL AUTO_INCREMENT COMMENT '게시물아이디',
  `board_id` int(9) NOT NULL DEFAULT '0' COMMENT '게시판아이디',
  `title` varchar(255) DEFAULT NULL COMMENT '제목',
  `user_id` varchar(12) DEFAULT NULL COMMENT '등록자 아이디',
  `user_nm` varchar(48) DEFAULT NULL COMMENT '등록자명',
  `passwd` varchar(48) DEFAULT NULL COMMENT '비밀번호',
  `sumry` text COMMENT '요약글',
  `contents` text COMMENT '글내용',
  `hit` int(6) DEFAULT '0' COMMENT '조회수',
  `parent_post_id` int(9) DEFAULT NULL COMMENT '상위글 아이디',
  `notice_yn` char(1) DEFAULT NULL COMMENT '대표공지여부',
  `email` varchar(255) DEFAULT NULL COMMENT '등록자 이메일',
  `movie_url` varchar(255) DEFAULT NULL COMMENT '동영상/외부링크 주소',
  `link_yn` char(1) DEFAULT NULL COMMENT '외부링크 여부',
  `etc_cd1` varchar(10) DEFAULT NULL COMMENT '구분코드1',
  `etc_cd2` varchar(10) DEFAULT NULL COMMENT '구분코드2',
  `del_yn` char(1) DEFAULT 'n' COMMENT '삭제여부',
  `reg_dt` datetime DEFAULT NULL COMMENT '등록일',
  `reg_id` varchar(12) DEFAULT NULL COMMENT '등록자 아이디',
  `mod_dt` datetime DEFAULT NULL COMMENT '수정일',
  `mod_id` varchar(12) DEFAULT NULL COMMENT '수정자 아이디',
  `mod_flag` varchar(1) DEFAULT NULL COMMENT '수정구분(a:관리자,u:사용자)',
  `ip` varchar(15) DEFAULT NULL COMMENT '등록아이피',
  PRIMARY KEY (`post_id`)
) ENGINE=MyISAM AUTO_INCREMENT=72 DEFAULT CHARSET=utf8 COMMENT='게시물 정보';



CREATE TABLE `tb_staff` (
  `STAFF_ID` varchar(12) NOT NULL COMMENT '관리자 아이디',
  `STAFF_NM` varchar(20) DEFAULT NULL COMMENT '관리자 이름',
  `PASSWD` varchar(32) DEFAULT NULL COMMENT '관리자 비밀번호',
  `DEPT_CD` varchar(10) DEFAULT NULL COMMENT '부서코드',
  `PHONE` varchar(20) DEFAULT NULL COMMENT '연락처',
  `MOBILE` varchar(20) DEFAULT NULL COMMENT '휴대폰',
  `EMAIL` varchar(128) DEFAULT NULL COMMENT '이메일',
  `STAFF_LVL` varchar(10) DEFAULT NULL COMMENT '관리자레벨',
  `USE_YN` char(1) DEFAULT NULL COMMENT '사용여부',
  `REG_DT` datetime DEFAULT NULL COMMENT '등록일',
  `REG_ID` varchar(12) DEFAULT NULL COMMENT '등록자 아이디',
  `MOD_DT` datetime DEFAULT NULL COMMENT '수정일',
  `MOD_ID` varchar(12) DEFAULT NULL COMMENT '수정자 아이디',
  `IP` varchar(15) DEFAULT NULL COMMENT '등록 아이피',
  PRIMARY KEY (`STAFF_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='관리자정보';


CREATE TABLE `tb_staff_menu` (
  `MENU_ID` varchar(36) NOT NULL COMMENT '메뉴아이디',
  `MENU_NM` varchar(96) DEFAULT NULL COMMENT '메뉴명',
  `PARENT_CD` varchar(10) DEFAULT NULL COMMENT '상위 메뉴 아이디',
  `PARENT_CD_NM` varchar(30) DEFAULT NULL COMMENT '상위 메뉴명',
  `MENU_URL` varchar(256) DEFAULT NULL COMMENT '메뉴 url',
  `USE_YN` varchar(1) DEFAULT NULL COMMENT '사용 여부',
  `BOARD_ID` int(11) DEFAULT NULL COMMENT '게시판 아이디',
  PRIMARY KEY (`MENU_ID`),
  KEY `PARENT_CD` (`PARENT_CD`,`USE_YN`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='관리자 메뉴';



CREATE TABLE `tb_staff_menu_auth` (
  `staff_id` varchar(12) NOT NULL COMMENT '관리자 아이디',
  `menu_id` varchar(36) NOT NULL COMMENT '메뉴 아이디',
  `c_auth` varchar(1) DEFAULT NULL COMMENT '등록권한',
  `r_auth` varchar(1) DEFAULT NULL COMMENT '읽기권한',
  `u_auth` varchar(1) DEFAULT NULL COMMENT '수정권한',
  `d_auth` varchar(1) DEFAULT NULL COMMENT '삭제권한',
  PRIMARY KEY (`staff_id`,`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='관리자 권한정보';


CREATE TABLE `tb_user` (
  `user_id` varchar(12) NOT NULL COMMENT '회원 아이디',
  `user_nm` varchar(20) DEFAULT NULL COMMENT '회원명',
  `passwd` varchar(32) DEFAULT NULL COMMENT '회원 비밀번호',
  `email` varchar(128) DEFAULT NULL COMMENT '이메일주소',
  `phone` varchar(20) DEFAULT NULL COMMENT '연락처',
  `mobile` varchar(20) DEFAULT NULL COMMENT '휴대폰',
  `post` varchar(7) DEFAULT NULL COMMENT '우편번호',
  `addr` varchar(128) DEFAULT NULL COMMENT '주소',
  `addr_desc` varchar(128) DEFAULT NULL COMMENT '상세주소',
  `sms_yn` varchar(1) DEFAULT NULL COMMENT 'sms 수신여부',
  `email_yn` varchar(1) DEFAULT NULL COMMENT '이메일 수신여부',
  `login_dt` datetime DEFAULT NULL COMMENT '최종로그인 시간',
  `del_yn` varchar(1) DEFAULT NULL COMMENT '삭제/탈퇴여부',
  `reg_dt` datetime DEFAULT NULL COMMENT '회원가입일',
  `leave_dt` datetime DEFAULT NULL COMMENT '회원탈퇴일',
  `ip` varchar(15) DEFAULT NULL COMMENT '로그인 아이피',
  PRIMARY KEY (`user_id`),
  KEY `user_ix01` (`del_yn`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='회원정보';



