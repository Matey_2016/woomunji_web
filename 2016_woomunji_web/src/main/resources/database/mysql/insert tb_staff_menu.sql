INSERT INTO 
  (MENU_ID, MENU_NM, PARENT_CD, PARENT_CD_NM, MENU_URL, USE_YN, BOARD_ID)
VALUES
  ('9a9362a5-5233-433d-8208-334735bc3256', '회원관리', 'M1000', '', '/operation/member/memberList.do', 'N', 0);

INSERT INTO 
  (MENU_ID, MENU_NM, PARENT_CD, PARENT_CD_NM, MENU_URL, USE_YN, BOARD_ID)
VALUES
  ('e3c9584f-8ba5-42fc-9ad1-f0d3f0ac8a4c', '운영자관리', 'M1000', '', '/operation/staff/staffList.do', 'Y', 0);

INSERT INTO 
  (MENU_ID, MENU_NM, PARENT_CD, PARENT_CD_NM, MENU_URL, USE_YN, BOARD_ID)
VALUES
  ('683c5eae-d8d2-4aca-8efd-c7caaf8ca513', '공통코드관리', 'M1000', '', '/operation/code/codeList.do', 'Y', 0);

INSERT INTO 
  (MENU_ID, MENU_NM, PARENT_CD, PARENT_CD_NM, MENU_URL, USE_YN, BOARD_ID)
VALUES
  ('96b28ae9-1c2a-4383-bde6-8e0f1c4aad10', '메뉴관리', 'M1000', '', '/operation/staff/staffMenuList.do', 'Y', 0);

INSERT INTO 
  (MENU_ID, MENU_NM, PARENT_CD, PARENT_CD_NM, MENU_URL, USE_YN, BOARD_ID)
VALUES
  ('c7a784c3-c9b6-46dd-8f91-0f7e30d0e3a9', '게시판 관리', 'M1000', '', '/operation/board/boardList.do', 'Y', 0);

INSERT INTO 
  (MENU_ID, MENU_NM, PARENT_CD, PARENT_CD_NM, MENU_URL, USE_YN, BOARD_ID)
VALUES
  ('3e5d5338-e897-424d-98ca-0f528fe957e8', '회원관리', 'M1000', '', '/operation/user/userList.do', 'Y', 0);

INSERT INTO 
  (MENU_ID, MENU_NM, PARENT_CD, PARENT_CD_NM, MENU_URL, USE_YN, BOARD_ID)
VALUES
  ('9b786cca-f9cb-4f94-b56d-ce8ebad79e9c', '배너관리', 'M1000', '', '/operation/banner/bannerList.do', 'Y', 0);

