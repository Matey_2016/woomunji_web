INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('ADMIN_FLAG', 'CODE', '관리자 레벨 구분', '관리자 레벨 구분', '관리자 레벨 구분', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('C1001', 'ADMIN_FLAG', '슈퍼관리자', 'M1000', '슈퍼관리자', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('C1002', 'ADMIN_FLAG', '관리자', '관리자', '관리자', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('MENU_FLAG', 'CODE', '관리자 메뉴구분', '관리자 메뉴구분', '관리자 메뉴구분', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('M1000', 'MENU_FLAG', '시스템메뉴', '/operation/board/boardList.do?lnb_id=c7a784c3-c9b6-46dd-8f91-0f7e30d0e3a9', '시스템 메뉴', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('M2000', 'MENU_FLAG', '국문홈페이지', '/operation/board/postList.do?board_id=6&lnb_id=58a5dd65-d429-4437-b18f-9b9ca6290740', '국문홈페이지', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('uPhone', 'CODE', '지역번호 코드', '지역번호 코드', '지역번호 코드', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('uMobile', 'CODE', '휴대폰 국번코드', '휴대폰 국번코드', '휴대폰 국번코드', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('uEmail', 'CODE', '이메일 도메인코드', '이메일 도메인코드', '이메일 도메인코드', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('010', 'uMobile', '', '', '010', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('011', 'uMobile', '', '', '011', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('016', 'uMobile', '', '', '016', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('017', 'uMobile', '', '', '017', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('018', 'uMobile', '', '', '018', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('019', 'uMobile', '', '', '019', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('02', 'uPhone', '', '', '02', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('031', 'uPhone', '', '', '031', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('032', 'uPhone', '', '', '032', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('033', 'uPhone', '', '', '033', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('naver.com', 'uEmail', '', '', '네이버', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('daum.net', 'uEmail', '', '', '다음', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('gmail.com', 'uEmail', '', '', '구글', 'Y');

INSERT INTO 
  (code_id, high_id, code_desc, code_etc, code_nm, use_yn)
VALUES
  ('nate.com', 'uEmail', '', '', '네이트', 'Y');



