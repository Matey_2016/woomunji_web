//<![CDATA[
	
		//달력팝업 (기본)
    	var basicCalendar = {
	 		monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	 		dayNamesMin: ['일','월','화','수','목','금','토'],
	 		weekHeader: 'Wk',
	 		dateFormat: 'yy-mm-dd', // 날짜형식(20120303)
	 		autoSize: false, //자동리사이즈(false 이면 상위 정의에 따름)
	 		changeMonth: true, // true이면 월변경, false이면 월변경못함
	 		changeYear: true, // true이면 년변경, false이면 년변경못함 
	 		showMonthAfterYear: true, //년 뒤에 월 표시
	 		buttonImageOnly: true, //이미지표시   
	 		buttonText: '', //버튼 텍스트 표시 
	 		buttonImage: '/images/operation/btn/ico_calendar.png', 
	 		showOn: "both", //엘리먼트와 이미지 동시 사용(both,button)
	 		yearRange: 'c-99:c+99', //1990년부터 2020년까지
	 		maxDate: '+5Y', // 오늘부터6년후날짜까지만, '+0d' 오늘 이전 날짜만 선택
	 		minDate: '-10Y'
 		};
    	
    	
    	//달력팝업 
    	//오늘 이전날짜 선택 불가, 종료일은 시작일 이후부터 선택가능
    	var todayCalendar = {
	 		monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	 		dayNamesMin: ['일','월','화','수','목','금','토'],
	 		weekHeader: 'Wk',
	 		dateFormat: 'yy-mm-dd', // 날짜형식(20120303)
	 		autoSize: false, //자동리사이즈(false 이면 상위 정의에 따름)
	 		changeMonth: true, // true이면 월변경, false이면 월변경못함
	 		changeYear: true, // true이면 년변경, false이면 년변경못함 
	 		showMonthAfterYear: true, //년 뒤에 월 표시
	 		buttonImageOnly: true, //이미지표시   
	 		buttonText: '', //버튼 텍스트 표시 
	 		buttonImage: '/images/operation/btn/ico_calendar.png', 
	 		showOn: "both", //엘리먼트와 이미지 동시 사용(both,button)
	 		yearRange: 'c-99:c+99', //1990년부터 2020년까지
	 		maxDate: '+5Y', // 오늘부터6년후날짜까지만, '+0d' 오늘 이전 날짜만 선택
	 		minDate: '0'
 		};
    	
    	//달력팝업 
    	var arrayCalendar = {
	 		monthNamesShort: ['1월','2월','3월','4월','5월','6월','7월','8월','9월','10월','11월','12월'],
	 		dayNamesMin: ['일','월','화','수','목','금','토'],
	 		weekHeader: 'Wk',
	 		dateFormat: 'yy-mm-dd', // 날짜형식(20120303)
	 		autoSize: false, //자동리사이즈(false 이면 상위 정의에 따름)
	 		changeMonth: true, // true이면 월변경, false이면 월변경못함
	 		changeYear: true, // true이면 년변경, false이면 년변경못함 
	 		showMonthAfterYear: true, //년 뒤에 월 표시
	 		buttonImageOnly: true, //이미지표시   
	 		buttonText: '', //버튼 텍스트 표시 
	 		buttonImage: '/images/operation/btn/ico_calendar.png', 
	 		//showOn: "both", //엘리먼트와 이미지 동시 사용(both,button)
	 		yearRange: 'c-99:c+99', //1990년부터 2020년까지
	 		maxDate: '+5Y', // 오늘부터6년후날짜까지만, '+0d' 오늘 이전 날짜만 선택
	 		minDate: '-10Y'
 		};
    	
    $(document).ready(function(){	
 		$("#start_dt").datepicker(todayCalendar);
 		$("#start_dt").attr("readonly",true); 
 		$("#end_dt").datepicker(todayCalendar);
 		$("#end_dt").attr("readonly",true);
 		$("#annncDt").datepicker(todayCalendar);
 		$("#annncDt").attr("readonly",true);
 		//$("img.ui-datepicker-trigger").attr("style","margin-left:5px; vertical-align:middle; cursor:pointer;"); //이미지버튼 style적용
 		$("img.ui-datepicker-trigger").attr("class","calender_btn m_r10"); //이미지버튼 css적용
 		$("#ui-datepicker-div").hide(); //자동으로 생성되는 div객체 숨김
	}); //end .ready
//]]>		