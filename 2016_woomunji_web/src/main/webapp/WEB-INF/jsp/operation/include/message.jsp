<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
<div class="checkpage-box">
	<div class="checkpage-box-top">
		<div class="checkpage-box-bot">
			<c:if test="${!empty param.directMsg}">
				<p>${param.directMsg}</p>
			</c:if>
			<c:if test="${empty param.directMsg}">
				<p><spring:message code="${param.msgCode}" text="잘못된 메세지 코드 (code: ${param.msgCode})" /></p>
			</c:if>
		</div>
	</div>

	<div class="checkpage-box-btn">
	<c:set var="isTargetTop" value="${param.forwardUrl eq '/cmmn/login'}" />
	<c:set var="nextUrl" value="${(empty param.forwardUrl)? '/operation/contents' : param.forwardUrl}.do" />
	<form action="${nextUrl}" method="get" <c:if test="${isTargetTop}">target="_top"</c:if>>
		<c:forEach items="${param}" var="p">
			<c:if test="${p.key ne 'msgCode' and p.key ne 'forwardUrl' and p.key ne 'baseURI' and p.key ne 'errors' and p.key ne 'x' and p.key ne 'y'}">
			<input type="hidden" name="${p.key}" value="${p.value}" /></c:if>
		</c:forEach>
		<input type="image" id="checkBtn" src="/images/operation/share_img/admin_btn_confirm_b.gif" alt="확인" class="border-no"/>
	</form> 
	</div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
    	$('#checkBtn').focus();
    });
    $(function() {
		var backFlag="${param.backFlag}";
		if (backFlag) {
			$('#checkBtn').click(function() {
				history.back();
				return false;
			});
		}
	}); 
</script>
<jsp:include page="/operation/foot.do"></jsp:include>
