<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
			<form name="searchForm" id="searchForm" method="get" action="cultureList.do" class="board_search">
				<input type="hidden" id="cpage" 	name="cpage" 	value="1"/>
				<input type="hidden" id="item_cd" 	name="item_cd" 	value=""/>
				<input type="hidden" id="crlts_no" 	name="crlts_no" value=""/>
				<input type="hidden" id="ctrd_cd" 	name="ctrd_cd" 	value=""/>
				<dl style="text-align:left;padding:0 0 10px 10px">
					<dt><strong>문화재 분류</strong></dt>
					<dd style="margin-left:3px">
					<c:forEach items="${cultureList}" var="code" varStatus="status">
						<input type="checkbox" id="item_cd_${code.code_id }" name="item_cds" value="${code.code_id }" <c:if test="${fn:contains(item_cd, code.code_id) }">checked</c:if>>
						<label for="item_cd_${code.code_id }" style="padding-right:5px">${code.code_nm }</label>
					</c:forEach>
					</dd>
				</dl>
				<dl style="text-align:left;padding:0 0 10px 10px">
					<dt><strong>지역</strong></dt>
					<dd style="margin-left:35px">
					<c:forEach items="${areaList}" var="code" varStatus="status">
						<input type="checkbox" id="ctrd_cd_${code.code_id }" name="ctrd_cds" value="${code.code_id }" <c:if test="${fn:contains(ctrd_cd, code.code_id) }">checked</c:if>>
						<label for="ctrd_cd_${code.code_id }" style="padding-right:5px">${code.code_nm }</label>
					</c:forEach>
					</dd>
				</dl>
				<dl style="text-align:left;padding:0 0 10px 10px">
					<dt><strong>시대구분</strong></dt>
					<dd style="margin-left:14px">
					<c:forEach items="${ageList}" var="code" varStatus="status">
						<input type="checkbox" id="age_cd_${code.code_id }" name="age_cds" value="${code.code_id }" <c:if test="${fn:contains(age_cd, code.code_id) }">checked</c:if>>
						<label for="age_cd_${code.code_id }" style="padding-right:5px">${code.code_nm }</label>
					</c:forEach>
					</dd>
				</dl>
				<dl style="text-align:left;padding:0 0 10px 10px">
					<dt>
						<strong>문화재명</strong>
					</dt>
					<dd style="margin-left:14px"> 
					<input type="text" maxlength="20" title="검색어 입력" class="search_input" id="s_text" name="s_text" value="${param.s_text }" placeholder="검색어를 입력하세요." />
					</dd>
					<dd><input type="image" alt="검색버튼" src="/images/operation/btn/searchBtn.gif" /></dd>
				</dl>
			</form><br> 
			<p class="adm_submit"></p>
			<div class="table_list">
			<table class="list-normal" summary="회원관리 게시판">
				<caption>회원관리 게시판</caption>
				<colgroup>
					<col width="8%" />
					<col width="auto" />
					<col width="20%" />
					<col width="12%" />
					<col width="10%" />
					<col width="10%" />
					<col width="10%" />
				</colgroup>
			
				<thead>
					<tr>
						<th scope="col">번호</th>
						<th scope="col">문재화명</th>
						<th scope="col">문화재명(한자)</th>
						<th scope="col">종목</th>
						<th scope="col">소재지</th>
						<th scope="col">등록일</th>
						<th scope="col">최종수정일</th>
					</tr>
				</thead>
			
				<tbody>
				<c:forEach items="${listVO.pageList}" var="resultVO" varStatus="status">
					<c:if test="${not status.last }"><tr></c:if>
					<c:if test="${status.last }"><tr class="last_line "></c:if>
						<td>${(listVO.recordCount-listVO.pageSize*(listVO.pageNo-1))-status.index}</td>
						<td class="txt-left"><a href="cultureView.do?item_cd=${resultVO.item_cd }&crlts_no=${resultVO.crlts_no }&ctrd_cd=${resultVO.ctrd_cd }">${resultVO.crlts_nm}</a></td>
						<td>${resultVO.crlts_nm_chcrt}</td>
						<td>${resultVO.item_nm} 제${resultVO.crlts_no_nm }호</td>
						<td>${resultVO.ctrd_nm}</td>
						<td>${resultVO.reg_date}</td>
						<td>${resultVO.upt_date}</td>
					</tr>
				</c:forEach>
				<c:if test="${listVO.recordCount eq 0}">
					<tr class="last_line ">
						<td colspan="7"><spring:message code="empty_list" /></td>
					</tr>
				</c:if>
				</tbody>
			</table>
			</div>
	
			<!-- paging area -->
			<div class="paging">
				<tld:pagingTag
					pageNo="${listVO.pageNo}"
					lastPageNo="${listVO.totalPageNo}"
					linkPattern="javascript:movePage(#pageNo);"
					stylePrefix=""
					firstPageDisplayPattern="<img src='/images/operation/btn/paging_prev1.gif' alt='처음' style='vertical-align:text-bottom'/>"
					prevGroupDisplayPattern="<img src='/images/operation/btn/paging_prev2.gif' alt='이전' style='vertical-align:text-bottom'/>"
					linkedPageDisplayPattern="#pageNo"
					currentPageDisplayPattern="#pageNo"
					nextGroupDisplayPattern="<img src='/images/operation/btn/paging_next2.gif' alt='다음' style='vertical-align:text-bottom' />"
					lastPageDisplayPattern="<img src='/images/operation/btn/paging_next1.gif' alt='맨끝' style='vertical-align:text-bottom'/>"
					groupSize="10" />
			</div>
			<script type="text/javascript">
				$(document).ready(function(){
					<c:if test="${empty param.cpage}">$("#cpage").attr("value","1");</c:if>
				});
			
			
				function movePage(page){
					$("#cpage").attr("value",page);
					$("#searchForm").submit();
				}
				
				function goView(item_cd,crlts_no,ctrd_cd  ){
					<c:if test="${not empty param.cpage}">$("#cpage").attr("value","${param.cpage}");</c:if>
					$("#item_cd").attr("value",item_cd);
					$("#crlts_no").attr("value",crlts_no);
					$("#ctrd_cd").attr("value",ctrd_cd);
					$("#searchForm").attr("action","cultureView.do");
					$("#searchForm").submit();
				}
			</script>
<jsp:include page="/operation/foot.do"></jsp:include>