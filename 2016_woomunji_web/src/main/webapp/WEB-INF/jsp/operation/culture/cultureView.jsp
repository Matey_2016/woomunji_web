<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
		<div class="table_memberList">
		<table class="view-normal" summary="상세뷰" >
		<caption>게시판 등록</caption>
		<colgroup>
			<col width="15%" />
			<col width="30" />
			<col width="15%" />
			<col width="30" />
		</colgroup>
		<tbody>
			<tr>
				<th>명칭</th>
				<td class="left">${resultVO.crlts_nm }(${resultVO.crlts_nm_chcrt })</td>
				<th>종목</th>
				<td class="left">${resultVO.item_nm } 제${resultVO.crlts_no_nm }호</td>
			</tr>
			<tr>
				<th>소재지</th>
				<td class="left">${resultVO.ctrd_nm }</td>
				<th>시대</th>
				<td class="left">${resultVO.age_nm }</td>
			</tr>
			<tr>
				<th>최초 등록일</th>
				<td class="left">${resultVO.reg_date }</td>
				<th>최종 수정일</th>
				<td class="left">${resultVO.upt_date }</td>
			</tr>
			<tr>
				<th>게시여부</th>
				<td class="left"><input type="checkbox" id="post_yn" name="post_yn" onclick="post_yn(this.checked);" <c:if test="${resultVO.post_yn ne 'N' }">checked</c:if>/><label for="post_yn">게시함</label></td>
			</tr>
			<tr>
				<td class="left" colspan="4">${resultVO.crlts_dc }</td>
			</tr>
			<tr>
				<td class="left" colspan="4">
				<c:forEach items="${imageList }" var="img" varStatus="status">
				<img src="${img.image_url }" style="vertical-align:top;border:1px;border-style:solid;padding:5px;margin:10px">
				</c:forEach>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	
	
	
	<!-- button area -->
	<div class="btn-all-align2 pd-b20">
		<p class="adm_listBtn">
		<span class="btn-align">
			<a href="postWrite.do?post_id=&amp;${searchParam }&amp;mode=M"><img alt="수정버튼" src="/images/operation/btn/editBtn02.gif" class="uodate"/></a>
			<a href="#this" onclick="history.back();"><img alt="목록버튼" src="/images/operation/btn/listBtn.gif" /></a>
		</span>
		</p>
	</div>
	
	
	
	
	<script type="text/javascript">
		
		function post_yn(checked){
			if (!checked){
				if(confirm("${resultVO.crlts_nm}의 게시를 중지하시겠습니까?")){
					$("#post_yn").attr("value","N");
				}
			} else {
				$("#post_yn").attr("value","Y");
			}
			
			
			$.ajax({
				type 	: 	"get",
				url 	: 	"/operation/culture/postUpdate.do",
				data 	: 	"item_cd=${resultVO.item_cd}&crlts_no=${resultVO.crlts_no}&ctrd_cd=${resultVO.ctrd_cd}&post_yn="+$("#post_yn").val(),
				success : 	function(data) {
								if (data.result == "T") {
									alert("${resultVO.crlts_nm}의 게시여부가 변경되었습니다.");
								} else {
									alert("${resultVO.crlts_nm}의 게시여부가 변경이 실패하였습니다.");
								}
				},
				error	:	function(request, status, error){
					alert("시스템 에러가 발생하였습니다.");
				}
			});
		}
		
		
	</script>
<jsp:include page="/operation/foot.do"></jsp:include>
