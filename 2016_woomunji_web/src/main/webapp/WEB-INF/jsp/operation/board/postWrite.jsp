<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
<script type="text/javascript" src="/js/vendor/ckeditor/ckeditor.js"></script>

		<div class="table_memberList">
		<form id="postDTO" action="postWriteDo.do" name="postDTO" method="post" enctype="multipart/form-data">
		<input type="hidden" name="board_id"  		id="board_id"  		value="${board.board_id }" />	
		<input type="hidden" name="board_nm"  		id="board_nm"  		value="${board.board_nm }" />	
		<input type="hidden" name="contents"  		id="contents"  		value=""/>
		<input type="hidden" name="org_title"  		id="org_title"  	value="${postDTO.title }"/>
		<input type="hidden" name="org_contents"  	id="org_contents"  	value="${fn:escapeXml(postDTO.contents) }"/>   
		<input type="hidden" name="mode"  	  		id="mode"  	 		value="${param.mode }"/>
		<input type="hidden" name="post_id"   		id="post_id"   		value="${postDTO.post_id }"/>
		<input type="hidden" name="parent_post_id"	id="parent_post_id" value="${postDTO.post_id }"/>
		<input type="hidden" name="email"     		id="email"   	 	value="${postDTO.email }"/>
		<table class="view-normal" summary="상세뷰" >
		<caption>게시판 등록</caption>
		<colgroup>
			<col width="15%" />
			<col width="*" />
		</colgroup>
		<tbody>
			<tr>
				<th>등록게시판</th>
				<td class="left font_style text_box">${board.board_nm }</td>
			</tr>
			<c:if test="${not empty code1}">
				<tr>
					<th>구분</th>
					<td class="left font_style text_box">
						<select title="구분" id="etc_cd1" name="etc_cd1" class="required input_type01">
							<option value="">선택</option>
							<c:forEach items="${code1 }" var="cate" varStatus="status">	
							<option value="${cate.code_id }" <c:if test="${cate.code_id eq postDTO.etc_cd1 }">selected</c:if>>${cate.code_nm }</option>
							</c:forEach>
						</select> 
						<c:if test="${not empty code2 and board.code_relation eq 'Y' }">
						<select title="구분" id="etc_cd2" name="etc_cd2" class="required input_type01">
							<option value="">선택</option>
						</select> 
						</c:if>
					</td>
				</tr>
			</c:if>
			<c:if test="${not empty code2 and board.code_relation ne 'Y'}">
				<tr>
					<th>구분</th>
					<td class="left font_style text_box">
						<select title="구분" id="etc_cd1" name="etc_cd2" class="required input_type01">
							<option value="">선택</option>
							<c:forEach items="${code2 }" var="cate" varStatus="status">	
							<option value="${cate.code_id }" <c:if test="${cate.code_id eq postDTO.etc_cd1 }">selected</c:if>>${cate.code_nm }</option>
							</c:forEach>
						</select> 
					</td>
				</tr>
			</c:if>
			<tr>
				<th>제목</th>
				<td class="left font_style text_box">
					<c:choose>
						<c:when test="${param.mode eq 'R' }"><c:set var="title" value="Re:${postDTO.title }"/></c:when>
						<c:otherwise><c:set var="title" value="${postDTO.title }"/></c:otherwise>
					</c:choose>
					<input type="text" name="title" style="ime-mode:active; width:400px" required="required" title="제목" value="${title }"/>
				</td>
			</tr>
			<tr>
				<th>등록자</th>
				<td class="left font_style text_box">
					<input type="hidden" name="user_id" value="${postDTO.user_id}"/>
					<input type="text" name="user_nm" value="${postDTO.user_nm}" style="ime-mode:active; width:200px" required="required" title="등록자"/>
				</td>
			</tr>
			
			
			<!-- 동영상게시판일 경우 동영상 경로 저장 -->
			<c:if test="${board.movie_yn eq 'Y' or board.exLink_yn eq 'Y'}">
			<tr>
				<th>동영상/외부링크 경로</th>
				<td class="left font_style text_box">
					<input type="text" name="movie_url" value="${postDTO.movie_url }" style="ime-mode:inactive; width:400px" required="required" title="동영상 외부링크 경로"/>
					<p style="margin-top:5px">
					<input type="checkbox" id="link_yn" value="Y" <c:if test="${postDTO.link_yn eq 'Y' }">checked</c:if> name="link_yn" class="chk" style="width:25px"/><label for="link_yn">새창으로 링크이동</label>
					</p>
				</td>
			</tr>
			</c:if>
			
			<!-- 비밀글인 경우 -->
			<c:if test="${board.pass_yn eq 'Y'}">
			<tr>
				<th>비밀번호</th>
				<td class="left font_style text_box">
					<input type="password" name="passwd" id="passwd" value="${postDTO.passwd }"/>
				</td>
			</tr>
			</c:if>
			
			<tr>
				<th>내용</th>
				<td class="left font_style text_box">
					<textarea name="editor1" id="editor1" style="ime-mode:active; width:300px">
					<c:choose>
						<c:when test="${param.mode eq 'R' }">
							<p>&nbsp;</p><p>---------------원본글---------------</p>${postDTO.contents }
						</c:when>
						<c:otherwise>${postDTO.contents }</c:otherwise>
					</c:choose>
					</textarea>
				</td>
			</tr>
			
			<!-- 요약글 사용하는 경우 -->
			<c:if test="${board.summary_yn eq 'Y'}">
			<tr>
				<th>요약글</th>
				<td class="left font_style text_box">
					<textarea name="sumry" id="sumry" style="ime-mode:active; width:90%" rows="5">${postDTO.sumry }</textarea>
				</td>
			</tr> 
			</c:if>
			
			<!-- 첨부파일 기능 -->
			<c:if test="${board.file_yn eq 'Y' }">
			<tr>
				<th>첨부파일</th>
				<td class="left font_style">
					<!-- 첨부된 파일 목록 -->
					<c:forEach items="${fileList}" var="attFileList" varStatus="status">
						<c:if test="${attFileList.fileType eq 'FILE' }">
						삭제 : <input type="checkbox" name="delfileId" value="${attFileList.fileId }" class="chk" /> ${attFileList.orgFileName }<br />
						</c:if>
					</c:forEach>
					
					<c:forEach begin="1" end="${board.file_cnt }" step="1" var="file" varStatus="status">
					<%-- <div id="file_div">
						<div id="file_div_${status.count }"> --%>
							<input type="file" name="attFile_${status.count }" id="attFile_${status.count }" style="width:70%"/>
						<!-- </div> 
					</div> -->
					</c:forEach>
				</td>
			</tr>
			</c:if>
			<c:if test="${board.thumb_yn eq 'Y' or board.movie_yn eq 'Y'}">
			<th>썸네일이미지</th>
			<td class="left font_style">
					<c:forEach items="${fileList}" var="attFileList" varStatus="status">
						<c:if test="${attFileList.fileType eq 'THUMB' }">
						삭제 : <input type="checkbox" name="delfileId" value="${attFileList.fileId }" class="chk" /> ${attFileList.orgFileName }<br />
						</c:if>
					</c:forEach>
					<!-- <div id="file_div_thumb">
						<div id="file_div_1_file_div_thumb"> -->
							<input type="file" name="thumb" id="thumb" style="width:70%"/><c:if test="${param.board_id eq 17 }"><span class="right">(권장 사이즈 : 190 X 207 픽셀)</span></c:if>
						<!-- </div>
					</div> -->
				</td>
			</c:if>
			
			<!-- 대표공지 사용시, 답변이 아닌경우 -->
			<c:if test="${board.notice_yn eq 'Y' && resultVO.mode ne 'R'}">
			<tr>
				<th>대표공지여부</th>
				<td class="left">
					<input type="checkbox" name="notice_yn" id="notice_yn" class="chk" value="Y" <c:if test="${postDTO.notice_yn eq 'Y' }">checked="checked"</c:if>/><label for="notice_yn"> 대표공지로 설정</label>
				</td>
			</tr>
			</c:if>
		</tbody>
	</table>
	<!-- 썸네일게시판, 동영상게시판은 파일추가 금지 -->
	</form>
	</div>
	
	<!-- button area -->
	<div class="btn-all-align2 pd-b20">
		<p class="adm_listBtn">
		<span class="btn-align">
			<a href="#none;"><img alt="저장버튼" src="/images/operation/btn/saveBtn.gif" id="save" class="create"/></a>
			<a href="#none" onclick="history.back();">
				<img src="/images/operation/btn/cancleBtn.gif" alt="취소" />
			</a>
		</span>
		</p>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
			
			CKEDITOR.replace('editor1'); 
			$("#save").click(function(){
				
				var editor = CKEDITOR.instances.editor1;
				$("#contents").val(editor.getData());
				
				if ($('#postDTO').validate()){ //유효성 검사를 적용
					$("#postDTO").submit();
				}
			});
			
			
			<c:if test="${board.code_relation eq 'Y'}">
				//구분1 변경시 연관코드 목록 호출
		        $("#etc_cd1").change(function(){
		       		workCdChange();
		       	});
				
				
		        function workCdChange(){
		        	if($("#etc_cd1").val() == ""){
		        		$("#etc_cd2").html("");
		        		$("#etc_cd2").append("<option value=''>선택</option>");
		        		return false;
		        	}
		        	
		        	$.ajax({
		        		type 	: 	"post",
		        		url 	: 	"/jsonCodeList.do",
		        		data 	: 	"code_etc="+$("#etc_cd1").val(),
		        		success : 	function(data) {
		        					$("#etc_cd2").html("");
		        					var code = data.jsonCodeList.split("^");
		        					for (var i=0; i<code.length; i++){
		        						if(i==0){
		        							$("#etc_cd2").append("<option value=''>선택</option>");
		        						}
		        						
		        						if('${mentoringDTO.etc_cd2}' == code[i].split(",")[0] ){
		        							$("#etc_cd2").append("<option value='"+code[i].split(",")[0]+"' selected>"+code[i].split(",")[1]+"</option>");
		        						}else{
		        							$("#etc_cd2").append("<option value='"+code[i].split(",")[0]+"'>"+code[i].split(",")[1]+"</option>");
		        						}						
		        					}
		        		},
		        		error	:	function(request, status, error){
		        			alert("시스템 에러가 발생하였습니다.");
		        		}
		        	});
		        }
			</c:if>
			
		});
	</script>
	<jsp:include page="/operation/foot.do"></jsp:include>
	
