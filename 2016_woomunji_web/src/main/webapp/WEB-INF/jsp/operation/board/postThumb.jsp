<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
			<script src="/html/js/nailthumb/jquery.nailthumb.1.1.min.js"></script>
			<link rel="stylesheet" href="jquery.nailthumb.1.1.min.css">
		    <style>
		      .my-thumb-1 {
		        width: 200px;
		        height: 150px;
		      }
		    </style>


			
			<script>
		      $(document).ready(function() {
		        $( '.my-thumb-1' ).nailthumb();
		      });
		    </script>
		    
			<form method="post" action="post.do" class="board_search" onsubmit="$('#loading').show();">
				<input type="hidden" id="pageId" name="pageId" value="${board.pageId }" />		
				<input type="hidden" id="gubun" name="gubun" value="List" />
				<input type="hidden" id="board_id" name="board_id" value="${board.board_id }" />
				<dl>
					<dt>
						<select title="카테고리 선택" id="queryStr1" name="queryStr1">
							<option value="title">제목</option> 
							<option value="user_nm">작성자</option>
							<option value="contents">내용</option> 
						</select>
					</dt>
					<dd>
					<input type="text" maxlength="20" title="검색어 입력" class="search_input" id="queryStr2" name="queryStr2" placeholder="검색어를 입력하세요." />
					</dd>
					<dd><input type="image" alt="검색버튼" src="/html/admin/images/btn/searchBtn.gif" /></dd>
				</dl>
				
			</form><br> 
			
			<p class="adm_submit"></p>
			<div class="">
			<table class="list-normal" summary="회원관리 게시판" border="1">
				<caption>회원관리 게시판</caption>
				<colgroup>
					<col width="20%" />
					<col width="20%" />
					<col width="20%" />
					<col width="20%" />
					<col width="20%" />
					
				</colgroup>
				<tbody>
				
				
				
				
				<c:set var="result" value="0"/>
				<c:forEach begin="0" end="${listVO.recordCount}" step="1" varStatus="status">
					
					
					
					<c:if test="${result < listVO.recordCount}">
					
					<c:forEach begin="1" end="5" step="1" varStatus="img">
					<c:set var="result" value="${result + 1 }"/>
					
					
					<c:if test="${ img.index eq 1 }"><tr></c:if>
					<td>
						<c:if test="${result-1 < listVO.recordCount}">
						<div class="my-thumb-1" style="float:none;position:relative;text-align:center" >
							<img src="/UPLOAD/BOARD${listVO.pageList[result-1].file_path}/${listVO.pageList[result-1].system_file_nm}">
						</div><br />
						<a href="post.do?post_id=${listVO.pageList[result-1].post_id }&amp;gubun=View&amp;mode=M&amp;${searchParam }" onclick="$('#loading').show();">${listVO.pageList[result-1].title }</a> 
						</c:if>
						<c:if test="${result-1 >= listVO.recordCount}">
							<div class="my-thumb-1" style="float:none;position:relative;text-align:center" >
							<a href="#none"><img src="/html/admin/images/btn/no_photo.png"></a>
							</div><br />&nbsp;
						</c:if>
						
					</td>
					</c:forEach>
					</tr>
					</c:if>
				</c:forEach>
				
				<c:if test="${listVO.recordCount eq 0}">
					<tr class="last_line ">
						<td colspan="6"><spring:message code="code_empty_list" /></td>
					</tr>
				</c:if>
				</tbody>
			</table>
			</div>
	
		<!-- paging area -->
		<div class="paging">
		<tld:pagingTag
			pageNo="${listVO.pageNo}"
			lastPageNo="${listVO.totalPageNo}"
			linkPattern="${request.requestURI}?cpage=#pageNo&amp;gubun=List&amp;${searchParam}"
			stylePrefix=""
			firstPageDisplayPattern="<img src='/html/admin/images/btn/prev1.gif' alt='처음' />"
			prevGroupDisplayPattern="<img src='/html/admin/images/btn/prev2.gif' alt='이전' />"
			linkedPageDisplayPattern="#pageNo"
			currentPageDisplayPattern="#pageNo"
			nextGroupDisplayPattern="<img src='/html/admin/images/btn/next2.gif' alt='다음' />"
			lastPageDisplayPattern="<img src='/html/admin/images/btn/next1.gif' alt='맨끝' />"
			groupSize="10" />
	</div>
	
	<!-- button area -->
	<!-- button area -->
	<div class="btn-all-align">
		<p class="adm_submit">
		<span class="btn-align">
			<a href="post.do?gubun=Write&amp;board_id=${param.board_id }&amp;mode=C&amp;pageId=${param.pageId}" onclick="$('#loading').show();">
				<img src="/html/admin/images/btn/adm_submit.gif" alt="등록버튼" />
			</a>
		</span>
		</p>
	</div>
