<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
		<div class="table_memberList">
		<table class="view-normal" summary="상세뷰" >
		<caption>게시판 등록</caption>
		<colgroup>
			<col width="15%" />
			<col width="*" />
		</colgroup>
		<tbody>
			<tr>
				<th>등록게시판</th>
				<td class="left">${board.board_nm }</td>
			</tr>
			<c:if test="${not empty resultVO.etc_cd1}">
				<tr>
					<th>구분</th>
					<td class="left">${resultVO.etc_nm1}</td>
				</tr>
			</c:if>
			<c:if test="${not empty resultVO.etc_cd2}">
				<tr>
					<th>구분</th>
					<td class="left">${resultVO.etc_nm2}</td>
				</tr>
			</c:if>
			<tr>
				<th>제목</th>
				<td class="left">${resultVO.title }</td>
			</tr>
			<tr>
				<th>등록자(아이디)</th>
				<td class="left">${resultVO.user_nm }(${resultVO.user_id })</td>
			</tr>
			<tr>
				<th>등록일</th>
				<td class="left">${resultVO.reg_dt }</td>
			</tr>
			<c:if test="${board.movie_yn eq 'Y' or board.exLink_yn eq 'Y'}">
			<tr>
				<th>동영상/외부링크 경로</th>
				<td class="left font_style text_box">
				${resultVO.movie_url }<c:if test="${resultVO.link_yn eq 'Y' }">(새창으로 링크이동)</c:if>
				</td>
			</tr>
			</c:if>
			<!-- 비밀글인 경우 -->
			<c:if test="${board.pass_yn eq 'Y'}">
			<tr>
				<th>비밀번호</th>
				<td class="left font_style text_box">${postDTO.passwd }</td>
			</tr>
			</c:if>
			<tr>
				<th>내용</th>
				<td class="left">
				<p>${resultVO.contents }</p>
				</td>
			</tr>
			<!-- 요약글 사용하는 경우 -->
			<c:if test="${board.summary_yn eq 'Y'}">
			<tr>
				<th>요약글</th>
				<td class="left">
				<p>${resultVO.sumry }</p>
				</td>
			</tr>
			</c:if>
			<c:if test="${board.file_yn eq 'Y' }">
			<tr>
				<th>첨부파일</th>
				<td class="left">
					<c:forEach items="${fileList}" var="attFileList" varStatus="status">
						<c:if test="${attFileList.fileType eq 'FILE' }">
						<a href="/download.do?filePath=${attFileList.filePath}&fileName=${attFileList.sysFileName}">${attFileList.orgFileName }</a><br />
						</c:if>
					</c:forEach>
				</td>
			</tr>
			</c:if>
			<c:if test="${board.thumb_yn eq 'Y' or board.movie_yn eq 'Y'}">
			<tr>
				<th>썸네일 이미지</th>
				<td class="left">
					<c:forEach items="${fileList}" var="attFileList" varStatus="status">
						<c:if test="${attFileList.fileType eq 'THUMB' }">
						<a href="/download.do?filePath=${attFileList.filePath}&fileName=${attFileList.sysFileName}">${attFileList.orgFileName }</a><br />
						</c:if>
					</c:forEach>
				</td>
			</tr>
			</c:if>
			<c:if test="${board.notice_yn eq 'Y' }">
			<tr>
				<th>대표공지여부</th>
				<td class="left">
					<c:if test="${resultVO.notice_yn eq 'Y' }">본 게시물은 대표공지글 입니다.</c:if>
				</td>
			</tr>
			</c:if>
		</tbody>
	</table>
	</div>
	
	
	<c:if test="${board.comment_yn eq 'Y' }">
	<div class="table_memberList" style="margin-top:25px">
		<form id="postDTO" action="commentWriteDo.do" name="postDTO" method="post">
		<input type="hidden" name="board_id" id="board_id" value="${board.board_id }" />	
		<input type="hidden" name="post_id"  id="post_id" value="${postDTO.post_id }"/>
		<table class="view-normal" summary="상세뷰" >
			<caption>게시판 등록</caption>
			<colgroup>
				<col width="15%" />
				<col width="*" />
			</colgroup>
			<tbody>
				<tr>
					<th>댓글</th>
					<td class="left font_style text_box">
					<textarea id="comment" name="comment" title="댓글 내용" required="required" style="width:100%" rows="5"></textarea>
					</td>
					<td class="left">
					<a href="#none;"><img alt="저장버튼" src="/images/operation/btn/saveBtn.gif" id="save" class="create"/></a>
					</td>
				</tr>
				<c:forEach items="${commentList }" var="comment" varStatus="status">
				<tr>
					<th>${comment.user_nm }</th>
					<td class="left"><tld:HtmlStringTag value="${comment.comment}"></tld:HtmlStringTag>  </td>
					<td>${comment.reg_dt }<a href="#this" onclick="commentDelete('${comment.comment_id}');"><img src="/images/operation/btn/btn_delete01.png" class="delete"></a></td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
		</form>
	</div>
	</c:if>
	<!-- button area -->
	<div class="btn-all-align2 pd-b20">
		<p class="adm_listBtn">
		<span class="btn-align">
			<c:if test="${board.reply_yn eq 'Y' and resultVO.parent_post_id eq resultVO.post_id  }">
			<a href="postWrite.do?post_id=${resultVO.post_id }&amp;${searchParam }&amp;mode=R"><img alt="답변버튼" src="/images/operation/btn/answerBtn.gif" class="create"/></a>
			</c:if>
			<a href="postWrite.do?post_id=${resultVO.post_id }&amp;${searchParam }&amp;mode=M"><img alt="수정버튼" src="/images/operation/btn/editBtn02.gif" class="uodate"/></a>
			<a href="#none;"><img alt="삭제버튼" src="/images/operation/btn/deleteBtn.gif" id="delete" class="delete"/></a>
			<a href="postList.do?${searchParam }"><img alt="목록버튼" src="/images/operation/btn/listBtn.gif" /></a>
		</span>
		</p>
	</div>
	
	
	<!-- 이전/다음글 -->
	<c:if test="${board.preNext_yn eq 'Y' and not empty preNextList}">
	<p class="adm_submit"></p>
	<div class="table_list">
	<table class="list-normal" summary="회원관리 게시판">
		<caption>회원관리 게시판</caption>
		<colgroup>
			<col width="10%" />
			<col width="auto" />
			<col width="15%" />
			<col width="15%" />
			<col width="15%" />
			
		</colgroup>
	
		<thead>
			<tr>
				<th scope="col">번호</th>
				<th scope="col">제목</th>
				<th scope="col">등록자</th>
				<th scope="col">조회수</th>
				<th scope="col">등록일</th>
				
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${preNextList}" var="preNextList" varStatus="status">
		
		<tr>
			<td>
			<c:if test="${preNextList.preNext eq 'PRE'}">이전글</c:if>
			<c:if test="${preNextList.preNext eq 'NEXT'}">다음글</c:if>
			</td>
			<td class="txt-left"><a href="postView.do?board_id=${preNextList.board_id}&post_id=${preNextList.post_id}">${preNextList.title}</a></td>
			<td>${preNextList.user_nm}</td>
			<td>${preNextList.hit}</td>
			<td>${preNextList.reg_dt}</td>
		</tr>
		</c:forEach>
		</tbody>
	</table>
	</div>
	</c:if>
	
	<script type="text/javascript">
		$(document).ready(function(){
			//게시물 삭제
			$("#delete").click(function(){
				var msg = "";
				if ("${board.reply_yn}" == "Y"){
					msg="문의글 삭제시 답변도 삭제됩니다.\n";
				}
				msg += "삭제하시겠습니까?";
				
				if (confirm(msg)){
					location.href="postDelete.do?post_id=${resultVO.post_id}&board_id=${resultVO.board_id}";
				}
			});
			
			//댓글등록
			$("#save").click(function(){
				if($("#postDTO").validate()){
					$("#postDTO").submit();
				}
			});
		});
		
		
		function commentDelete(comment_id){
			if (confirm("해당 댓글을 삭제하시겠습니까?")){
				location.href="commentDelete.do?post_id=${resultVO.post_id}&board_id=${resultVO.board_id}&comment_id="+comment_id;
			}
		}
	</script>
<jsp:include page="/operation/foot.do"></jsp:include>
