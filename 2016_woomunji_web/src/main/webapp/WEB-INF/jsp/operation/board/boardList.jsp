<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
			<form method="get" action="boardList.do" class="board_search">
				<dl>
					<dt>
						<select title="카테고리 선택" id="s_type" name="s_type">
							<option value="user_nm">이름</option> 
							<option value="user_id">아이디</option>
						</select>
					</dt>
					<dd> 
					<input type="text" maxlength="20" title="검색어 입력" class="search_input" id="s_text" name="s_text" placeholder="검색어를 입력하세요." />
					</dd>
					<dd><input type="image" alt="검색버튼" src="/images/operation/btn/searchBtn.gif" /></dd>
				</dl>
			</form><br> 
			<p class="adm_submit"></p>
			<div class="table_list">
			<table class="list-normal" summary="회원관리 게시판">
				<caption>회원관리 게시판</caption>
				<colgroup>
					<col width="5%" />
					<col width="auto" />
					<col width="8%" />
					<col width="8%" />
					<col width="8%" />
					<col width="8%" />
					<col width="8%" />
					<col width="8%" />
					<col width="8%" />
					<col width="8%" />
					<col width="8%" />
				</colgroup>
			
				<thead>
					<tr>
						<th scope="col">번호</th>
						<th scope="col">이름</th>
						<th scope="col">아이디</th>
						<th scope="col">게시글수</th>
						<th scope="col">파일첨부</th>
						<th scope="col">답변</th>
						<th scope="col">이전/다음</th>
						<th scope="col">썸네일</th>
						<th scope="col">동영상</th>
						<th scope="col">비밀글</th>
						<th scope="col">바로가기</th>
					</tr>
				</thead>
			
				<tbody>
				<c:forEach items="${listVO.pageList}" var="resultVO" varStatus="status">
					<c:if test="${not status.last }"><tr></c:if>
					<c:if test="${status.last }"><tr class="last_line "></c:if>
						<td>${(listVO.recordCount-listVO.pageSize*(listVO.pageNo-1))-status.index}</td>
						<td class="txt-left"><a href="boardWrite.do?board_id=${resultVO.board_id}&amp;mode=M&amp;${searchParam}">${resultVO.board_nm}</a></td>
						<td>${resultVO.board_id}</td>
						<td>${resultVO.postCnt}</td>
						<td>${resultVO.file_yn}</td>
						<td>${resultVO.reply_yn}</td>
						<td>${resultVO.preNext_yn}</td>
						<td>${resultVO.thumb_yn}</td>
						<td>${resultVO.movie_yn}</td>
						<td>${resultVO.pass_yn}</td>
						<td><a href="postList.do?board_id=${resultVO.board_id }">바로가기</a></td>
					</tr>
				</c:forEach>
				<c:if test="${listVO.recordCount eq 0}">
					<tr class="last_line ">
						<td colspan="11"><spring:message code="empty_list" /></td>
					</tr>
				</c:if>
				</tbody>
			</table>
			</div>
	
			<!-- paging area -->
			<div class="paging">
				<tld:pagingTag
					pageNo="${listVO.pageNo}"
					lastPageNo="${listVO.totalPageNo}"
					linkPattern="${request.requestURI}?cpage=#pageNo&amp;${searchParam}"
					stylePrefix=""
					firstPageDisplayPattern="<img src='/images/operation/btn/prev1.gif' alt='처음' />"
					prevGroupDisplayPattern="<img src='/images/operation/btn/prev2.gif' alt='이전' />"
					linkedPageDisplayPattern="#pageNo"
					currentPageDisplayPattern="#pageNo"
					nextGroupDisplayPattern="<img src='/images/operation/btn/next2.gif' alt='다음' />"
					lastPageDisplayPattern="<img src='/images/operation/btn/next1.gif' alt='맨끝' />"
					groupSize="10" />
			</div>
			
			<!-- button area -->
			<div class="btn-all-align">
				<p class="adm_submit">
				<span class="btn-align">
					<a href="boardWrite.do?mode=C"><img src="/images/operation/btn/adm_submit.gif" alt="등록버튼" class="create"/></a>
				</span>
				</p>
			</div>
<jsp:include page="/operation/foot.do"></jsp:include>