<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
		<form id="boardDTO" name="boardDTO" action="boardWriteDo.do" method="POST">
		<input type="hidden" name="mode" 	 value="${resultVO.mode }"/>
		<input type="hidden" name="board_id" value="${resultVO.board_id }"/>
		
		<div class="table_memberList">
		<table class="view-normal" summary="상세뷰" >
		<caption>게시판 등록</caption>
		<colgroup>
			<col width="15%" />
			<col width="*" />
		</colgroup>

		<tbody>
			<tr>
				<th>게시판 이름</th>
				<td class="left font_style text_box">
					<input type="text" id="board_nm" name="board_nm" required="required" title="게시판 이름" value="${resultVO.board_nm }" placeholder="게시판 이름을 입력하세요."/>
				</td>
			</tr>
			<%-- <tr>
				<th>게시판 타입</th>
				<td class="left font_style text_box">
					<select id="type_cd" name="type_cd">
						<option value="">선택</option>
						<c:forEach items="${codeList }" var="list" varStatus="status">
							<option value="${list.code_id }" <c:if test="${resultVO.type_cd eq list.code_id }">selected</c:if>>${list.code_nm }</option>
						</c:forEach>
					</select>
				</td>
			</tr>  --%>
			<tr>
				<th>부가기능 설정</th>
				<td class="left">
					<div><input type="checkbox" name="file_yn" 		id="file_yn" 		<c:if test="${resultVO.file_yn eq 'Y' 		}">checked</c:if> 	class="chk" value="Y"/><label for="file_yn"> 첨부파일 사용</label></div>
					<div><input type="checkbox" name="reply_yn" 	id="reply_yn" 		<c:if test="${resultVO.reply_yn eq 'Y' 		}">checked</c:if> 	class="chk" value="Y"/><label for="reply_yn"> 답변기능 사용</label></div>
					<div><input type="checkbox" name="notice_yn" 	id="notice_yn" 		<c:if test="${resultVO.notice_yn eq 'Y' 	}">checked</c:if> 	class="chk" value="Y"/><label for="notice_yn"> 대표공지 사용</label></div>
					<div><input type="checkbox" name="comment_yn" 	id="comment_yn" 	<c:if test="${resultVO.comment_yn eq 'Y' 	}">checked</c:if> 	class="chk" value="Y"/><label for="comment_yn"> 댓글기능 사용</label></div>
					<div><input type="checkbox" name="pass_yn" 		id="pass_yn" 		<c:if test="${resultVO.pass_yn eq 'Y'		}">checked</c:if> 	class="chk" value="Y"/><label for="pass_yn"> 비밀글기능 사용</label></div>
					<div><input type="checkbox" name="preNext_yn" 	id="preNext_yn" 	<c:if test="${resultVO.preNext_yn eq 'Y'	}">checked</c:if> 	class="chk" value="Y"/><label for="preNext_yn"> 이전/다음글 사용</label></div>
					<div><input type="checkbox" name="thumb_yn" 	id="thumb_yn" 		<c:if test="${resultVO.thumb_yn eq 'Y'		}">checked</c:if> 	class="chk" value="Y"/><label for="thumb_yn"> 썸네일기능 사용</label></div>
					<div><input type="checkbox" name="movie_yn" 	id="movie_yn" 		<c:if test="${resultVO.movie_yn eq 'Y'		}">checked</c:if> 	class="chk" value="Y"/><label for="movie_yn"> 동영상기능 사용</label></div>
					<div><input type="checkbox" name="summary_yn" 	id="summary_yn" 	<c:if test="${resultVO.summary_yn eq 'Y'	}">checked</c:if> 	class="chk" value="Y"/><label for="summary_yn"> 요약글 사용</label></div>
					<div><input type="checkbox" name="exLink_yn" 	id="exLink_yn" 		<c:if test="${resultVO.exLink_yn eq 'Y'		}">checked</c:if> 	class="chk" value="Y"/><label for="exLink_yn"> 외부링크 사용</label></div>
					<div><input type="checkbox" name="urlDeny_yn" 	id="urlDeny_yn" 	<c:if test="${resultVO.urlDeny_yn eq 'Y'	}">checked</c:if> 	class="chk" value="Y"/><label for="urlDeny_yn"> url 조작금지</label></div>
				</td>
			</tr>
			<tr>
				<th>첨부파일 갯수</th>
				<td class="left text_box">
					<input type="text" id="file_cnt" name="file_cnt" required="required" title="첨부파일 갯수" value="${resultVO.file_cnt }" style="width:30px"/>
				</td>
			</tr>
			
			<tr>
				<th>코드사용1</th>
				<td class="left box_s02">
					<select id="use_code1" name="use_code1">
						<option value="">코드선택</option>
						<c:forEach items="${codeList}" var="code" varStatus="status">
						<option value="${code.code_id }" <c:if test="${resultVO.use_code1 eq code.code_id }">selected</c:if>>${code.code_nm }</option>
						</c:forEach>
					</select>
				</td>
			</tr>
			<tr>
				<th>코드사용2</th>
				<td class="left box_s02">
					<select id="use_code2" name="use_code2">
						<option value="">코드선택</option>
						<c:forEach items="${codeList}" var="code" varStatus="status">
						<option value="${code.code_id }" <c:if test="${resultVO.use_code2 eq code.code_id }">selected</c:if>>${code.code_nm }</option>
						</c:forEach>
					</select>
					<input type="checkbox" id="code_realtion" name="code_relation" value="Y" <c:if test="${resultVO.code_relation eq 'Y' }">checked</c:if> style="width:20px"/>
					<label for="code_relation">코드2는 코드1에 종속됩니다.(코드1의  "공통코드"값을 코드2의 "비고"값에 입력하세요.)</label>
				</td>
			</tr>
			<tr>
				<th>목록갯수</th>
				<td class="left font_style text_box">
					<input type="text" id="page_size" name="page_size" style="width:30px" required="required" title="목록갯수" value="${resultVO.page_size }"/>
				</td>
			</tr>
			<tr>
				<th>새글 표시기간</th>
				<td class="left font_style text_box">
					<input type="text" id="new_day" name="new_day" style="width:30px" required="required" title="새글 표시기간" value="${resultVO.new_day }"/>일(0일 경우 새글표시 하지않음)
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	
	<!-- button area -->
	<div class="btn-all-align2 pd-b20">
		<p class="adm_listBtn">
		<span class="btn-align">
			<a href="#none;"><img alt="저장버튼" src="/images/operation/btn/saveBtn.gif" id="save" class="create"/></a>
			<a href="boardList.do?${searchParam }"><img src="/images/operation/btn/cancleBtn.gif" alt="취소" /></a>
		</span>
		</p>
	</div>
	</form>
	<script type="text/javascript">
	$(document).ready(function(){
	    $("#save").click(function(){
	    	if ($('#boardDTO').valid()) {	
	    		$('#boardDTO').submit(); 
	    	}
	    }); 
	});
	</script>
<jsp:include page="/operation/foot.do"></jsp:include>