<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
		<form method="get" action="postList.do" class="board_search">
			<input type="hidden" id="pageId" name="pageId" value="${board.pageId }" />		
			<input type="hidden" id="gubun" name="gubun" value="List" />
			<input type="hidden" id="board_id" name="board_id" value="${board.board_id }" />
			<dl>
				<dt>
					<select title="카테고리 선택" id="s_type" name="s_type">
						<option value="all">전체</option> 
						<option value="title">제목</option> 
						<option value="user_nm">작성자</option>
						<option value="contents">내용</option> 
					</select>
				</dt>
				<dd>
				<input type="text" maxlength="20" title="검색어 입력" class="search_input" id="s_text" name="s_text" placeholder="검색어를 입력하세요." />
				</dd>
				<dd><input type="image" alt="검색버튼" src="/images/operation/btn/searchBtn.gif" /></dd>
			</dl>
		</form><br> 
			
			<p class="adm_submit"></p>
			<div class="table_list">
			<table class="list-normal" summary="회원관리 게시판">
				<caption>회원관리 게시판</caption>
				<colgroup>
					<col width="10%" />
					<c:if test="${not empty listVO.pageList[0].etc_cd1 }">
						<col width="10%" />
					</c:if>
					<col width="auto" />
					<col width="15%" />
					<col width="15%" />
					<col width="15%" />
					
				</colgroup>
			
				<thead>
					<tr>
						<th scope="col">번호</th>
						<c:if test="${not empty listVO.pageList[0].etc_cd1 }">
							<th scope="col">구분</th>
						</c:if>
						<th scope="col">제목</th>
						<th scope="col">등록자</th>
						<th scope="col">등록일</th>
						<th scope="col">첨부파일</th>
						
					</tr>
				</thead>
				<tbody>
				<c:forEach items="${listVO.pageList}" var="resultVO" varStatus="status">
					<c:if test="${not status.last }"><tr></c:if>
					<c:if test="${status.last }"><tr class="last_line "></c:if>
						<td>${(listVO.recordCount-listVO.pageSize*(listVO.pageNo-1))-status.index}</td>
						<c:if test="${not empty listVO.pageList[0].etc_cd1 }">
							<td>${resultVO.etc_nm1}</td>
						</c:if>
						<td class="txt-left">
							
							<c:if test="${resultVO.parent_post_id ne resultVO.post_id}">
							<img src="/images/operation/sub/list_re_icon02.gif">
							</c:if>
							
							
							<a href="postView.do?post_id=${resultVO.post_id }&amp;mode=M&amp;${searchParam }">
							<c:choose>
								<c:when test="${resultVO.notice_yn eq 'Y'}"><strong>[공지]</strong>${resultVO.title}</c:when>
								<c:otherwise>${resultVO.title}</c:otherwise>
							</c:choose>
							</a>
							
							<!-- 새글 아이콘 표시 -->
							<c:if test="${board.new_day > 0 && resultVO.new_interval < board.new_day}">
							<img src="/images/operation/sub/newIcon.gif">
							</c:if>
							
							
							
							
						</td>
						<td>${resultVO.user_nm}</td>
						<td>${resultVO.reg_dt}</td>
						<td><c:if test="${resultVO.fileCnt > 0 }"><img src="/images/operation/sub/add_file.gif" title="첨부파일" alt="첨부파일"/></c:if></td>
					</tr>
				</c:forEach>
				<c:if test="${listVO.recordCount eq 0}">
					<tr class="last_line ">
						<td colspan="8"><spring:message code="empty_list" /></td>
					</tr>
				</c:if>
				</tbody>
			</table>
			</div>
	
		<!-- paging area -->
		<div class="paging">
			<tld:pagingTag pageNo="${listVO.pageNo}"
				lastPageNo="${listVO.totalPageNo}"
				linkPattern="${request.requestURI}?cpage=#pageNo&amp;gubun=List&amp;${searchParam}"
				stylePrefix=""
				firstPageDisplayPattern="<img src='/html/admin/images/btn/prev1.gif' alt='처음' />"
				prevGroupDisplayPattern="<img src='/html/admin/images/btn/prev2.gif' alt='이전' />"
				linkedPageDisplayPattern="#pageNo"
				currentPageDisplayPattern="#pageNo"
				nextGroupDisplayPattern="<img src='/html/admin/images/btn/next2.gif' alt='다음' />"
				lastPageDisplayPattern="<img src='/html/admin/images/btn/next1.gif' alt='맨끝' />"
				groupSize="10" />
		</div>
	
	<!-- button area -->
	<!-- button area -->
	<div class="btn-all-align">
		<p class="adm_submit">
		<span class="btn-align">
			<a href="postWrite.do?&amp;board_id=${param.board_id }&amp;mode=C">
				<img src="/images/operation/btn/adm_submit.gif" alt="등록버튼" class="create"/>
			</a>
		</span>
		</p>
	</div>
<jsp:include page="/operation/foot.do"></jsp:include>