<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
			
			<form:form commandName="userDTO" action="/operation/user/userRegist.do" method="POST">
			<input type="hidden" id="pageId" name="pageId" value="${param.pageId }" />
			<input type="hidden" id="mode" name="mode" value="${param.mode }" />
			<input type="hidden" name="user_id" value="${userDTO.user_id }"/>
			<input type="hidden" name="user_nm" value="${userDTO.user_nm }"/>
			<input type="hidden" name="email_yn" value="${userDTO.email_yn }"/>
			<input type="hidden" name="sms_yn" value="${userDTO.sms_yn }"/>
			<div class="table_memberList">
			<table class="view-normal" summary="상세뷰" >
					<caption>이름, 아이디, 메일, 연락처, 휴대폰, 가입일, 닉네임, 성별, 주소, 가입구분, IP</caption>
					<colgroup>
					<col width="15%" />
					<col width="*" />
				</colgroup>
				<tbody>
					<tr>
						<th>아이디</th>
						<td class="left">${userDTO.user_id}</td>
					</tr>
					<tr>
						<th>이름</th>
						<td class="left">${userDTO.user_nm}</td>
					</tr>
					<tr>
						<th>메일</th>
						<td class="left text_box box_s02">
							<input type="text" name="email_id" id="email_id" value="${fn:split(userDTO.email,'@')[0] }" style="width:124px;">
							<span class="whelkTxt">@</span>
							<input type="text" name="email_domain" id="email_domain" value="${fn:split(userDTO.email,'@')[1] }" style="width:164px;">
							
							
								<select name="email_vendor" id="email_vendor">
									<option value="">직접입력</option>
									<c:forEach items="${emailCode }" var="code" varStatus="status">
									<option value="${code.code_id }">${code.code_nm }</option>
									</c:forEach>
								</select>
							
							 
						</td>
					</tr>
					<tr>
						<th>연락처</th>
						<td class="left text_box">
							<select name="phone1" id="phone1">
								<c:forEach items="${phoneCode }" var="code" varStatus="status">
								<option value="${code.code_id }" <c:if test="${fn:split(userDTO.phone,'-')[0] eq code.code_id }">selected="selected"</c:if>>${code.code_nm }</option>
								</c:forEach>
							</select>
							<span class="whelkTxt">-</span>
							<input type="text" name="phone2" id="phone2" value="${fn:split(userDTO.phone,'-')[1] }" maxlength="4" style="width:97px;">
							<span class="whelkTxt">-</span>
							<input type="text" name="phone3" id="phone3" value="${fn:split(userDTO.phone,'-')[2] }" maxlength="4" style="width:97px;">
						</td>
					</tr>
					<tr>
						<th>휴대폰</th>
						<td class="left text_box">
							<select name="mobile1" id="mobile1">
								<c:forEach items="${mobileCode }" var="code" varStatus="status">
								<option value="${code.code_id }" <c:if test="${fn:split(userDTO.mobile,'-')[0] eq code.code_id }">selected="selected"</c:if>>${code.code_nm }</option>
								</c:forEach>
							</select>
							<span class="whelkTxt">-</span>
							<input type="text" name="mobile2" id="mobile2" value="${fn:split(userDTO.mobile,'-')[1] }" maxlength="4" style="width:97px;">
							<span class="whelkTxt">-</span>
							<input type="text" name="mobile3" id="mobile3" value="${fn:split(userDTO.mobile,'-')[2] }" maxlength="4" style="width:97px;">
						</td>
					</tr>
					<tr>
						<th>가입일</th>
						<td class="left">${userDTO.reg_dt}</td>
					</tr>
				</tbody>
			</table>
			
			</div>
			
			<p class="adm_listBtn">
			<a href="#" id="save"><img alt="저장버튼" src="/images/operation/btn/saveBtn.gif" class="update"/></a>
			<a href="userList.do?${searchParam}"><img alt="목록버튼" src="/images/operation/btn/listBtn.gif" /></a>
			</p>
			</form:form>
		<!-- paging area -->
		<div class="paging"></div>
		
		<!-- button area -->
		<div class="btn-all-align"></div>


<script type="text/javascript">
	
	$(document).ready(function(){
		//이메일 도메인 선택
		$("#email_vendor").change(function(){
			$("#email_domain").val($("#email_vendor").val());
		});    	
		
		
		
		//회원정보 수정 버튼
		$("#save").click(function(){
			
			 			
			var u_email = $("#email_id").val()+"@"+$("#email_domain").val();
			var regEmail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
			if(!regEmail.test(u_email)) {
				//alert("이메일 주소가 유효하지 않습니다");
				//return false;
			}
			
			
			
			if($("input:radio[id='sms_y']").is(":checked")){
				
				
				var u_mobile = $("#mobile1").val()+"-"+$("#mobile2").val()+"-"+$("#mobile3").val();
				var regMobile = /^01[016789]-\d{3,4}-\d{4}$/;

				if(!regMobile.test(u_mobile)) {
					//alert("휴대전화번호를 올바르게 입력해주세요");
					//return false;
				}
				
				
			} else {
				
				if (	($("#mobile2").val()== "" && $("#mobile3").val() == "") 	&& 
					 	($("#phone2").val()	== "" && $("#phone3").val() == "") 	){
						
						//alert("연락처 중 하나는 필수입력 항목입니다.");
						//return;
				}
			}
			
			
			//전화번호 유효성 체크
			if ($("#phone2").val()	!= "" || $("#phone3").val() != ""){
				var u_phone = $("#phone1").val()+"-"+$("#phone2").val()+"-"+$("#phone3").val();
				var regExp = /^\d{2,3}-\d{3,4}-\d{4}$/;
				
				if(!regExp.test(u_phone)) {
					//alert("전화번호를 올바르게 입력해주세요");
					//return false;
				}
				
			}
		
			 
			$("#userDTO").submit();
			;
			
			
			//회원정보 수정
			/* 
			$.ajax({
				type 	: 	"post",
				url 	: 	"/mypage/member/memberUpdate.do",
				data 	: 	$("#userDTO").serialize(),
				success : 	function(data){
								
								if 	(data.result == "1") { 
									alert("회원정보가 변경되었습니다.");
								} else if (data.result == "0") {
									alert("회원정보가 변경에 실패하였습니다.");
								}
								
								//location.href="/mypage/member/memberChk.do";
							}
			});
			 */
		});
		
		
		
	});
</script>
<jsp:include page="/operation/foot.do"></jsp:include>