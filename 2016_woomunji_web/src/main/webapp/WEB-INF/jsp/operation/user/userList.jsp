<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>

			<form method="get" action="userList.do" class="board_search" id="searchForm">
				<dl>
					<dt>
						<select title="카테고리 선택" id="s_type" name="s_type">
							<option value="user_nm" <c:if test="${param.s_type eq 'user_nm' }">selected</c:if>>이름</option> 
							<option value="user_id" <c:if test="${param.s_type eq 'user_id' }">selected</c:if>>아이디</option>
						</select>
					</dt>
					<dd>
					<input type="text" maxlength="20" title="검색어 입력" class="search_input" id="s_text" style="ime-mode:active" name="s_text" placeholder="검색어를 입력하세요." value="${param.s_text }"/>
					</dd>
					<dd><input type="image" alt="검색버튼" src="/images/operation/btn/searchBtn.gif" /></dd>
				</dl>
			</form><br> 
			
			<p class="adm_submit">현재페이지 : ${listVO.pageNo }/${listVO.totalPageNo }</p>
			<div class="table_list">
			<table class="list-normal" summary="회원관리 게시판">
				<caption>회원관리 게시판</caption>
				<colgroup>
					<col width="10%" />
					<col width="13%" />
					<col width="13%" />
					<col width="auto" />
					<col width="13%" />
					<col width="13%" />
					<col width="10%" />
					<col width="10%" />
				</colgroup>
				<thead>
					<tr>
						<th scope="col">번호</th>
						<th scope="col">이름</th>
						<th scope="col">아이디</th>
						<th scope="col">메일</th>
						<th scope="col">연락처</th>
						<th scope="col">휴대폰</th>
						<th scope="col">가입일</th>
						<th scope="col">탈퇴일</th>
					</tr>
				</thead>
				<tbody>
				<c:forEach items="${listVO.pageList}" var="resultVO" varStatus="status">
					<c:if test="${not status.last }"><tr></c:if>
					<c:if test="${status.last }"><tr class="last_line "></c:if>
						<td>${(listVO.recordCount-listVO.pageSize*(listVO.pageNo-1))-status.index}</td>
						<td><a href="userView.do?user_id=${resultVO.user_id}&amp;${searchParam}">${resultVO.user_nm}</a></td>
						<td>${resultVO.user_id}</td>
						<td>${resultVO.email}</td>
						<td>${resultVO.phone}</td>
						<td>${resultVO.mobile}</td>
						<td>${resultVO.reg_dt}</td>
						<td>${resultVO.leave_dt}</td>
					</tr>
				</c:forEach>
				<c:if test="${listVO.recordCount eq 0}">
					<tr class="last_line ">
						<td colspan="8"><spring:message code="empty_list" /></td>
					</tr>
				</c:if>
				</tbody>
			</table>
			</div>
	
			<!-- paging area -->
			<div class="paging">
				<tld:pagingTag
					pageNo="${listVO.pageNo}"
					lastPageNo="${listVO.totalPageNo}"
					linkPattern="${request.requestURI}?cpage=#pageNo&amp;${searchParam}"
					stylePrefix=""
					firstPageDisplayPattern="<img src='/images/operation/btn/paging_prev1.gif' alt='처음' />"
					prevGroupDisplayPattern="<img src='/images/operation/btn/paging_prev2.gif' alt='이전' />"
					linkedPageDisplayPattern="#pageNo"
					currentPageDisplayPattern="#pageNo"
					nextGroupDisplayPattern="<img src='/images/operation/btn/paging_next2.gif' alt='다음' />"
					lastPageDisplayPattern="<img src='/images/operation/btn/paging_next1.gif' alt='맨끝' />"
					groupSize="10" />
			</div>
	
			<!-- button area -->
			<div class="btn-all-align">
			<p class="adm_submit">
				<span class="btn-align">
				<a href="#none" id="excel"><img src="/images/operation/btn/adm_down.gif" alt="등록버튼"/></a>
				</span>
			</p>
			</div>

			<script type="text/javascript">
			    $(document).ready(function(){
			    	$("#excel").click(function(){
			    		$("#searchForm").attr("action","/operation/user/userExcelDown.do");
			    		$("#searchForm").submit();
			    		$("#loading").hide();
			    	});
			    });
			</script>
<jsp:include page="/operation/foot.do"></jsp:include>