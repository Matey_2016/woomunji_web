<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
			<div class="table_memberList">
			<table class="view-normal" summary="상세뷰" >
					<caption>이름, 아이디, 메일, 연락처, 휴대폰, 가입일, 닉네임, 성별, 주소, 가입구분, IP</caption>
					<colgroup>
					<col width="15%" />
					<col width="*" />
				</colgroup>
				<tbody>
					<tr>
						<th>아이디</th>
						<td class="left">${userDTO.user_id}</td>
					</tr>
					<tr>
						<th>이름</th>
						<td class="left">${userDTO.user_nm}</td>
					</tr>
					<tr>
						<th>메일</th>
						<td class="left">${userDTO.email}</td>
					</tr>
					<tr>
						<th>연락처</th>
						<td class="left">${userDTO.phone}</td>
					</tr>
					<tr>
						<th>휴대폰</th>
						<td class="left">${userDTO.mobile}</td>
					</tr>
					<tr>
						<th>가입일</th>
						<td class="left">${userDTO.reg_dt}</td>
					</tr>
					<tr>
						<th>비밀번호 초기화</th> 
						<td class="left"><a href="#this" id="initPasswd"><img src="/images/operation/btn/edit_btn.gif"></a>&nbsp;&nbsp;&nbsp;(비밀번호가 사용자 아이디로 변경됩니다.)</td>
					</tr>
				</tbody>
			</table>
			</div>
			
			<p class="adm_listBtn">
				<a href="userModify.do?user_id=${userDTO.user_id}&mode=M&amp;${searchParam}">
					<img src="/images/operation/btn/editBtn02.gif" alt="수정화면 이동" />
				</a>
				<a href="userList.do?${searchParam}">
					<img alt="목록버튼" src="/images/operation/btn/listBtn.gif" />
				</a>
			</p>
			
		<!-- paging area -->
		<div class="paging"></div>
		
		<!-- button area -->
		<div class="btn-all-align"></div>
		<script type="text/javascript">
	
			$(document).ready(function(){
				$("#initPasswd").click(function(){
					if (confirm("${userDTO.user_nm}님의 비밀번호를 초기화하시겠습니까?")){
						//폼전송
						$.ajax({
							type 	: 	"post",
							url 	: 	"initPasswd.do",
							data 	: 	"user_id=${userDTO.user_id}",
							success : 	function(data){
											if 	(data.result == "1") { 
												alert("비밀번호가 회원님의 아이디로 변경되었습니다.");
											}else if (data.result == "0") {
												
											}
										}
						});	//end .ajax
					}
				});
				
			});
		</script>
<jsp:include page="/operation/foot.do"></jsp:include>