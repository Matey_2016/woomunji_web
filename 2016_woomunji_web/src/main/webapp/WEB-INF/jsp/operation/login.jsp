<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/comm_include.jsp" %>
<script type="text/javascript">
	$(document).ready(function(){
		$("#login").click(function(){
		
			if ($('#form').valid()) {
				$.ajax({
					type 	: 	"post",
					url 	: 	"login.do",
					data 	: 	$("#form").serialize(),
					success : 	function(data) {
									if (data.result == "T") {
										location.href="/operation/contents.do";
									} else {
										alert("아이디 또는 비밀번호가 일치하지 않습니다.");
									}
					},
					error	:	function(request, status, error){
						alert("시스템 에러가 발생하였습니다.");
					}
				});
			}
		});
		
	});
</script>
<title>관리자 로그인</title> 
</head>
<body class="login_wrap">
	<div id="loading" style="width:100%;height:100%;position:fixed;top:0;left:0;background:url(/images/operation/login/bg.png);z-index:9999; display:none">
		<div style="-moz-box-shadow:0px 0px 7px #555;-webkit-box-shadow:0px 0px 7px #555;-ms-box-shadow:0px 0px 7px #555;box-shadow:0px 0px 7px #555;text-align:center;font-size:12px;font-weight:bold;color:#555;background:#fff;padding:30px;position:absolute;top:50%;left:50%;margin-left:-74px;-moz-border-radius:5px;-webkit-border-radius:5px;border-radius:5px;" >
			<img src="/images/operation/login/loading.gif" alt="화면을 로딩하는 중입니다." style="margin-bottom:2px" /><br/>
			화면을 로딩중입니다.
		</div>
	</div> 
	<div class="login_inner">
	<form method="post" id="form" class="login_form">
	<fieldset>
		<legend>로그인</legend>
		<h1><img src="/images/operation/sub/adm_m_logo.png" alt="" /></h1>
		<div class="idpwd">
			<span><input id="staff_id" name="staff_id" type="text" class="select_focus" value="admin" placeholder="아이디를 입력하세요." title="아이디" required="required"/></span>
			<span class="password"><input type="password" id="passwd" name="passwd"  value="1111" class="select_focus02" placeholder="비밀번호를 입력하세요." title="비밀번호" required="required" /></span>
		</div>
		<a href="#this;" id="login"><img src="/images/operation/sub/adm_login_btn.png"></a>
	</fieldset>
	</form>
	<div class="adm_main_footer">
	</div>
	</div>
</body>
</html>