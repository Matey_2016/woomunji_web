<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
	<form:form commandName="staffDTO" action="staffWriteDo.do" method="POST">
		<input type="hidden" id="mode" name="mode" value="${param.mode}" />		
		<c:if test="${param.mode eq 'M'}">
			<input type="hidden" id="staff_id" name="staff_id" value="${staffDTO.staff_id}" />
			<c:if test="${staffDTO.staff_lvl eq 'C1001'}">
			<input type="hidden" id="staff_lvl" name="staff_lvl" value="${staffDTO.staff_lvl}" />
			<input type="hidden" id="use_yn" name="use_yn" value="${staffDTO.use_yn}" />
			</c:if>			
		</c:if>	
		
		<div class="table_memberList">
		<table class="view-normal" summary="상세뷰" >
			<caption>관리자ID, 관리자명, 소속부서명, 이메일, 관리자등급, 사용여부</caption>
			<colgroup>
				<col width="15%" />
				<col width="*" />
			</colgroup>
	
			<tbody>
				<tr>
					<th>관리자ID</th>
					<td class="left text_box">
						<c:if test="${param.mode eq 'C'}">
							<form:input path="staff_id" maxLength="24" required="required" title="관리자ID" cssStyle="ime-mode:inactive;" />
						</c:if>
						<c:if test="${param.mode eq 'M'}">
							${staffDTO.staff_id}
						</c:if>
					</td>
				</tr>
				<tr>
					<th>관리자명</th>
					<td class="left text_box">
						<form:input path="staff_nm" maxLength="10" required="required" title="관리자명" cssStyle="ime-mode:active;"/>
					</td>
				</tr>
				<tr>
					<th>비밀번호</th>
					<td class="left text_box">
						<form:password path="passwd" maxLength="20" title="비밀번호" cssStyle="ime-mode:inactive;"/>
					</td>
				</tr>
				<tr>
					<th>연락처</th>
					<td class="left text_box"><form:input path="phone" maxLength="24" cssStyle="ime-mode:active;"/></td>
				</tr>
				<tr>
					<th>휴대폰</th>
					<td class="left text_box"><form:input path="mobile" maxLength="24" cssStyle="ime-mode:active;"/></td>
				</tr>
				<tr>
					<th>이메일</th>
					<td class="left text_box"><form:input path="email" maxLength="128" cssStyle="ime-mode:active; width:300px"/>
				</tr>
				<c:choose>
					<c:when test="${staffDTO.staff_lvl eq 'C1001'}" >
					<tr>
						<th>관리자등급</th>
						<td class="left">${staffDTO.staff_lvl_nm}</td>
					</tr>
					<tr>
						<th>사용여부</th>
						<td class="left">${staffDTO.use_yn_nm}</td>
					</tr>
					</c:when>
					<c:otherwise>
					<tr>
						<th>관리자등급</th>
						<td class="left box_s02">
						<form:select path="staff_lvl">
							<c:forEach items="${codeList}" var="codeDTO" varStatus="Status">
								<form:option value="${codeDTO.code_id}">${codeDTO.code_nm}</form:option> 
							</c:forEach>
						</form:select>
						</td>
					</tr>
					<tr>
						<th>사용여부</th>
						<td class="left"><form:checkbox path="use_yn" value="Y" /><label for="use_yn1"> 사용함</label></td>
					</tr>					
					</c:otherwise>			
			</c:choose>		
			</tbody>
		</table>
		</div>
	
	<p>&nbsp;<br />&nbsp;</p>
	<div class="location">
		<div><h3>메뉴별 권한 설정</h3></div>
	</div>
	<div class="table_memberList1">
	<table class="view-normal" summary="상세뷰" >
		<colgroup>
			<col width="8%" />
			<col width="15%" />
			<col width="*" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
		</colgroup>
		<thead>
			<tr>
				<th scope="col" rowspan="2">선택</th>
				<th scope="col" rowspan="2">상위메뉴</th>
				<th scope="col" rowspan="2">메뉴명</th>
				<th scope="col" colspan="4">권한</th>
			</tr>
			<tr>
				<th scope="col">읽기</th>
				<th scope="col">등록</th>
				<th scope="col">수정</th>
				<th scope="col">삭제</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${authInfoList}" var="authInfoVO" varStatus="status">
				<c:if test="${not status.last }">
				<tr>
				</c:if>
				<c:if test="${status.last }">
				<tr class="last_line ">
				</c:if>
					<td><input type="checkbox" id="menu_chk${status.index}" name="menu_chk${status.index}" onclick="setAuth('${status.index}', this.checked);"/></td>
					<td>${authInfoVO.parent_cd_nm}</td>
					<td class="txt-left">${authInfoVO.menu_nm}<c:if test="${authInfoVO.use_yn eq 'N'}">(사용안함)</c:if></td>
					<td><input type="checkbox" id="rChk${status.index}" name="rChk${status.index}" class="authChk" <c:if test="${authInfoVO.rChk eq 'Y'}">checked</c:if> value="Y"/></td>
					<td><input type="checkbox" id="cChk${status.index}" name="cChk${status.index}" class="authChk" <c:if test="${authInfoVO.cChk eq 'Y'}">checked</c:if> value="Y"/></td>
					<td><input type="checkbox" id="uChk${status.index}" name="uChk${status.index}" class="authChk" <c:if test="${authInfoVO.uChk eq 'Y'}">checked</c:if> value="Y"/></td>
					<td><input type="checkbox" id="dChk${status.index}" name="dChk${status.index}" class="authChk" <c:if test="${authInfoVO.dChk eq 'Y'}">checked</c:if> value="Y"/></td>
					<input type="hidden" id="menu_id${status.index}" name="menu_id" value="${authInfoVO.menu_id}" />
				</tr>
			</c:forEach>
		</tbody>
	</table>
	</div>
	</form:form>
	<!-- button area -->
	<div class="btn-all-align2 pd-b20">
		<p class="adm_listBtn">
		<span class="btn-align">
			<a href="#none;" onclick="staffRegist();"><img alt="저장버튼" src="/images/operation/btn/saveBtn.gif" id="save" class="create"/></a>
			<a href="${baseURI}List.do?${searchParam}">
				<img src="/images/operation/btn/cancleBtn.gif" alt="취소" />
			</a>
		</span>
		</p>
	</div>
	<script type="text/javascript">
	//<![CDATA[
	     $(document).ready(function(){
	        <c:if test="${param.mode eq 'C'}">
	        	$("#use_yn1").attr("checked", true);
	        	$("#passwd").attr("required", "required");
	        </c:if>
	    });
	//]]>
	
	function staffRegist(){
		if ($('#staffDTO').valid()) {	
			$('#staffDTO').submit(); 
		}
	}
	
	function setAuth(index, checked){
		$("#rChk"+index).attr("checked", checked);
		$("#cChk"+index).attr("checked", checked);
		$("#uChk"+index).attr("checked", checked);
		$("#dChk"+index).attr("checked", checked);
	}
	</script>
<jsp:include page="/operation/foot.do"></jsp:include>