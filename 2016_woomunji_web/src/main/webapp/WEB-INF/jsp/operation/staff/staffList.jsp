<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
	<!-- search area -->
	<form method="post" action="staffList.do" class="board_search">
		<dl>
			<dt>
				<select title="카테고리 선택" id="s_type" name="s_type">
					<option value="all" <c:if test="${param.queryStr1 eq 'all' }">selected</c:if>>전체</option>
					<option value="staff_id" <c:if test="${param.queryStr1 eq 'staff_id' }">selected</c:if>>관리자ID</option>
					<option value="staff_nm" <c:if test="${param.queryStr1 eq 'staff_nm' }">selected</c:if>>관리자명</option>
				</select>
			</dt>
			<dd>
			<input type="text" maxlength="20" title="검색어 입력" class="search_input" id="s_text" name="s_text" 
			placeholder="검색어 입력" value="<c:if test="${param.s_text != null }">${param.queryStr2 }</c:if>" />
			</dd>
			<dd><input type="image" alt="검색버튼" src="/images/operation/btn/searchBtn.gif" /></dd>
		</dl>		
	</form><br>
				
				
	<p class="board-total">
		전체 운영자 수 <span>총 <fmt:formatNumber pattern="#,###" value="${listVO.recordCount}" />명</span>
	</p>
	
	<div class="table_list">
		<table class="list-normal" summary="운영자목록">
			<caption>운영자목록</caption>
			<colgroup>
				<col width="5%" />
				<col width="10%" />
				<col width="10%" />
				<col width="25%" />
				<col width="auto" />
				<col width="auto" />
				<col width="10%" />
			</colgroup>
		
			<thead>
				<tr>
					<th scope="col">번호</th>
					<th scope="col">관리자ID</th>
					<th scope="col">관리자명</th>
					<th scope="col">연락처</th>
					<th scope="col">이메일</th>
					<th scope="col">관리자등급</th>
					<th scope="col">사용여부</th>
				</tr>
			</thead>
		
			<tbody>
			<c:forEach items="${listVO.pageList}" var="resultVO" varStatus="status">
				<c:if test="${not status.last }">
				<tr>
				</c:if>
				<c:if test="${status.last }">
				<tr class="last_line ">
				</c:if>
					<td>${(listVO.recordCount-listVO.pageSize*(listVO.pageNo-1))-status.index}</td>
					<td><a href="staffView.do?staff_id=${resultVO.staff_id}&amp;${searchParam}" onclick="$('#loading').show();">${resultVO.staff_id}</a></td>
					<td>${resultVO.staff_nm}</td>
					<td>${resultVO.phone}/${resultVO.mobile}</td>
					<td class="txt-left">${resultVO.email}</td>
					<td>${resultVO.staff_lvl_nm}</td>					
					<td>
					<c:choose>
						<c:when test="${resultVO.use_yn eq 'Y' }">사용</c:when>
						<c:otherwise>사용안함</c:otherwise>
					</c:choose>
					</td>
				</tr>
			</c:forEach>
			<c:if test="${listVO.recordCount eq 0}">
				<tr class="last_line ">
					<td colspan="7"><spring:message code="empty_list" /></td>
				</tr>
			</c:if>
			</tbody>
		</table>
		</div>
	
	<!-- paging area -->
	<div class="paging">
		<tld:pagingTag
			pageNo="${listVO.pageNo}"
			lastPageNo="${listVO.totalPageNo}"
			linkPattern="${request.requestURI}?cpage=#pageNo&amp;${searchParam}"
			stylePrefix=""
			firstPageDisplayPattern="<img src='/html/admin/images/btn/prev1.gif' alt='처음' />"
			prevGroupDisplayPattern="<img src='/html/admin/images/btn/prev2.gif' alt='이전' />"
			linkedPageDisplayPattern="#pageNo"
			currentPageDisplayPattern="#pageNo"
			nextGroupDisplayPattern="<img src='/html/admin/images/btn/next2.gif' alt='다음' />"
			lastPageDisplayPattern="<img src='/html/admin/images/btn/next1.gif' alt='맨끝' />"
			groupSize="10" />
	</div>
	
	<!-- button area -->
	<div class="btn-all-align">
		<p class="adm_submit">
		<span class="btn-align">
			<a href="staffWrite.do?mode=C&amp;${searchParam}">
				<img src="/images/operation/btn/adm_submit.gif" alt="등록버튼" class="create"/>
			</a>
		</span>
		</p>
	</div>
<jsp:include page="/operation/foot.do"></jsp:include>