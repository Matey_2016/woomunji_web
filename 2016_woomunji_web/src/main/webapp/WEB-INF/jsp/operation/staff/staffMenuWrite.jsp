<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
	<form id="menuForm" name="menuForm" action="staffMenuWriteDo.do" method="POST">
		<input type="hidden" id="mode" name="mode" value="${param.mode}" />		
		<c:if test="${param.mode eq 'M'}">
			<input type="hidden" id="menu_id" name="menu_id" value="${staffMenuDTO.menu_id}" />
		</c:if>		
	<div class="table_memberList">
	<table class="view-normal" summary="상세뷰" >
		<caption>메뉴ID, 메뉴명, 상위메뉴, 메뉴URL, 사용여부</caption>
		<colgroup>
			<col width="15%" />
			<col width="*" />
		</colgroup>
		<tbody>
			<tr>
				<th>메뉴ID</th>
				<td class="left text_box">
					<c:if test="${param.mode eq 'C'}">
						자동입력됩니다.
					</c:if>
					<c:if test="${param.mode eq 'M'}">
						${staffMenuDTO.menu_id}
					</c:if>
				</td>
			</tr>
			<tr>
				<th>메뉴명</th>
				<td class="left text_box">
					<input type="text" id="menu_nm" name="menu_nm" maxLength="32" value="${staffMenuDTO.menu_nm}" required="required" title="메뉴명"/>
				</td>
			</tr>
			<tr>
				<th>상위메뉴</th>
				<td class="left box_s02">
				<select id="parent_cd" name="parent_cd">
					<c:forEach items="${codeList}" var="codeDTO" varStatus="Status">
						<option value="${codeDTO.code_id}" <c:if test="${staffMenuDTO.parent_cd eq codeDTO.code_id }">selected="selected"</c:if>>${codeDTO.code_nm}</option> 
					</c:forEach>
				</select>
			</tr>
			<tr>
				<th>메뉴URL</th>
				<td class="left long_text_box">
					<input type="text" id="menu_url" name="menu_url" maxLength="256" value="${staffMenuDTO.menu_url}" required="required" title="메뉴URL"/>
			</tr>
			<tr>
				<th>사용여부</th>
				<td class="left">
					<input type="radio" id="use_yn_y" name="use_yn" value="Y" <c:if test="${staffMenuDTO.use_yn eq 'Y' }">checked="checked"</c:if>/><label for="use_yn_y">사용</label>
					<input type="radio" id="use_yn_n" name="use_yn" value="N" <c:if test="${staffMenuDTO.use_yn eq 'N' }">checked="checked"</c:if>/><label for="use_yn_y">사용안함</label>
				</td>
			</tr>				
		</tbody>
	</table>
	</div>
	
	
	<!-- button area -->
	<div class="btn-all-align2 pd-b20">
		<p class="adm_listBtn">
		<span class="btn-align">
			<a href="#none;"><img alt="저장버튼" src="/images/operation/btn/saveBtn.gif" id="save" class="create"/></a>
			<a href="staffMenuList.do?${searchParam}" onclick="$('#loading').show();">
				<img src="/images/operation/btn/cancleBtn.gif" alt="취소" />
			</a>
		</span>
		</p>
	</div>
	</form>
	<script type="text/javascript">
	//<![CDATA[
	     $(document).ready(function(){
	        $("#save").click(function(){
	        	
	        	if ($('#menuForm').valid()) {
	        		$('#menuForm').submit();     
	        	}
	        });
	        
	        <c:if test="${param.mode eq 'C'}">
				$("#use_yn_y").attr("checked", true);
			</c:if>
	    });
	//]]>
	</script>
<jsp:include page="/operation/foot.do"></jsp:include>

