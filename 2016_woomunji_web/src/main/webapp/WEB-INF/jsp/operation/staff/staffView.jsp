<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
	<div class="table_memberList">
	<table class="view-normal" summary="상세뷰" >
		<caption>관리자ID, 관리자명, 소속부서명, 관리자등급, 사용여부</caption>
		<colgroup>
			<col width="15%" />
			<col width="*" />
		</colgroup>

		<tbody>
			<tr>
				<th>관리자ID</th>
				<td class="left">${staffDTO.staff_id}</td>
			</tr>
			<tr>
				<th>관리자명</th>
				<td class="left">${staffDTO.staff_nm}</td>
			</tr>
			<tr>
				<th>연락처</th>
				<td class="left">${staffDTO.phone}/${staffDTO.mobile}</td>
			</tr>
			<tr>
				<th>이메일</th>
				<td class="left">${staffDTO.email}</td>
			</tr>
			<tr>
				<th>관리자등급</th>
				<td class="left">${staffDTO.staff_lvl_nm}</td>
			</tr>
			<tr>
				<th>사용여부</th>
				<td class="left">
				<c:choose>
					<c:when test="${staffDTO.use_yn eq 'Y' }">사용</c:when>
					<c:otherwise>사용안함</c:otherwise>
				</c:choose>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	
	<p>&nbsp;<br />&nbsp;</p>
	<div class="location">
	<div><h3>메뉴별 권한 설정</h3></div>
	</div>
	<div class="table_memberList1">
	<table class="view-normal" summary="상세뷰" >
		<caption>선택, 상위메뉴, 메뉴명, 메뉴코드, 읽기, 등록, 수정, 삭제</caption>
		<colgroup>
			<col width="8%" />
			<col width="15%" />
			<col width="*" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
			<col width="8%" />
		</colgroup>
		<thead>
			<tr>
				<th scope="col" rowspan="2">No</th>
				<th scope="col" rowspan="2">상위메뉴</th>
				<th scope="col" rowspan="2">메뉴명</th>
				<th scope="col" colspan="4">권한</th>
			</tr>
			<tr>
				<th scope="col">읽기</th>
				<th scope="col">등록</th>
				<th scope="col">수정</th>
				<th scope="col">삭제</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${authInfoList}" var="authInfoVO" varStatus="status">
				<c:if test="${not status.last }">
				<tr>
				</c:if>
				<c:if test="${status.last }">
				<tr class="last_line ">
				</c:if>
					<td>${status.count }</td>
					<td class="txt-left">${authInfoVO.parent_cd_nm}</td>
					<td class="txt-left">${authInfoVO.menu_nm}<c:if test="${authInfoVO.use_yn eq 'N'}">(사용안함)</c:if></td>
					<td><c:if test="${authInfoVO.rChk eq 'Y'}">O</c:if></td>
					<td><c:if test="${authInfoVO.cChk eq 'Y'}">O</c:if></td>
					<td><c:if test="${authInfoVO.uChk eq 'Y'}">O</c:if></td>
					<td><c:if test="${authInfoVO.dChk eq 'Y'}">O</c:if></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
	<!-- button area -->
	<div class="btn-all-align">
		<p class="adm_listBtn">
			<span class="btn-align">
			<a href="staffWrite.do?mode=M&staff_id=${staffDTO.staff_id}&amp;${searchParam}">
				<img src="/images/operation/btn/editBtn02.gif" alt="수정화면 이동" class="update"/>
			</a>
			<a href="staffList.do?${searchParam}" title="목록화면 이동" onclick="$('#loading').show();">
				<img src="/images/operation/btn/listBtn.gif" alt="목록화면 이동" />
			</a>
			</span>
		</p>
	</div>
<jsp:include page="/operation/foot.do"></jsp:include>