<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
	<!-- search area  -->
	<form method="post" action="staffMenuList.do" class="board_search">
		<dl>
			<dd>
				<select id="parent_cd" name="parent_cd" style="width:150px">
					<option value="">상위메뉴 전체</option> 
					<c:forEach items="${codeList}" var="codeDTO" varStatus="Status">
						<option value="${codeDTO.code_id}" <c:if test="${param.parent_cd eq codeDTO.code_id }">selected="selected"</c:if>>${codeDTO.code_nm}</option> 
					</c:forEach>
				</select>
			</dd>
			<dd><input type="image" alt="검색버튼" src="/images/operation/btn/searchBtn.gif" /></dd>
		</dl>		
	</form><br>
	
	<div class="table_list">
		<table class="list-normal" summary="운영자목록">
			<caption>메뉴목록</caption>
			<colgroup>
				<col width="8%" />
				<col width="15%" />
				<col width="15%" />
				<col width="auto" />
				<col width="10%" />
			</colgroup>
		
			<thead>
				<tr>
					<th scope="col">번호</th>
					<th scope="col">메뉴명</th>
					<th scope="col">상위메뉴</th>
					<th scope="col">메뉴URL</th>
					<th scope="col">사용여부</th>
				</tr>
			</thead>
		
			<tbody>
			<c:forEach items="${listVO}" var="resultVO" varStatus="status">
				<c:if test="${not status.last }">
				<tr>
				</c:if>
				<c:if test="${status.last }">
				<tr class="last_line ">
				</c:if>
					<td>${status.count }</td>
					<td><a href="staffMenuWrite.do?menu_id=${resultVO.menu_id }&mode=M&parent_cd=${param.parent_cd}">${resultVO.menu_nm}</a></td>
					<td>${resultVO.parent_cd_nm}</td>
					<td class="txt-left">${resultVO.menu_url}</td>					
					<td>
					<c:choose>
						<c:when test="${resultVO.use_yn eq 'Y' }">사용</c:when>
						<c:otherwise>사용안함</c:otherwise>
					</c:choose>
					</td>
				</tr>
			</c:forEach>
			<c:if test="${empty listVO}">
				<tr class="last_line ">
					<td colspan="5"><spring:message code="empty_list" /></td>
				</tr>
			</c:if>
			</tbody>
		</table>
		</div>
		<div class=non_paging></div>
		
		
		<!-- button area -->
		<div class="btn-all-align">
			<p class="adm_submit">
			<span class="btn-align">
				<a href="staffMenuWrite.do?mode=C"><img src="/images/operation/btn/adm_submit.gif" alt="등록버튼" class="create"/></a>
			</span>
			</p>
		</div>
<jsp:include page="/operation/foot.do"></jsp:include>