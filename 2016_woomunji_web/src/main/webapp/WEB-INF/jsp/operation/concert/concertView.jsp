<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
		<div class="table_memberList">
		<table class="view-normal" summary="상세뷰" >
		<caption>게시판 등록</caption>
		<colgroup>
			<col width="15%" />
			<col width="30" />
			<col width="15%" />
			<col width="30" />
		</colgroup>
		<tbody>
			<tr>
				<th>공연/전시명</th>
				<td class="left" colspan="3">${resultVO.title }</td>
			</tr>
			<tr>
				<th>기간</th>
				<td class="left">${resultVO.start_date } ~ ${resultVO.end_date }</td>
				<th>장소</th>
				<td class="left">${resultVO.place }</td>
			</tr>
			<tr>
				<th>티켓요금</th>
				<td class="left" colspan="3">${resultVO.price }</td>
			</tr>
			<tr>
				<th>분류</th>
				<td class="left">${resultVO.realm_name }</td>
				<th>지역</th>
				<td class="left">${resultVO.area }</td>
			</tr>
			<tr>
				<th>공연/전시 내용</th>
				<td class="left" colspan="3" id="content">${resultVO.contents1 }</td>
			</tr>
			<tr>
				<th>예매 URL</th>
				<td class="left" colspan="3">${resultVO.url }</td>
			</tr>
			<tr>
				<th>공연장 주소</th>
				<td class="left" colspan="3">${resultVO.place_addr }</td>
			</tr>
			<tr>
				<th>장소 URL</th>
				<td class="left" colspan="3">${resultVO.url }</td>
			</tr>
			<tr>
				<th>연락처</th>
				<td class="left">${resultVO.phone }</td>
				<th>게시여부</th>
				<td class="left"><input type="checkbox" id="post_yn" name="post_yn" onclick="post_yn(this.checked);" <c:if test="${resultVO.post_yn ne 'N' }">checked</c:if>/><label for="post_yn">게시함</label></td>
			</tr>
			<tr>
				<th>최초 등록일</th>
				<td class="left">${resultVO.reg_date }</td>
				<th>최종 수정일</th>
				<td class="left">${resultVO.upt_date }</td>
			</tr>
			<tr>
				<th>이미지</th>
				<td class="left" colspan="4">
				<img src="${resultVO.thumbnail }" style="vertical-align:top;border:1px;border-style:solid;padding:5px;margin:10px">
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	
	
	
	<!-- button area -->
	<div class="btn-all-align2 pd-b20">
		<p class="adm_listBtn">
		<span class="btn-align">
			<a href="postWrite.do?post_id=&amp;${searchParam }&amp;mode=M"><img alt="수정버튼" src="/images/operation/btn/editBtn02.gif" class="uodate"/></a>
			<a href="#this" onclick="history.back();"><img alt="목록버튼" src="/images/operation/btn/listBtn.gif" /></a>
		</span>
		</p>
	</div>
	
	<script type="text/javascript">
		
		$(document).ready(function(){
			$(".remove_pTag").attr("style","");	
			//$("#content").html($('.remove_imgTag').attr("alt"));
		});
		
		function post_yn(checked){
			if (!checked){
				if(confirm("${resultVO.title}의 게시를 중지하시겠습니까?")){
					$("#post_yn").attr("value","N");
				}
			} else {
				$("#post_yn").attr("value","Y");
			}
			
			
			$.ajax({
				type 	: 	"get",
				url 	: 	"/operation/culture/postUpdate.do",
				data 	: 	"seq=${resultVO.seq}&post_yn="+$("#post_yn").val(),
				success : 	function(data) {
								if (data.result == "T") {
									alert("${resultVO.title}의 게시여부가 변경되었습니다.");
								} else {
									alert("${resultVO.title}의 게시여부가 변경이 실패하였습니다.");
								}
				},
				error	:	function(request, status, error){
					alert("시스템 에러가 발생하였습니다.");
				}
			});
		}
	</script>
<jsp:include page="/operation/foot.do"></jsp:include>
