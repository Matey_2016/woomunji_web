<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
	<script src="/js/vendor/jquery-ui.min.js"></script>
	<link href="/js/vendor/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<script type="text/javascript" src="/js/vendor/calendar.js" ></script>
	<form:form commandName="bannerDTO" action="bannerWriteDo.do" method="POST" enctype="multipart/form-data">
		<input type="hidden" id="pageId" name="pageId" value="${param.pageId }" />
		<input type="hidden" id="mode" name="mode" value="${param.mode}" />
		<input type="hidden" id="file_path" name="file_path" value="${bannerDTO.file_path}" />
		<input type="hidden" id="file_nm" name="file_nm" value="${bannerDTO.file_nm}" />
		<input type="hidden" id="system_file_nm" name="system_file_nm" value="${bannerDTO.system_file_nm}" />
		<c:if test="${param.mode eq 'M'}">
			<input type="hidden" id="banner_id" name="banner_id" value="${bannerDTO.banner_id}" />
		</c:if>	
			
	<div class="table_memberList">
	<table class="view-normal" summary="상세뷰" >
		<caption>배너 등록/수정</caption>
		<colgroup>
			<col width="15%" />
			<col width="*" />
		</colgroup>

		<tbody>
			<tr>
				<th>제목</th>
				<td class="left text_box">
					<form:input path="title" maxLength="45" title="제목" required="required"/>
				</td>
			</tr>
			<tr>
				<th>기간</th>
				<td class="left text_box">
					<form:input path="start_dt" title="시작일" required="required" readonly="true" cssStyle="width:80px"/> ~
					<form:input path="end_dt" 	title="종료일" required="required" readonly="true" cssStyle="width:80px"/>
					
				</td>
			</tr>
			<tr>
				<th>이미지파일</th>
				<td class="left font_style">
					<input type="file" name="attFile_1" id="attFile_1" title="이미지파일"/>
					<p>${bannerDTO.file_nm}</p>
					<p>&nbsp;</p>
					<p>- 상단배너 이미지 사이즈는 447px X 426px 로 올려주시기 바랍니다.</p>
					<!-- <p>- 하단배너 이미지 사이즈는 465px X 139px 로 올려주시기 바랍니다.</p>
					<p>- 사업안내 이미지 사이즈는 92px X 92px 로 올려주시기 바랍니다.</p> -->
				</td>
			</tr>
			<tr>
				<th>링크</th>
				<td class="left text_box">
					<form:input path="link_url" cssStyle="ime-mode:inactive; width:550px;"/>
					<br />링크가 존재하지 않을 경우 #none을 입력해 주시기 바랍니다.
					<br />내부페이지 링크인 경우 도메인은 입력하지 마십시요.
					<p class="err-msg" ><form:errors path="link_url" /></p>
				</td>
			</tr>
			<tr>
				<th>타겟</th>
				<td class="left">
					<input type="radio" name="target" id="target1" value="O" <c:if test="${bannerDTO.target eq 'O' }">checked</c:if>> : 내부 페이지&nbsp;&nbsp;&nbsp;
					<input type="radio" name="target" id="target2" value="B" <c:if test="${bannerDTO.target eq 'B' }">checked</c:if>> : 외부 페이지
					<p class="err-msg" ><form:errors path="target" /></p>
				</td>
			</tr>
			<tr>
				<th>위치</th>
				<td class="left">
					<input type="radio" name="position" id="position" value="T" <c:if test="${bannerDTO.position eq 'T' }">checked</c:if>> : 상단배너&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
			<tr>
				<th>사용여부</th>
				<td class="left"><form:checkbox path="use_yn" value="Y" /><label for="useYn1"> 사용함</label></td>
			</tr>
			<tr>
				<th>요약글</th>
				<td class="left text_box"><form:input path="summary"/></td>
			</tr>
			
		</tbody>
	</table>
	</div>
	
	<!-- button area -->
	<div class="btn-all-align2 pd-b20">
		<p class="adm_listBtn">
		<span class="btn-align">
			<a href="#none;" onclick="bannerRegist();">
				<img alt="저장버튼" src="/images/operation/btn/saveBtn.gif" id="save" class="create"/>
			</a>
			<a href="bannerList.do?${searchParam}">
				<img src="/images/operation/btn/cancleBtn.gif" alt="취소" />
			</a>
		</span>
		</p>
	</div>
	</form:form>
	<script type="text/javascript">
		$(document).ready(function(){
	    	 <c:if test="${param.mode eq 'C'}">
	    	 	$("input:radio[name='target']:radio[value='O']").attr("checked",true);
	    	 	$("input:radio[name='position']:radio[value='T']").attr("checked",true);
	    	 	$("input:checkbox[name='use_yn']:checkbox[value='Y']").attr("checked",true);
	    	 	$("#attFile_1").attr("required", "required");
	    	 </c:if>
	    	 
	    	$("#start_dt").datepicker(todayCalendar);
	     	$("#end_dt").datepicker(todayCalendar);
	    	 
	         $("input[type=checkbox]").click(function() {
	             if($(this).is(":checked")) {
	             	$(this).val("Y");
	             } else {
	             	$(this).val("N");
	             }
	         });
	         
	      
	    });
	
		function bannerRegist(){
			if ($('#bannerDTO').validate()){ //유효성 검사를 적용
				if ($("#link_url").val()==""){
					$("#link_url").attr("value","#none");
				}
				$('#bannerDTO').submit(); 
			}
		}
	
	</script>
<jsp:include page="/operation/foot.do"></jsp:include>