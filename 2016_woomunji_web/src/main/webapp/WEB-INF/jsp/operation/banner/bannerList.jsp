<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
	<!-- search area -->
	<form method="post" action="${URI}.do" class="board_search" onsubmit="$('#loading').show();">
		<input type="hidden" id="pageId" name="pageId" value="${param.pageId }" />
		<dl>
			<dd>
			<input type="text" maxlength="20" title="검색어 입력" class="search_input" id="s_text" name="s_text" 
			placeholder="검색어 입력" value="<c:if test="${param.s_text != null }">${param.s_text }</c:if>" />
			</dd>
			<dd><input type="image" alt="검색버튼" src="/images/operation/btn/searchBtn.gif" /></dd>
		</dl>		
	</form><br>
				
	<p class="board-total">
		배너 수 <span>총 <fmt:formatNumber pattern="#,###" value="${listVO.recordCount}" />개</span>
	</p>
	
	<div class="table_list">
		<table class="list-normal" summary="배너목록">
			<caption>배너 목록</caption>
			<colgroup>
				<col width="8%" />
				<col width="auto" />
				<col width="25%" />
				<col width="10%" />
				<col width="10%" />
				<col width="10%" />
			</colgroup>
		
			<thead>
				<tr>
					<th scope="col">번호</th>
					<th scope="col">제목</th>
					<th scope="col">기간</th>
					<th scope="col">링크구분</th>
					<th scope="col">위치</th>
					<th scope="col">등록일</th>
				</tr>
			</thead>
		
			<tbody>
			<c:forEach items="${listVO.pageList}" var="resultVO" varStatus="status">
				<c:if test="${not status.last }">
				<tr>
				</c:if>
				<c:if test="${status.last }">
				<tr class="last_line ">
				</c:if>
					<td>${(listVO.recordCount-listVO.pageSize*(listVO.pageNo-1))-status.index}</td>
					<td class="txt-left"><a href="bannerView.do?banner_id=${resultVO.banner_id}&amp;${searchParam}">${resultVO.title}</a></td>
					<td>${resultVO.start_dt} ~ ${resultVO.end_dt}</td>
					<td>
					<c:choose>
						<c:when test="${resultVO.position eq 'T' }">상단배너</c:when>
						<c:when test="${resultVO.position eq 'B' }">하단배너</c:when>
						<c:when test="${resultVO.position eq 'I' }">사업안내</c:when>
						<c:otherwise></c:otherwise>
					</c:choose>
					</td>
					<td>
					<c:choose>
						<c:when test="${resultVO.target eq 'O' }">내부링크</c:when>
						<c:otherwise>외부링크</c:otherwise>
					</c:choose>
					</td>
					<td>${resultVO.reg_dt}</td>
				</tr>
			</c:forEach>
			<c:if test="${listVO.recordCount eq 0}">
				<tr class="last_line ">
					<td colspan="5"><spring:message code="empty_list" /></td>
				</tr>
			</c:if>
			</tbody>
		</table>
		</div>
	 
		<!-- paging area -->
		<div class="paging">
			<tld:pagingTag
					pageNo="${listVO.pageNo}"
					lastPageNo="${listVO.totalPageNo}"
					linkPattern="${request.requestURI}?cpage=#pageNo&amp;${searchParam}"
					stylePrefix=""
					firstPageDisplayPattern="<img src='/images/operation/btn/prev1.gif' alt='처음' />"
					prevGroupDisplayPattern="<img src='/images/operation/btn/prev2.gif' alt='이전' />"
					linkedPageDisplayPattern="#pageNo"
					currentPageDisplayPattern="#pageNo"
					nextGroupDisplayPattern="<img src='/images/operation/btn/next2.gif' alt='다음' />"
					lastPageDisplayPattern="<img src='/images/operation/btn/next1.gif' alt='맨끝' />"
					groupSize="10" />
		</div>
		 
		<!-- button area -->
		<div class="btn-all-align">
			<p class="adm_submit">
			<span class="btn-align">
				<a href="bannerWrite.do?mode=C&amp;${searchParam}">
					<img src="/images/operation/btn/adm_submit.gif" alt="등록버튼" class="create"/>
				</a>
			</span>
			</p>
		</div>
<jsp:include page="/operation/foot.do"></jsp:include>