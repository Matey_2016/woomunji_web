<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/operation/include/dtd.jsp" %>
<jsp:include page="/operation/top.do"></jsp:include>
<jsp:include page="/operation/left.do"></jsp:include>
	<div class="table_memberList">
	<table class="view-normal" summary="상세뷰" >
		<caption>배너 조회</caption>
		<colgroup>
			<col width="15%" />
			<col width="*" />
		</colgroup>

		<tbody>
			<tr>
				<th>제목</th>
				<td class="left">${bannerDTO.title}</td>
			</tr>
			<tr>
				<th>기간</th>
				<td class="left">${bannerDTO.start_dt} ~ ${bannerDTO.end_dt}</td>
			</tr>
			<tr>
				<th>이미지 파일</th>
				<td class="left"><a href="/download.do?filePath=${bannerDTO.file_path}&fileName=${bannerDTO.system_file_nm}">${bannerDTO.file_nm}</a></td>
			</tr>
			<tr>
				<th>링크</th>
				<td class="left">${bannerDTO.link_url}</td>
			</tr>
			<tr>
				<th>타겟</th>
				<td class="left">
				<c:choose>
					<c:when test="${bannerDTO.target eq 'O' }">내부페이지</c:when>
					<c:when test="${bannerDTO.target eq 'B' }">외부페이지</c:when>
					<c:otherwise></c:otherwise>
				</c:choose>
				</td>
			</tr>
			<tr>
				<th>위치</th>
				<td class="left">
				<c:choose>
					<c:when test="${bannerDTO.position eq 'T' }">상단배너</c:when>
					<c:when test="${bannerDTO.position eq 'B' }">하단배너</c:when>
					<c:when test="${bannerDTO.position eq 'I' }">사업안내</c:when>
					<c:otherwise></c:otherwise>
				</c:choose>
				</td>
			</tr>
			<tr>
				<th>사용여부</th>
				<td class="left">
				<c:choose>
					<c:when test="${bannerDTO.use_yn eq 'Y' }">사용</c:when>
					<c:otherwise>사용안함</c:otherwise>
				</c:choose>
				</td>
			</tr>
		</tbody>
	</table>
	</div>
	
	<!-- button area -->
	<div class="btn-all-align">
		<p class="adm_listBtn">
			<span class="btn-align">
			<a href="bannerWrite.do?mode=M&banner_id=${bannerDTO.banner_id}&amp;${searchParam}">
				<img src="/images/operation/btn/editBtn02.gif" alt="수정화면 이동" class="update"/>
			</a>
			<a href="bannerList.do?${searchParam}" title="목록화면 이동">
				<img src="/images/operation/btn/listBtn.gif" alt="목록화면 이동" />
			</a>
			</span>
		</p>
	</div>
<jsp:include page="/operation/foot.do"></jsp:include>