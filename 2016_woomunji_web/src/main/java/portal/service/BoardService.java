package portal.service;

import java.sql.SQLException;

import portal.dto.BoardDTO;
import portal.dto.ListDTO;

public interface BoardService {
	
	/**
	 * 게시판 목록(관리자)
	 * @param loginDTO
	 * @return
	 * @throws SQLException
	 */
	public ListDTO boardList(BoardDTO boardDTO) throws Exception;
	
	
	
	/**
	 * 게시판 등록
	 * @param boardDTO
	 * @return
	 * @throws Exception
	 */
	public int insertBoard(BoardDTO boardDTO) throws Exception;
	
	
	/**
	 * 회원정보조회
	 * @param boardDTO
	 * @return
	 * @throws Exception
	 */
	public BoardDTO boardView(BoardDTO boardDTO) throws Exception;
	
	
	/**
	 * 비밀번호 초기화
	 * @param boardDTO
	 * @throws Exception
	 */
	public int updateBoard(BoardDTO boardDTO) throws Exception;

}
