package portal.service.serviceImpl;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import portal.dao.CultureDAO;
import portal.dto.CultureDTO;
import portal.dto.ListDTO;
import portal.service.CultureService;

@Service("cultureService")
public class CultureServiceImpl implements CultureService{
	@Resource(name="cultureDao")
	private CultureDAO cultureDao;
	
	/**
	 * 문화재 목록 
	 */ 
	@Override 
	public ListDTO cultureList(CultureDTO cultureDTO) throws Exception{
		return cultureDao.cultureList(cultureDTO);
	} 
	
	
	/**
	 * 문화재 상세조회
	 * @param cultureDTO
	 * @return
	 * @throws SQLException
	 */
	@Override 
	public CultureDTO cultureView(CultureDTO cultureDTO) throws SQLException{
		return cultureDao.cultureView(cultureDTO);
	}
	
	
	/**
	 * 문화재 이미지 목록
	 * @param cultureDTO
	 * @return
	 * @throws SQLException
	 */
	@Override 
	public List<CultureDTO> cultureImageList(CultureDTO cultureDTO) throws SQLException{
		return cultureDao.cultureImageList(cultureDTO);
	}
	
	/**
	 * 문화재 게시여부 변경
	 * @param cultureDTO
	 * @return
	 * @throws SQLException
	 */
	@Override 
	public int postUpdate(CultureDTO cultureDTO) throws SQLException{
		return cultureDao.postUpdate(cultureDTO);
	}
}
