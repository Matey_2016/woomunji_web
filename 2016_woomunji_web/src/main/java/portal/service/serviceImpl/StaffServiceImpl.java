package portal.service.serviceImpl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import portal.dto.ListDTO;
import portal.dao.StaffDAO;
import portal.dto.StaffDTO;
import portal.service.StaffService;

@Service("staffService")
@Transactional(rollbackFor={Exception.class})

public class StaffServiceImpl implements StaffService {
	@Resource(name="staffDAO")
	private StaffDAO staffDAO;
	
	
	/**
	 * 운영자 로그인
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public StaffDTO staffLoginDo(StaffDTO staffDTO) throws Exception{
		return staffDAO.staffLoginDo(staffDTO);		
	}
	
	
	/**
	 * 운영자 리스트 조회
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public ListDTO selectStaffList(StaffDTO staffDto) throws Exception{
		return staffDAO.selectStaffList(staffDto);		
	}
	
	/**
	 * 운영자 정보 조회
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public StaffDTO selectStaffInfo(StaffDTO staffDto) throws Exception{
		return staffDAO.selectStaffInfo(staffDto);
	}

	/**
	 * 운영자 수정
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public int updateStaffInfo(StaffDTO staffDto) throws Exception{
		return staffDAO.updateStaffInfo(staffDto);				
	}

	/**
	 * 운영자 등록
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public int insertStaffInfo(StaffDTO staffDto) throws Exception{
		return staffDAO.insertStaffInfo(staffDto);
	}

	/**
	 * 운영자별 관리권한 정보 호출
	 * @param staffDTO
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<StaffDTO> selectStaffAuthInfoList(StaffDTO staffDto) throws Exception{
		return staffDAO.selectStaffAuthInfoList(staffDto);
		
	}
	
	/**
	 * 운영자 메뉴 설정 초기화
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public int initStaffAuth(StaffDTO staffDto) throws Exception{
		return staffDAO.initStaffAuth(staffDto);
	}
	
	/**
	 * 운영자 메뉴 설정 등록
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public int insertStaffAuth(StaffDTO staffDto) throws Exception{
		return staffDAO.insertStaffAuth(staffDto);
	}
	
	
	/**
	 * 운영자 아이디 중복체크
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public int checkStaffId(StaffDTO staffDTO) throws Exception{
		return staffDAO.checkStaffId(staffDTO);
	}
	

}
