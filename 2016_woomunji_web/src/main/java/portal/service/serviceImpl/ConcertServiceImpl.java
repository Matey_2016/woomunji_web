package portal.service.serviceImpl;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import portal.dao.ConcertDAO;
import portal.dto.ConcertDTO;
import portal.dto.ListDTO;
import portal.service.ConcertService;

@Service("concertService")
public class ConcertServiceImpl implements ConcertService{
	@Resource(name="concertDao")
	private ConcertDAO concertDao;
	
	/**
	 * 공연전시 목록 
	 */ 
	@Override 
	public ListDTO concertList(ConcertDTO concertDTO) throws Exception{
		return concertDao.concertList(concertDTO);
	} 
	
	
	/**
	 * 공연전시 상세조회
	 * @param concertDTO
	 * @return
	 * @throws SQLException
	 */
	@Override 
	public ConcertDTO concertView(ConcertDTO concertDTO) throws SQLException{
		return concertDao.concertView(concertDTO);
	}
	
	
	/**
	 * 공연전시 게시여부 변경
	 * @param concertDTO
	 * @return
	 * @throws SQLException
	 */
	@Override 
	public int postUpdate(ConcertDTO concertDTO) throws SQLException{
		return concertDao.postUpdate(concertDTO);
	}
}
