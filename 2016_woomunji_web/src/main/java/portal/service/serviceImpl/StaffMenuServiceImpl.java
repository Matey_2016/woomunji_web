package portal.service.serviceImpl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import portal.dao.StaffMenuDAO;
import portal.dto.StaffMenuDTO;
import portal.service.StaffMenuService;

@Service("staffMenuService")
@Transactional(rollbackFor={Exception.class})
public class StaffMenuServiceImpl implements StaffMenuService{
	@Resource(name="staffMenuDao")
	private StaffMenuDAO staffMenuDao;
		
	
	@Override
	public int selectStaffMenuCnt() throws Exception {
		return staffMenuDao.selectStaffMenuCnt();
	}

	/**
	 * 메뉴 리스트 조회
	 * @param staffMenuVO
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<StaffMenuDTO> selectStaffMenuList(StaffMenuDTO staffMenuDTO) throws Exception{
		return staffMenuDao.selectStaffMenuList(staffMenuDTO);		 
	}
	
	/**
	 * 메뉴 정보 조회
	 * @param staffMenuVO
	 * @return
	 * @throws Exception
	 */
	@Override
	public StaffMenuDTO selectStaffMenuInfo(StaffMenuDTO staffMenuDTO) throws Exception{
		return staffMenuDao.selectStaffMenuInfo(staffMenuDTO);
	}

	/**
	 * 메뉴 수정
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public int updateStaffMenuInfo(StaffMenuDTO staffMenuDTO) throws Exception{
		return staffMenuDao.updateStaffMenuInfo(staffMenuDTO);				
	}

	/**
	 * 메뉴 등록
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public int insertStaffMenuInfo(StaffMenuDTO staffMenuDTO) throws Exception{
		return staffMenuDao.insertStaffMenuInfo(staffMenuDTO);
	}
	
	
	/**
	 * lnb를 완성하기 위한 관리자별 메뉴 가져오기
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<StaffMenuDTO> staffMenuCheckList(StaffMenuDTO staffMenuDTO) throws Exception{
		return staffMenuDao.staffMenuCheckList(staffMenuDTO);		
	}
	
	
	/**
	 * 메뉴 접근권한 조회
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public StaffMenuDTO getMenuAuth(StaffMenuDTO staffMenuDTO) throws Exception{
		return staffMenuDao.getMenuAuth(staffMenuDTO);
	}
	
	
	/**
	 * 메뉴 접근권한 조회
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<StaffMenuDTO> getTopMenu(StaffMenuDTO staffMenuDTO) throws Exception{
		return staffMenuDao.getTopMenu(staffMenuDTO);
	}
	
	
	/**
	 * gnb를 완성하기 위한 관리자별 메뉴 가져오기
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<StaffMenuDTO> staffGnbMenuMake(StaffMenuDTO staffMenuDTO) throws Exception{
		return staffMenuDao.staffGnbMenuMake(staffMenuDTO);		
	}
	
	/**
	 * 메뉴명 가져오기
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public StaffMenuDTO selectMenuName(StaffMenuDTO staffMenuDTO) throws Exception{
		return staffMenuDao.selectMenuName(staffMenuDTO);		
	}

}
