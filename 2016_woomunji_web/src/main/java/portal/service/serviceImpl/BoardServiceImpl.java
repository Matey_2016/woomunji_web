package portal.service.serviceImpl;

import java.sql.SQLException;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import portal.dao.BoardDAO;
import portal.dto.BoardDTO;
import portal.dto.ListDTO;
import portal.service.BoardService;

@Service("boardService")
public class BoardServiceImpl implements BoardService{
	@Resource(name="boardDao")
	private BoardDAO boardDao;
	
	/**
	 * 게시판목록(관리자) 
	 */ 
	@Override 
	public ListDTO boardList(BoardDTO boardDTO) throws Exception{
		return boardDao.boardList(boardDTO);
	} 
	
	
	/**
	 * 게시판 등록
	 * @param boardDTO
	 * @return
	 * @throws Exception
	 */
	public int insertBoard(BoardDTO boardDTO) throws Exception{
		return boardDao.insertBoard(boardDTO);
	} 
	
	
	
	/**
	 * 게시판정보 조회
	 */
	@Override 
	public BoardDTO boardView(BoardDTO boardDTO) throws Exception{
		return boardDao.boardView(boardDTO);
	} 
	
	
	/**
	 * 게시판 수정
	 * @param boardDTO
	 * @throws SQLException
	 */
	@Override 
	public int updateBoard(BoardDTO boardDTO) throws Exception{
		return boardDao.updateBoard(boardDTO);
	} 
	
	
	
}
