package portal.service.serviceImpl;

import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import portal.dao.PostDAO;
import portal.dto.AttachedFileDTO;
import portal.dto.ListDTO;
import portal.dto.PostDTO;
import portal.service.PostService;



@Service("postService")
public class PostServiceImpl implements PostService{
	@Resource(name="postDao")
	private PostDAO postDao;
	
	/**
	 * 게시물목록(관리자) 
	 */ 
	@Override 
	public ListDTO postAdminList(PostDTO postDTO) throws Exception{
		return postDao.postAdminList(postDTO);
	} 
	
	
	/**
	 * post_id 채번  
	 */ 
	@Override 
	public String selectPostId() throws Exception{
		return postDao.selectPostId();
	} 
	
	
	/**
	 * 이전글 다음글 목록
	 */
	@Override 
	public List<PostDTO> prevNextList(PostDTO postDTO) throws Exception{
		return postDao.prevNextList(postDTO);
	} 
	
	
	/**
	 * 추천게시물 목록
	 */
	@Override 
	public List<PostDTO> topList(PostDTO postDTO) throws Exception{
		return postDao.topList(postDTO);
	} 
	
	
	/**
	 * 썸네일 게시물목록(관리자) 
	 */ 
	@Override 
	public ListDTO postThumbList(PostDTO postDTO) throws Exception{
		return postDao.postThumbList(postDTO);
	} 
	
	
	/**
	 * 첨부파일 목록
	 */
	@Override 
	public List<AttachedFileDTO> fileList(PostDTO postDTO) throws Exception{
		return postDao.fileList(postDTO);
	}
	
	
	
	/**
	 * 게시물 등록
	 * @param postDTO
	 * @return
	 * @throws Exception
	 */
	public int insertPost(PostDTO postDTO) throws Exception{
		return postDao.insertPost(postDTO);
	} 
	
	
	/**
	 * 게시물 등록 후 부모글 정보 수정
	 * @param postDTO
	 * @return
	 * @throws Exception
	 */
	public int updateParentPostId(PostDTO postDTO) throws Exception{
		return postDao.updateParentPostId(postDTO);
	} 
	
	
	/**
	 * 게시물 조회
	 */
	@Override 
	public PostDTO postView(PostDTO postDTO) throws Exception{
		return postDao.postView(postDTO);
	} 
	
	
	/**
	 * 게시물 수정
	 * @param postDTO
	 * @throws SQLException
	 */
	@Override 
	public int updatePost(PostDTO postDTO) throws Exception{
		return postDao.updatePost(postDTO);
	} 
	
	/**
	 * 첨부파일 등록
	 */
	@Override
	public int insertAttFile(List<AttachedFileDTO> fileList) throws Exception {// 새로운 파일 정보 등록
		
		int result = 0;
		if ( fileList != null ) {
			for(AttachedFileDTO i :fileList){
				result = postDao.insertAttFile(i);
			}
		}
		
		return result;
	}
	
	
	/**
	 * 첨부파일 삭제
	 */
	public int deleteAttFile(PostDTO postDTO, String[] delfileId) throws Exception {
		
		int result = 1;
		
		// 선택된 파일 삭제( 
		if(delfileId != null && delfileId.length > 0){
			AttachedFileDTO tmpDto = new AttachedFileDTO();
			tmpDto.setDelFileId(delfileId);
			tmpDto.setParentId(postDTO.getPost_id());
			
			// 1. 삭제할 정보를 미리 불러오고
			//List<AttachedFileDTO> delFileList = postDao.selectedFileList(tmpDto); 
			
			// 2. 해당 정보 삭제 
			result = postDao.deleteAddFile(tmpDto);
			
			// 3. 2번을 정상 통과한 후에 실제 파일 삭제
//			for(AttachedFileDTO i :delFileList){
//				//FileUploadUtil.forceDelete(Constants.UPLOAD_PATH+i.getFilePath()+Constants.FILE_SEPARATOR+i.getSysFileName());
//			}			
		}	
		
		return result;
		
	}
	
	
	/**
	 * 게시물,파일, 댓글 일괄삭제
	 */
	public int deletePost(PostDTO postDTO) throws Exception {
		return postDao.deletePost(postDTO);
	}
	
	
	/**
	 * 코멘트 목록
	 * @param postDTO
	 * @return
	 * @throws Exception
	 */
	public List<PostDTO> commentList(PostDTO postDTO) throws Exception{
		return postDao.commentList(postDTO);
	}
	
	
	/**
	 * 코멘트 등록
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public int commentInsert(PostDTO postDTO) throws SQLException{
		return postDao.commentInsert(postDTO);
	}
	

	/**
	 * 코멘트 삭제
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public int commentDelete(PostDTO postDTO) throws SQLException{
		return postDao.commentDelete(postDTO);
	}
	
	
	
	/**
	 * 게시물목록(관리자)
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public ListDTO useList(PostDTO postDTO) throws SQLException{
		return postDao.useList(postDTO); 
	}
	
	
	/**
	 * 게시물 답변 조회
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public PostDTO postReplyView(PostDTO postDTO) throws SQLException{
		return postDao.postReplyView(postDTO);
	}
	
}
