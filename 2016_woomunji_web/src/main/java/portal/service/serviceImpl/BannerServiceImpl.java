package portal.service.serviceImpl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import portal.dto.AttachedFileDTO;
import portal.dto.ListDTO;
import portal.dao.BannerDAO;
import portal.dto.BannerDTO;
import portal.service.BannerService;

@Service("bannerService")
public class BannerServiceImpl implements BannerService {
	
	@Resource(name="bannerDao")
	private BannerDAO bannerDao;
	
	public ListDTO selectBannerList(BannerDTO bannerDto) throws Exception{ 
		return bannerDao.selectBannerList(bannerDto);
	}

	@Override
	public BannerDTO selectBannerInfo(BannerDTO bannerDto) throws Exception {
		return bannerDao.selectBannerInfo(bannerDto);
	}

	@Override
	public int updateBannerInfo(BannerDTO bannerDto) throws Exception {
		return bannerDao.updateBannerInfo(bannerDto);
	}
	
	@Override
	public int updateBannerFile(BannerDTO bannerDTO) throws Exception {
		return bannerDao.updateBannerFile(bannerDTO);
	}

	@Override
	public int insertBannerInfo(BannerDTO bannerDto) throws Exception {
		return bannerDao.insertBannerInfo(bannerDto);
	}

	@Override
	public int deleteBannerInfo(BannerDTO bannerDto) throws Exception {
		return bannerDao.deleteBannerInfo(bannerDto);
	}
	
	/**
	 * 사용할 배너 목록
	 * @param bannerDto
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<BannerDTO> selectUsingList(BannerDTO bannerDto) throws Exception{
		return bannerDao.selectUsingList(bannerDto);
	}
	
	
}
