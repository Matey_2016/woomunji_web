package portal.service.serviceImpl;

import java.sql.SQLException;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import portal.dto.ListDTO;
import portal.dao.UserDAO;
import portal.dto.UserDTO;
import portal.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService{
	@Resource(name="userDao")
	private UserDAO userDao;
	
	/**
	 * 회원목록(관리자)
	 */ 
	@Override 
	public ListDTO userList(UserDTO userDTO) throws Exception{
		return userDao.userList(userDTO);
	} 
	
	
	/**
	 * 회원정보 조회(관리자)
	 */
	@Override 
	public UserDTO userView(UserDTO userDTO) throws Exception{
		return userDao.userView(userDTO);
	} 
	
	
	/**
	 * 비밀번호 초기화(관리자)
	 */
	@Override 
	public int initPasswd(UserDTO userDTO) throws Exception{
		return userDao.initPasswd(userDTO);
	} 
	
	
	/**
	 * 비밀번호 초기화(관리자)
	 */
	@Override 
	public int lastLogin(UserDTO userDTO) throws Exception{
		return userDao.lastLogin(userDTO);
	} 
	
	/**
	 * 아이디 중복체크(사용자)
	 */
	@Override 
	public int idCheck(UserDTO userDTO) throws Exception{
		return userDao.idCheck(userDTO);
	} 
	
	
	/**
	 * 아이디 중복체크(사용자)
	 */
	@Override 
	public int certCheck(UserDTO userDTO) throws Exception{
		return userDao.certCheck(userDTO);
	} 
	
	
	/**
	 * 회원정보등록(사용자)
	 */
	@Override 
	public int insertUser(UserDTO userDTO) throws SQLException{
		return userDao.insertUser(userDTO);
	}
	
	
	/**
	 * 사용자 관심분야 저장
	 */
	@Override 
	public int interestInsert(UserDTO userDTO) throws SQLException{
		return userDao.interestInsert(userDTO);
	}
	
	
	/**
	 * 사용자 관심분야 삭제
	 */
	@Override 
	public int interestDelete(UserDTO userDTO) throws SQLException{
		return userDao.interestDelete(userDTO);
	}
	
	
	/**
	 * 회원정보수정(사용자)
	 */
	@Override 
	public int updateUser(UserDTO userDTO) throws SQLException{
		return userDao.updateUser(userDTO);
	}
	
	
	/**
	 * 회원정보수정(사용자)
	 */
	@Override 
	public int updateUserCert(UserDTO userDTO) throws SQLException{
		return userDao.updateUserCert(userDTO);
	}
	
	
	/**
	 * 로그인
	 */
	@Override 
	public UserDTO login(UserDTO userDTO) throws SQLException{
		return userDao.login(userDTO);
	}
	
	
	/**
	 * 아이디/비밀번호 찾기
	 */
	@Override 
	public UserDTO findUser(UserDTO userDTO) throws SQLException{
		return userDao.findUser(userDTO);
	}
	
	
	/**
	 * 회원탈퇴
	 */
	@Override 
	public UserDTO leaveUser(UserDTO userDTO) throws SQLException{
		return userDao.leaveUser(userDTO);
	}
}
