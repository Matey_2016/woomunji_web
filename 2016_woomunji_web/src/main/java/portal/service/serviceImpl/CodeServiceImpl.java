package portal.service.serviceImpl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import portal.dto.ListDTO;
import portal.dao.CodeDAO;
import portal.service.CodeService;
import portal.dto.CodeDTO;

@Service("codeService")
public class CodeServiceImpl implements CodeService {
	
	@Resource(name="codeDAO")
	private CodeDAO codeDAO;

    /**
     * 공통코드  리스트조회
     * @param comcodeVO
     * @return
     * @throws Exception
     */

	@Override
	public List<CodeDTO> mainCodeList() throws Exception {
		return codeDAO.mainCodeList();
	}
    
	/**
     * 공통코드 사용 상위 코드로 하위 코드 겁색
     * @param highCd
     * @return
     * @throws Exception
     */
	@Override
	public List<CodeDTO> subCodeList(CodeDTO codeDTO) throws Exception {
		return codeDAO.subCodeList(codeDTO);
	}
	
	/**
	 * 공통코드 정보 조회
	 * @param comcodeVO
	 * @return
	 * @throws Exception
	 */
  
	@Override
	public CodeDTO codeView(CodeDTO codeDTO) throws Exception {
		return codeDAO.codeView(codeDTO);
	}
	
	/**
	 * 공통코드 수정
	 * @param comcodeVO
	 * @return
	 * @throws Exception
	 */

	@Override
	public int codeUpdate(CodeDTO codeDTO) throws Exception {
		return codeDAO.codeUpdate(codeDTO);
	}
    
	/**
	 * 공통코드 등록
	 * @param comcodeVO
	 * @return
	 * @throws Exception
	 */

	@Override
	public int codeInsert(CodeDTO codeDTO) throws Exception {
		return codeDAO.codeInsert(codeDTO);
	}
    
	/**
	 * 상위 코드 조회
	 * @param highCd
	 * @return
	 * @throws Exception
	 */
	@Override
	public String selectSuperCd(String highId) throws Exception {
		
		return codeDAO.selectSuperCd(highId);
	}
    
	/**
	 * 특정 HighCode에 대한 Code 리스트 반환
	 * @param comcodeVO
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<CodeDTO> getCodeList(CodeDTO codeDTO) throws Exception {
		return codeDAO.getCodeList(codeDTO);
	}
    
	
	/**
     * 공통코드 사용 상위 코드로 하위 코드 겁색
     * @param highCd
     * @return
     * @throws Exception
     */
	@Override
	public List<CodeDTO> jsonCodeList(CodeDTO codeDTO) throws Exception {
		return codeDAO.jsonCodeList(codeDTO);
	}
	
	
	
	
	
	
	
	
	/**
	 * 특정 HighCode에 대한 Code 리스트 중 일부 데이터 제외한 값만 반환 
	 * @param comcodeVO
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<CodeDTO> getCodeListExcept(CodeDTO codeDTO)
			throws Exception {
		
		return codeDAO.getCodeListExcept(codeDTO);
	}
    

	/**
	 * 특정 HighCode에 대한 Code 리스트 중 선택된 데이터 값만 반환 
	 * @param comcodeVO
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<CodeDTO> getCodeListInclude(CodeDTO codeDTO)
			throws Exception {
		
		return codeDAO.getCodeListInclude(codeDTO);
	}
    
	/**
	 * 코드목록중  CODE_ETC값에 따른 목록 호출
	 * @param comcodeVO
	 * @return
	 * @throws Exception
	 */
	@Override
	public List<CodeDTO> getCodeListSpecial(CodeDTO codeDTO)
			throws Exception {
		
		return codeDAO.getCodeListSpecial(codeDTO);
	}
    
	/**
	 * 공통코드 중복체크
	 * @param comcodeVO
	 * @return
	 * @throws Exception
	 */
	@Override
	public CodeDTO selectDuplCodeInfo(CodeDTO codeDTO) throws Exception {
		
		return codeDAO.selectDuplCodeInfo(codeDTO);
	}

	public CodeDAO getCodeDAO() {
		return codeDAO;
	}

	public void setCodeDAO(CodeDAO codeDAO) {
		this.codeDAO = codeDAO;
	}

	@Override
	public List<CodeDTO> getUserMenu() throws Exception {
		
		return codeDAO.getUserMenu();
	}
	
	
	
	

}
