package portal.service;

import java.util.List;

import portal.dto.ListDTO;
import portal.dto.StaffDTO;

public interface StaffService {
	
	/**
	 * 운영자 로그인
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public StaffDTO staffLoginDo(StaffDTO staffDTO) throws Exception;
	
	
	/**
	 * 운영자 리스트 조회
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public ListDTO selectStaffList(StaffDTO staffDto) throws Exception;
	
	/**
	 * 운영자 정보 조회
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public StaffDTO selectStaffInfo(StaffDTO staffDto) throws Exception;

	/**
	 * 운영자 수정
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public int updateStaffInfo(StaffDTO staffDto) throws Exception;

	/**
	 * 운영자 등록
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public int insertStaffInfo(StaffDTO staffDto) throws Exception;
	
	/**
	 * 운영자별 관리권한 정보 호출
	 * @param staffDTO
	 * @return
	 * @throws Exception
	 */
	public List<StaffDTO> selectStaffAuthInfoList(StaffDTO staffDto) throws Exception;
	
	/**
	 * 운영자 메뉴 설정 초기화
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public int initStaffAuth(StaffDTO staffDto) throws Exception;
	
	/**
	 * 운영자 메뉴 설정 등록
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public int insertStaffAuth(StaffDTO staffDto) throws Exception;
	
	
	/**
	 * 운영자 아이디 중복체크
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public int checkStaffId(StaffDTO staffDTO) throws Exception;
}
