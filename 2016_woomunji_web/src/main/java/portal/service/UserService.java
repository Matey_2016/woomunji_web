package portal.service;

import java.sql.SQLException;

import portal.dto.ListDTO;
import portal.dto.UserDTO;

public interface UserService {
	
	/**
	 * 회원 목록(관리자)
	 * @param loginDTO
	 * @return
	 * @throws SQLException
	 */
	public ListDTO userList(UserDTO userDTO) throws Exception;
	
	
	/**
	 * 회원정보조회(관리자)
	 * @param userDTO
	 * @return
	 * @throws Exception
	 */
	public UserDTO userView(UserDTO userDTO) throws Exception;
	
	
	/**
	 * 비밀번호 초기화(관리자)
	 * @param userDTO
	 * @throws Exception
	 */
	public int initPasswd(UserDTO userDTO) throws Exception;
	
	
	/**
	 * 비밀번호 초기화(관리자)
	 */
	public int lastLogin(UserDTO userDTO) throws Exception;
	
	
	/**
	 * 아이디 중복체크(사용자)
	 * @param userDTO
	 * @return
	 * @throws Exception
	 */
	public int idCheck(UserDTO userDTO) throws Exception;
	
	
	/**
	 * 아이디 중복체크(사용자)
	 * @param userDTO
	 * @return
	 * @throws Exception
	 */
	public int certCheck(UserDTO userDTO) throws Exception;
	
	
	
	/**
	 * 회원정보등록(사용자)
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public int insertUser(UserDTO userDTO) throws SQLException;
	
	
	/**
	 * 사용자 관심분야 삭제
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public int interestDelete(UserDTO userDTO) throws SQLException;
	
	
	/**
	 * 사용자 관심분야 저장
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public int interestInsert(UserDTO userDTO) throws SQLException;
	
	
	/**
	 * 회원정보수정(사용자)
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public int updateUser(UserDTO userDTO) throws SQLException;
	
	
	/**
	 * 회원정보수정(사용자)
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public int updateUserCert(UserDTO userDTO) throws SQLException;
	
	
	/**
	 * 로그인
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public UserDTO login(UserDTO userDTO) throws SQLException;
	
	
	/**
	 * 아이디/비밀번호 찾기
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public UserDTO findUser(UserDTO userDTO) throws SQLException;
	
	
	/**
	 * 회원탈퇴
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public UserDTO leaveUser(UserDTO userDTO) throws SQLException;
}
