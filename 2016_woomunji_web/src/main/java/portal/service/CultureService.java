package portal.service;

import java.sql.SQLException;
import java.util.List;

import portal.dto.CultureDTO;
import portal.dto.ListDTO;

public interface CultureService {
	
	/**
	 * 문화재목록
	 * @param CultureDTO
	 * @return
	 * @throws SQLException
	 */
	public ListDTO cultureList(CultureDTO cultureDTO) throws Exception;
	
	
	/**
	 * 문화재 상세조회
	 * @param cultureDTO
	 * @return
	 * @throws SQLException
	 */
	public CultureDTO cultureView(CultureDTO cultureDTO) throws SQLException;
	
	
	/**
	 * 문화재 이미지 목록
	 * @param cultureDTO
	 * @return
	 * @throws SQLException
	 */
	public List<CultureDTO> cultureImageList(CultureDTO cultureDTO) throws SQLException;
	
	
	/**
	 * 문화재 게시여부 변경
	 * @param cultureDTO
	 * @return
	 * @throws SQLException
	 */
	public int postUpdate(CultureDTO cultureDTO) throws SQLException;
}
