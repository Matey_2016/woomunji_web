package portal.service;

import java.util.List;

import portal.dto.AttachedFileDTO;
import portal.dto.ListDTO;
import portal.dto.BannerDTO;

public interface BannerService {

	/**
	 * 베너 관리자 리스트
	 * @param bannerDto
	 * @return
	 * @throws Exception
	 */
	public ListDTO selectBannerList(BannerDTO bannerDto) throws Exception;
	
	/**
	 * 베너 상세정보
	 * @param bannerDto
	 * @return
	 * @throws Exception
	 */
	public BannerDTO selectBannerInfo(BannerDTO bannerDto) throws Exception;
	
	/**
	 * 베너 수정
	 * @param bannerDto
	 * @return
	 * @throws Exception
	 */
	public int updateBannerInfo(BannerDTO bannerDto) throws Exception;
	
	
	/**
	 * 베너 수정
	 * @param bannerDto
	 * @return
	 * @throws Exception
	 */
	public int updateBannerFile(BannerDTO bannerDTO) throws Exception;
	
	
	/**
	 * 베너 등록
	 * @param bannerDto
	 * @return
	 * @throws Exception
	 */
	public int insertBannerInfo(BannerDTO bannerDto) throws Exception;
	
	/**
	 * 베너 삭제
	 * @param bannerDto
	 * @return
	 * @throws Exception
	 */
	public int deleteBannerInfo(BannerDTO bannerDto) throws Exception;
	
	/**
	 * 사용할 배너 목록
	 * @param bannerDto
	 * @return
	 * @throws Exception
	 */
	public List<BannerDTO> selectUsingList(BannerDTO bannerDto) throws Exception;
}
