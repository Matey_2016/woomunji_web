package portal.service;

import java.sql.SQLException;
import java.util.List;

import portal.dto.AttachedFileDTO;
import portal.dto.ListDTO;
import portal.dto.PostDTO;

public interface PostService {
	
	/**
	 * 게시판 목록(관리자)
	 * @param loginDTO
	 * @return
	 * @throws SQLException
	 */
	public ListDTO postAdminList(PostDTO postDTO) throws Exception;
	
	/**
	 * post_id 채번
	 * @return
	 * @throws Exception
	 */
	public String selectPostId() throws Exception;
	
	/**
	 * 이전글/다음글 목록
	 * @param postDTO
	 * @return
	 * @throws Exception
	 */
	public List<PostDTO> prevNextList(PostDTO postDTO) throws Exception;
	
	/**
	 * 추천게시물 목록
	 * @param postDTO
	 * @return
	 * @throws Exception
	 */
	public List<PostDTO> topList(PostDTO postDTO) throws Exception;
	
	
	/**
	 * 썸네일게시판 목록(관리자)
	 * @param loginDTO
	 * @return
	 * @throws SQLException
	 */
	public ListDTO postThumbList(PostDTO postDTO) throws Exception;
	
	
	/**
	 * 첨부파일 목록
	 * @param postDTO
	 * @return
	 * @throws Exception
	 */
	public List<AttachedFileDTO> fileList(PostDTO postDTO) throws Exception;
	
	/**
	 * 게시물 등록
	 * @param postDTO
	 * @return
	 * @throws Exception
	 */
	public int insertPost(PostDTO postDTO) throws Exception;
	
	
	/**
	 * 게시물 등록 후 부모글 정보 수정
	 * @param postDTO
	 * @return
	 * @throws Exception
	 */
	public int updateParentPostId(PostDTO postDTO) throws Exception;
	
	
	/**
	 * 회원정보조회
	 * @param postDTO
	 * @return
	 * @throws Exception
	 */
	public PostDTO postView(PostDTO postDTO) throws Exception;
	
	
	/**
	 * 비밀번호 초기화
	 * @param postDTO
	 * @throws Exception
	 */
	public int updatePost(PostDTO postDTO) throws Exception;

	
	/**
	 * 첨부파일 등록
	 * @param fileList
	 * @throws Exception
	 */
	public int insertAttFile(List<AttachedFileDTO> fileList) throws Exception ;
	
	
	/**
	 * 첨부파일 삭제
	 * @param fileList
	 * @return
	 * @throws Exception
	 */
	public int deleteAttFile(PostDTO postDTO, String[] delfileId) throws Exception ;
	
	/**
	 * 게시물,파일, 댓글 일괄삭제
	 * @param postDTO
	 * @return
	 * @throws Exception
	 */
	public int deletePost(PostDTO postDTO) throws Exception ;
	
	
	/**
	 * 코멘트 목록
	 * @param postDTO
	 * @return
	 * @throws Exception
	 */
	public List<PostDTO> commentList(PostDTO postDTO) throws Exception;
	
	
	/**
	 * 코멘트 등록
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public int commentInsert(PostDTO postDTO) throws SQLException;
	

	/**
	 * 코멘트 삭제
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public int commentDelete(PostDTO postDTO) throws SQLException;
	
	
	/**
	 * 게시물목록(관리자)
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public ListDTO useList(PostDTO postDTO) throws SQLException;
	
	
	
	/**
	 * 게시물 답변 조회
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public PostDTO postReplyView(PostDTO postDTO) throws SQLException;
	
	
	
	
	
	
}
