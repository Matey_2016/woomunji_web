/*------------------------------------------------------------------------------
 * NAME : CommonServiece.
 * DESC : 공통코드 관리를 위한 비지니스단.
 * VER  : v1.0
 * PROJ : DAEDONG System
 * Copyright Digital Bay System All rights reserved
 *------------------------------------------------------------------------------
 *                  변         경         사         항
 *------------------------------------------------------------------------------
 *    DATE     AUTHOR                      DESCRIPTION
 *-----------  ------  ---------------------------------------------------------
 * 2015.08.11  이돈수(미디어젠)   최초 프로그램 작성
 *------------------------------------------------------------------------------*/
package portal.service;

import java.util.List;

import portal.dto.CodeDTO;

public interface CodeService {
	
    /**
     * 공통코드 리스트 조회
     * @param comcodeDTO
     * @return
     * @throws Exception
     */
	public List<CodeDTO> mainCodeList()throws Exception;
	
	
	
	/**
	 * 공통코드 사용 상위 코드로 하위 코드 겁색
	 * @param highCd
	 * @return
	 * @throws Exception
	 */
	public List<CodeDTO> subCodeList(CodeDTO codeDTO) throws Exception;
	
	
	/**
	 * 공통코드 정보 조회
	 * @param comcodeDTO
	 * @return
	 * @throws Exception
	 */
	public CodeDTO codeView(CodeDTO  codeDTO)throws Exception;
	
	/**
	 * 공통코드 수정
	 * @param comcodeDTO
	 * @return
	 * @throws Exception
	 */
	
	public int codeUpdate(CodeDTO  codeDTO)throws Exception;
	
	/**
	 * 공통코드 등록
	 * @param comcodeDTO
	 * @return
	 * @throws Exception
	 */
	public int codeInsert(CodeDTO codeDTO)throws Exception;
	
	
	/**
	 * 상위 코드 조회
	 * @param highId
	 * @return
	 * @throws Exception
	 */
	public String selectSuperCd(String highId)throws Exception;
	
	
	/**
	 * 특정 최상위코드에 대한 코드 리스트 반환..
	 * @param comcodeDTO
	 * @return
	 * @throws Exception
	 */
	public List<CodeDTO> getCodeList(CodeDTO codeDTO)throws Exception;
	
	
	
	/**
	 * 특정 최상위코드에 대한 코드 리스트 반환..
	 * @param comcodeDTO
	 * @return
	 * @throws Exception
	 */
	public List<CodeDTO> jsonCodeList(CodeDTO codeDTO)throws Exception;
	
	
	
	
	
	
	
	
	/**
	 * 특정 최상위 코드에 대한 코드 리스트중 일부 데이만 제외하고 반환.
	 * @param comcodeDTO
	 * @return
	 * @throws Exception
	 */
	public List<CodeDTO> getCodeListExcept(CodeDTO codeDTO)throws Exception;
	
	/**
	 * 특정 최상위 코드에 대한 코드 리스트 중 선택된 값만 반환.
	 * @param comcodeDTO
	 * @return
	 * @throws Exception
	 */
	
	public List<CodeDTO> getCodeListInclude(CodeDTO codeDTO)throws Exception;
	
	/**
	 * 코드목록중 CODE_ETC값에 따른 목록 호출
	 * @param comcodeDTO
	 * @return
	 * @throws Exception
	 */
	
	public List<CodeDTO> getCodeListSpecial(CodeDTO codeDTO)throws Exception;
	
	
	/**
	 * 공통코드 중복 체크
	 * @param comcodeDTO
	 * @return
	 * @throws Exception
	 */
	public CodeDTO selectDuplCodeInfo(CodeDTO codeDTO)throws Exception;
	
	
	/**
	 * 공통코드에 정의된 사용자 메뉴 정보
	 * @return
	 * @throws Exception
	 */
	public List<CodeDTO> getUserMenu()throws Exception;
	
}
