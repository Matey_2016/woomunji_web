package portal.service;

import java.sql.SQLException;

import portal.dto.ConcertDTO;
import portal.dto.ListDTO;

public interface ConcertService {
	
	/**
	 * 공연전시목록
	 * @param ConcertDTO
	 * @return
	 * @throws SQLException
	 */
	public ListDTO concertList(ConcertDTO concertDTO) throws Exception;
	
	
	/**
	 * 공연전시 상세조회
	 * @param concertDTO
	 * @return
	 * @throws SQLException
	 */
	public ConcertDTO concertView(ConcertDTO concertDTO) throws SQLException;
	
	
	/**
	 * 공연전시 게시여부 변경
	 * @param concertDTO
	 * @return
	 * @throws SQLException
	 */
	public int postUpdate(ConcertDTO concertDTO) throws SQLException;
}
