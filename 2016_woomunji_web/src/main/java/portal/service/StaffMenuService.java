package portal.service;

import java.util.List;

import portal.dto.StaffMenuDTO;

public interface StaffMenuService {
	
	public int selectStaffMenuCnt() throws Exception;
	/**
	 * 메뉴 리스트 조회
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public List<StaffMenuDTO> selectStaffMenuList(StaffMenuDTO staffMenuDTO) throws Exception;
	
	/**
	 * 메뉴 정보 조회
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public StaffMenuDTO selectStaffMenuInfo(StaffMenuDTO staffMenuDTO) throws Exception;

	/**
	 * 메뉴 수정
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public int updateStaffMenuInfo(StaffMenuDTO staffMenuDTO) throws Exception;

	/**
	 * 메뉴 등록
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public int insertStaffMenuInfo(StaffMenuDTO staffMenuDTO) throws Exception;
	
	
	/**
	 * 메뉴 접근권한 조회
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public StaffMenuDTO getMenuAuth(StaffMenuDTO staffMenuDTO) throws Exception;
	
	
	
	/**
	 * lnb를 완성하기 위한 관리자별 메뉴 가져오기
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public List<StaffMenuDTO> getTopMenu(StaffMenuDTO staffMenuDTO) throws Exception;
	
	
	
	/**
	 * lnb를 완성하기 위한 관리자별 메뉴 가져오기
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public List<StaffMenuDTO> staffMenuCheckList(StaffMenuDTO staffMenuDTO) throws Exception;
	
	/**
	 * gnb를 완성하기 위한 관리자별 메뉴 가져오기
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public List<StaffMenuDTO> staffGnbMenuMake(StaffMenuDTO staffMenuDTO) throws Exception;
	
	/**
	 * 메뉴명 가져오기
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public StaffMenuDTO selectMenuName(StaffMenuDTO staffMenuDTO) throws Exception;
	
	
}
