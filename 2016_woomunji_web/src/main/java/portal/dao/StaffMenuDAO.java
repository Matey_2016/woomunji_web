package portal.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import portal.dto.StaffMenuDTO;

@Repository("staffMenuDao")
public class StaffMenuDAO extends BaseSqlMapDAO {
	private String NAMESPACE = "portal.dao.StaffMenuDAO";	
	
	
	public int selectStaffMenuCnt() throws Exception{
		return queryForInt(NAMESPACE+".selectStaffMenuCnt");
	}
	/**
	 * 메뉴 리스트 조회
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */	
	public List<StaffMenuDTO> selectStaffMenuList(StaffMenuDTO staffMenuDTO) throws Exception{
		return queryForList(NAMESPACE+".selectStaffMenuList",staffMenuDTO);
	}
	
	/**
	 * 메뉴 정보 조회
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public StaffMenuDTO selectStaffMenuInfo(StaffMenuDTO staffMenuDTO) throws Exception{
		return (StaffMenuDTO) queryForObject(NAMESPACE + ".selectStaffMenuInfo", staffMenuDTO);
	}

	/**
	 * 메뉴 수정
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public int updateStaffMenuInfo(StaffMenuDTO staffMenuDTO) throws Exception{
		return update(NAMESPACE + ".updateStaffMenuInfo", staffMenuDTO);
	}

	/**
	 * 메뉴 등록
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public int insertStaffMenuInfo(StaffMenuDTO staffMenuDTO) throws Exception{
		return insert(NAMESPACE + ".insertStaffMenuInfo", staffMenuDTO);
	}
	
	/**
	 * lnb를 완성하기 위한 관리자별 메뉴 가져오기
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public List<StaffMenuDTO> staffMenuCheckList(StaffMenuDTO staffMenuDTO) throws Exception{
		return selectList(NAMESPACE + ".staffMenuCheckList", staffMenuDTO);
	}
	
	
	/**
	 * 메뉴 접근권한 조회
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public StaffMenuDTO getMenuAuth(StaffMenuDTO staffMenuDTO) throws Exception{
		return (StaffMenuDTO) selectOne(NAMESPACE + ".getMenuAuth", staffMenuDTO);
	}
	
	
	
	/**
	 * gnb를 완성하기 위한 관리자별 메뉴 가져오기
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public List<StaffMenuDTO> getTopMenu(StaffMenuDTO staffMenuDTO) throws Exception{
		return selectList(NAMESPACE + ".getTopMenu", staffMenuDTO);
	}
	
	
	/**
	 * gnb를 완성하기 위한 관리자별 메뉴 가져오기
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public List<StaffMenuDTO> staffGnbMenuMake(StaffMenuDTO staffMenuDTO) throws Exception{
		return selectList(NAMESPACE + ".staffGnbMenuMake", staffMenuDTO);
	}
	
	/**
	 * 메뉴명 가져오기
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public StaffMenuDTO selectMenuName(StaffMenuDTO staffMenuDTO) throws Exception{
		return (StaffMenuDTO) selectOne(NAMESPACE + ".selectMenuName", staffMenuDTO);
	}
}
