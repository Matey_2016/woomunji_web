package portal.dao;

import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import portal.dto.ListDTO;
import portal.dto.UserDTO;

@Repository("userDao")
public class UserDAO extends BaseSqlMapDAO {
	
	private String nameSpace = "portal.dao.UserDAO";
	
	/**
	 * 회원목록(관리자)
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public ListDTO userList(UserDTO userDTO) throws SQLException{
		return queryForPage(nameSpace + ".userCount", nameSpace + ".userList", userDTO);
	}
	
	/**
	 * 회원정보 조회(관리자)
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public UserDTO userView(UserDTO userDTO) throws SQLException{
		return (UserDTO)selectOne(nameSpace + ".userView", userDTO);
	}
	
	
	/**
	 * 비밀번호 초기화(관리자)
	 * @param userDTO
	 * @throws SQLException
	 */
	public int initPasswd(UserDTO userDTO) throws SQLException{
		return update(nameSpace + ".initPasswd", userDTO);
	}
	
	
	public int lastLogin(UserDTO userDTO) throws Exception{
		return update(nameSpace + ".lastLogin", userDTO);
	} 
	
	
	/**
	 * 아이디 중복체크(사용자)
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public int idCheck(UserDTO userDTO) throws SQLException{
		return queryForInt(nameSpace + ".idCheck", userDTO);
	}
	
	
	/**
	 * 아이디 중복체크(사용자)
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public int certCheck(UserDTO userDTO) throws SQLException{
		return queryForInt(nameSpace + ".certCheck", userDTO);
	}
	
	
	/**
	 * 회원정보등록(사용자)
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public int insertUser(UserDTO userDTO) throws SQLException{
		return insert(nameSpace + ".insertUser", userDTO);
	}
	
	
	/**
	 * 사용자 관심분야 삭제
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public int interestDelete(UserDTO userDTO) throws SQLException{
		return delete(nameSpace + ".interestDelete", userDTO);
	}
	
	
	/**
	 * 사용자 관심분야 저장
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public int interestInsert(UserDTO userDTO) throws SQLException{
		return insert(nameSpace + ".interestInsert", userDTO);
	}
	
	
	/**
	 * 회원정보수정(사용자)
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public int updateUser(UserDTO userDTO) throws SQLException{
		return insert(nameSpace + ".updateUser", userDTO);
	}
	
	/**
	 * 회원정보수정(사용자)
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public int updateUserCert(UserDTO userDTO) throws SQLException{
		return insert(nameSpace + ".updateUserCert", userDTO);
	}
	
	
	
	/**
	 * 사용자 로그인
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public UserDTO login(UserDTO userDTO) throws SQLException{
		return (UserDTO)selectOne(nameSpace + ".login", userDTO);
	}
	
	
	/**
	 * 아이디/비밀번호 찾기
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public UserDTO findUser(UserDTO userDTO) throws SQLException{
		return (UserDTO)selectOne(nameSpace + ".findUser", userDTO);
	}
	
	
	/**
	 * 회원탈퇴
	 * @param userDTO
	 * @return
	 * @throws SQLException
	 */
	public UserDTO leaveUser(UserDTO userDTO) throws SQLException{
		return (UserDTO)selectOne(nameSpace + ".leaveUser", userDTO);
	}
}