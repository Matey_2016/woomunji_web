package portal.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;

import portal.dto.AttachedFileDTO;
import portal.dto.ListDTO;
import portal.dto.PostDTO;

@Repository("postDao")
public class PostDAO extends BaseSqlMapDAO {
	
	private String NAMESPACE = "portal.dao.PostDAO";
	
	/**
	 * 게시물목록(관리자)
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public ListDTO postAdminList(PostDTO postDTO) throws SQLException{
		return queryForPage(NAMESPACE + ".postAdminCount", NAMESPACE + ".postAdminList", postDTO);
	}
	
	
	/**
	 * post_id 채번
	 * @return
	 * @throws SQLException
	 */
	public String selectPostId()throws SQLException{
		return (String)selectOne(NAMESPACE + ".selectPostId");
	}
	
	
	/**
	 * 이전글 다음글 목록
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public List<PostDTO> prevNextList(PostDTO postDTO) throws SQLException{
		return selectList(NAMESPACE + ".prevNextList", postDTO);
	}
	
	
	/**
	 * 게시물목록(관리자)
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public ListDTO postThumbList(PostDTO postDTO) throws SQLException{
		return queryForPage(NAMESPACE + ".postAdminCount", NAMESPACE + ".postThumbList", postDTO);
	}
	
	
	/**
	 * 추천게시물 목록
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public List<PostDTO> topList(PostDTO postDTO) throws SQLException{
		return selectList(NAMESPACE + ".topList", postDTO);
	}
	
	
	
	/**
	 * 첨부파일 조회
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public List<AttachedFileDTO> fileList(PostDTO postDTO) throws SQLException{
		return selectList(NAMESPACE + ".fileList", postDTO);
	}
	
	
	
	/**
	 * 게시물 등록
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public int insertPost(PostDTO postDTO) throws SQLException{
		return insert(NAMESPACE + ".insertPost", postDTO);
	}
	
	
	/**
	 * 게시물 등록 후 부모글 정보 수정
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public int updateParentPostId(PostDTO postDTO) throws SQLException{
		return update(NAMESPACE + ".updateParentPostId", postDTO);
	}
	
	
	/**
	 * 게시물 조회
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public PostDTO postView(PostDTO postDTO) throws SQLException{
		update(NAMESPACE + ".updateHit", postDTO);
		return (PostDTO)selectOne(NAMESPACE + ".postView", postDTO);
	}
	
	
	/**
	 * 게시물 수정
	 * @param postDTO
	 * @throws SQLException
	 */
	public int updatePost(PostDTO postDTO) throws SQLException{
		return update(NAMESPACE + ".updatePost", postDTO);
	}
	
	
	/**
	 * 첨부파일 등록
	 * @param fileDto
	 * @return
	 * @throws Exception
	 */
	public int insertAttFile(AttachedFileDTO fileDto) throws Exception{
		int result = 0;
		try {
			result = insert(NAMESPACE + ".insertAttFile", fileDto);
		}catch(Exception e) {
			System.out.println(e.toString());
			throw e;
		}finally{
		}
		return result; 
	}	
	
	
	/**
	 * 첨부파일 부분적 삭제
	 * @param fileVO
	 * @return
	 * @throws Exception
	 */
	public int deleteAddFile(AttachedFileDTO fileVO) throws Exception{
		int result = 0;
		result = delete(NAMESPACE + ".deleteAttFile", fileVO);
		return result;
		
	}
	
	
	/**
	 * 수정/삭제용 파일목록
	 * @return
	 */
	public List<AttachedFileDTO> selectedFileList(AttachedFileDTO fileVO) throws Exception{
		return selectList(NAMESPACE + ".selectedFileList", fileVO);
	}
	
	
	//게시물 삭제
	public int deletePost(PostDTO postDTO) throws SQLException{
		return update(NAMESPACE + ".postDelete", postDTO);
	}
	
	
	/**
	 * 코멘트 목록
	 * @param postDTO
	 * @return
	 * @throws Exception
	 */
	public List<PostDTO> commentList(PostDTO postDTO) throws Exception{
		return selectList(NAMESPACE + ".commentList", postDTO);
	}
	
	
	/**
	 * 코멘트 등록
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public int commentInsert(PostDTO postDTO) throws SQLException{
		return insert(NAMESPACE + ".commentInsert", postDTO);
	}
	

	/**
	 * 코멘트 삭제
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public int commentDelete(PostDTO postDTO) throws SQLException{
		return update(NAMESPACE + ".commentDelete", postDTO);
	}
	
	
	/**
	 * 게시물목록(관리자)
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public ListDTO useList(PostDTO postDTO) throws SQLException{
		return queryForPage(NAMESPACE + ".useListCount", NAMESPACE + ".useList", postDTO);
	}
	
	
	/**
	 * 게시물 답변 조회
	 * @param postDTO
	 * @return
	 * @throws SQLException
	 */
	public PostDTO postReplyView(PostDTO postDTO) throws SQLException{
		update(NAMESPACE + ".updateHit", postDTO);
		return (PostDTO)selectOne(NAMESPACE + ".postReplyView", postDTO);
	}
	
}