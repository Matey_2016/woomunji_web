package portal.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import portal.dto.AttachedFileDTO;
import portal.dto.BannerDTO;
import portal.dto.ListDTO;

@Repository("bannerDao")
public class BannerDAO extends BaseSqlMapDAO {
	
	private static final String nameSpace = "portal.dao.BannerDAO";
	
	/**
	 * 베너관리 리스트
	 * @param bannerDto
	 * @return
	 * @throws Exception
	 */
	public ListDTO selectBannerList(BannerDTO bannerDto) throws Exception{ 
		return queryForPage(nameSpace + ".selectBannerCnt", nameSpace + ".selectBannerList", bannerDto);
	}
	
	/**
	 * 베너상세 페이지
	 * @param bannerDto
	 * @return
	 * @throws Exception
	 */
	public BannerDTO selectBannerInfo(BannerDTO bannerDto) throws Exception {
		return (BannerDTO) selectOne(nameSpace + ".selectBannerInfo", bannerDto);
	}

	
	/**
	 * 베너 수정
	 * @param bannerDto
	 * @return
	 * @throws Exception
	 */
	public int updateBannerFile(BannerDTO bannerDTO) throws Exception{
		return update(nameSpace + ".updateBannerFile", bannerDTO);
	}
	
	/**
	 * 베너 수정
	 * @param bannerDto
	 * @return
	 * @throws Exception
	 */
	public int updateBannerInfo(BannerDTO bannerDto) throws Exception{
		return update(nameSpace + ".updateBannerInfo", bannerDto);
	}
	
	
	/**
	 * 베너 등록
	 * @param bannerDto
	 * @return
	 * @throws Exception
	 */
	public int insertBannerInfo(BannerDTO bannerDto) throws Exception{
		int result = 0;
		try {
			result = insert(nameSpace + ".insertBannerInfo", bannerDto);			
		}catch(Exception e) {
			throw e;
		}finally{
		}
		return result; 
	}	
	
	/**
	 * 베너 삭제
	 * @param bannerDto
	 * @return
	 * @throws Exception
	 */
	public int deleteBannerInfo(BannerDTO bannerDto) throws Exception{
		int result = 0;
		try {
			result = update(nameSpace + ".deleteBannerInfo", bannerDto);	
		}catch(Exception e) {
			System.out.println(e.toString());
			throw e;
		}finally{
		}
		return result; 
	}
	
	/**
	 * 사용할 배너 목록
	 * @param bannerDto
	 * @return
	 * @throws Exception
	 */
	public List<BannerDTO> selectUsingList(BannerDTO bannerDto) throws Exception{
		List<BannerDTO> result = selectList(nameSpace + ".selectUsingList", bannerDto);
		return result;		
	}
}
