package portal.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import portal.dto.ListDTO;
import portal.dto.StaffDTO;

@Repository("staffDAO")
public class StaffDAO extends BaseSqlMapDAO {
	private static final String NAMESPACE = "portal.dao.StaffDAO";
	
	/**
	 * 운영자 로그인
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public StaffDTO staffLoginDo(StaffDTO staffDTO) throws Exception{
		return (StaffDTO) selectOne(NAMESPACE + ".staffLoginDo", staffDTO);
	}
	
	
	/**
	 * 운영자 리스트 조회
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public ListDTO selectStaffList(StaffDTO staffDTO) throws Exception{
		return queryForPage(NAMESPACE + ".selectStaffCnt", NAMESPACE + ".selectStaffList", staffDTO);
	}
	
	/**
	 * 운영자 정보 조회
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public StaffDTO selectStaffInfo(StaffDTO staffDTO) throws Exception{
		return (StaffDTO) selectOne(NAMESPACE + ".selectStaffInfo", staffDTO);
	}

	/**
	 * 운영자 수정
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public int updateStaffInfo(StaffDTO staffDTO) throws Exception{
		return update(NAMESPACE + ".updateStaffInfo", staffDTO);
	}

	/**
	 * 운영자 등록
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public int insertStaffInfo(StaffDTO staffDTO) throws Exception{
		return insert(NAMESPACE + ".insertStaffInfo", staffDTO);
	}
	
	/**
	 * 운영자별 관리권한 정보 호출
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public List<StaffDTO> selectStaffAuthInfoList(StaffDTO staffDTO) throws Exception{
		return selectList(NAMESPACE + ".selectStaffAuthInfoList", staffDTO);
	}
	
	/**
	 * 운영자 메뉴 설정 초기화
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public int initStaffAuth(StaffDTO staffDTO) throws Exception{
		return delete(NAMESPACE + ".initStaffAuth", staffDTO);
	}
	
	/**
	 * 운영자 메뉴 설정 등록
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public int insertStaffAuth(StaffDTO staffDTO) throws Exception{
		return insert(NAMESPACE + ".insertStaffAuth", staffDTO);
	}
	
	
	/**
	 * 운영자 아이디 중복체크
	 * @param staffDto
	 * @return
	 * @throws Exception
	 */
	public int checkStaffId(StaffDTO staffDTO) throws Exception{
		return 0;//selectOne(NAMESPACE + ".checkStaffId", staffDTO);
	}
	
}