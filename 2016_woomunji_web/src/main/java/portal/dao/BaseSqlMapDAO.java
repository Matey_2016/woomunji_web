package portal.dao;

import java.util.List;

import portal.dto.BaseDTO;
import portal.dto.ListDTO;
import egovframework.rte.psl.dataaccess.EgovAbstractMapper;


public class BaseSqlMapDAO extends EgovAbstractMapper {

	public int queryForInt(String id) {
		return ((Integer) selectOne(id, null)).intValue();
	}

	public int queryForInt(String id, Object parameterObject) {
		return ((Integer) selectOne(id,parameterObject)).intValue();
	}

	public int insert(String id, Object parameterObject) {
		return super.insert(id, parameterObject);
	}
	
	public int update(String id, Object parameterObject) {
		return super.update(id, parameterObject);
	}

	public int delete(String id, Object parameterObject) {
		return super.delete(id, parameterObject);
	}

	public Object queryForObject(String id) {
		return selectByPk(id , null);
	}
	public Object queryForObject(String id, Object parameterObject) {
		return selectByPk(id, parameterObject);
	}

	public <E> List<E> queryForList(String selectSqlId, Object parameterBean) {
		return selectList(selectSqlId, parameterBean);
	}

	
	public <E> List<E> queryForList(String selectSqlId) {
		return selectList(selectSqlId);
	}


	public Object queryForMap(String id, Object parameterBean, String keyObject, String valueObject) {
		return  selectMap(id, keyObject, valueObject);
	}
	
	public ListDTO queryForPage(String countSqlId, String selectSqlId, BaseDTO parameterBean) {
		ListDTO listDTO = new ListDTO();
		listDTO.setPageSize( parameterBean.getPageSize() );
		
		
		if (parameterBean.getCpage() == 1) {
			listDTO.setPageNo(parameterBean.getStart() / parameterBean.getPageGrpSize() + 1);
		} else {
			listDTO.setPageNo(parameterBean.getCpage());
		}
		
		if (countSqlId != null) {
			int recordCount = queryForInt(countSqlId, parameterBean);
			listDTO.setRecordCount(recordCount);
		} else {
			listDTO.setRecordCount(2147483647);
		}
		
		listDTO.setPageList(selectList(selectSqlId, parameterBean));
		return listDTO;
	}
}