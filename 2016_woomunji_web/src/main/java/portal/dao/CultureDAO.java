package portal.dao;

import java.sql.SQLException;
import java.util.List;

import org.springframework.stereotype.Repository;

import portal.dto.CultureDTO;
import portal.dto.ListDTO;

@Repository("cultureDao")
public class CultureDAO extends BaseSqlMapDAO {
	
	private String NAMESPACE = "portal.dao.CultureDAO";
	
	/**
	 * 문화재 목록
	 * @param cultureDTO
	 * @return
	 * @throws SQLException
	 */
	public ListDTO cultureList(CultureDTO cultureDTO) throws SQLException{
		return queryForPage(NAMESPACE + ".cultureCount", NAMESPACE + ".cultureList", cultureDTO);
	}
	
	
	/**
	 * 문화재 상세조회
	 * @param cultureDTO
	 * @return
	 * @throws SQLException
	 */
	public CultureDTO cultureView(CultureDTO cultureDTO) throws SQLException{
		return (CultureDTO)queryForObject(NAMESPACE + ".cultureView", cultureDTO);
	}
	
	
	/**
	 * 문화재 이미지 목록
	 * @param cultureDTO
	 * @return
	 * @throws SQLException
	 */
	public List<CultureDTO> cultureImageList(CultureDTO cultureDTO) throws SQLException{
		return queryForList(NAMESPACE + ".cultureImageList", cultureDTO);
	}
	
	
	/**
	 * 문화재 게시여부 변경
	 * @param cultureDTO
	 * @return
	 * @throws SQLException
	 */
	public int postUpdate(CultureDTO cultureDTO) throws SQLException{
		return update(NAMESPACE + ".postUpdate", cultureDTO);
	}
	
}