package portal.dao;

import java.sql.SQLException;

import org.springframework.stereotype.Repository;

import portal.dto.ConcertDTO;
import portal.dto.ListDTO;

@Repository("concertDao")
public class ConcertDAO extends BaseSqlMapDAO {
	
	private String NAMESPACE = "portal.dao.ConcertDAO";
	
	/**
	 * 공연전시 목록
	 * @param concertDTO
	 * @return
	 * @throws SQLException
	 */
	public ListDTO concertList(ConcertDTO concertDTO) throws SQLException{
		return queryForPage(NAMESPACE + ".concertCount", NAMESPACE + ".concertList", concertDTO);
	}
	
	
	/**
	 * 공연전시 상세조회
	 * @param concertDTO
	 * @return
	 * @throws SQLException
	 */
	public ConcertDTO concertView(ConcertDTO concertDTO) throws SQLException{
		return (ConcertDTO)queryForObject(NAMESPACE + ".concertView", concertDTO);
	}
	
	
	/**
	 * 공연전시 게시여부 변경
	 * @param concertDTO
	 * @return
	 * @throws SQLException
	 */
	public int postUpdate(ConcertDTO concertDTO) throws SQLException{
		return update(NAMESPACE + ".postUpdate", concertDTO);
	}
	
}