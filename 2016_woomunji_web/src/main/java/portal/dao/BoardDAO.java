package portal.dao;

import java.sql.SQLException;
import org.springframework.stereotype.Repository;

import portal.dto.BoardDTO;
import portal.dto.ListDTO;

@Repository("boardDao")
public class BoardDAO extends BaseSqlMapDAO {
	
	private String NAMESPACE = "portal.dao.BoardDAO";
	
	/**
	 * 게시판목록(관리자)
	 * @param boardDTO
	 * @return
	 * @throws SQLException
	 */
	public ListDTO boardList(BoardDTO boardDTO) throws SQLException{
		return queryForPage(NAMESPACE + ".boardCount", NAMESPACE + ".boardList", boardDTO);
	}
	
	
	/**
	 * 게시판 등록
	 * @param boardDTO
	 * @return
	 * @throws SQLException
	 */
	public int insertBoard(BoardDTO boardDTO) throws SQLException{
		return insert(NAMESPACE + ".insertBoard", boardDTO);
	}
	
	
	/**
	 * 게시판정보 조회
	 * @param boardDTO
	 * @return
	 * @throws SQLException
	 */
	public BoardDTO boardView(BoardDTO boardDTO) throws SQLException{
		return (BoardDTO)selectOne(NAMESPACE + ".boardView", boardDTO);
	}
	
	
	/**
	 * 게시판 수정
	 * @param boardDTO
	 * @throws SQLException
	 */
	public int updateBoard(BoardDTO boardDTO) throws SQLException{
		return update(NAMESPACE + ".updateBoard", boardDTO);
	}
	
}