package portal.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import portal.dto.CodeDTO;

@Repository("codeDAO")
public class CodeDAO  extends  BaseSqlMapDAO{
	
	private String nameSpace="portal.dao.CodeDAO" ;
	
	
	/**
	 * 메인코드 리스트 조회
	 * @param comcodeVO
	 * @return
	 * @throws Exception
	 */
	public List<CodeDTO> mainCodeList()throws Exception {
		List<CodeDTO> result = selectList(nameSpace + ".mainCodeList");
		return result;
	}
	
    
	/**
	 * 상위코드로 하위 코드 검색
	 * @param highCd
	 * @return
	 * @throws Exception
	 */
	public List<CodeDTO> subCodeList(CodeDTO codeDTO) {
		List<CodeDTO> result = selectList(nameSpace + ".subCodeList", codeDTO);
		return result;		
	}
	
	/**
	 * 공통코드 상세 조회
	 * @param comcodeVO
	 * @return
	 * @throws Exception
	 */
	public CodeDTO codeView(CodeDTO CodeDTO) {
		return (CodeDTO) queryForObject(nameSpace + ".codeView", CodeDTO);
	}
	
	/**
	 * 공통코드 수정
	 * @param comcodeVO
	 * @return
	 * @throws Exception
	 */
	public int codeUpdate(CodeDTO codeDTO) {
		return update(nameSpace + ".codeUpdate", codeDTO);
	}
	
	
	/**
	 * 공통코드 등록
	 * @param comcodeVO
	 * @return
	 * @throws Exception
	 */
	public int codeInsert(CodeDTO codeDTO) {
	  return insert(nameSpace + ".codeInsert", codeDTO);
	}
    
	
	/**
	 * 특정 HighCode에 대한 Code 리스트 반환
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public List<CodeDTO> jsonCodeList(CodeDTO codeDTO) {
		return selectList(nameSpace + ".jsonCodeList", codeDTO);
	}
	
	/**
	 * 상위 코드 조회
	 * @param highCd
	 * @return
	 * @throws Exception
	 */
	public String selectSuperCd(String highCd) {
		return (String) selectByPk(nameSpace + ".selectSuperCd", highCd);
	}
    
	/**
	 * 특정 HighCode에 대한 Code 리스트 반환
	 * @param staffMenuDto
	 * @return
	 * @throws Exception
	 */
	public List<CodeDTO> getCodeList(CodeDTO codeDTO) {
		return selectList(nameSpace + ".getCodeList", codeDTO);
	}
    
	/**
	 * 특정 HighCode에 대한 Code 리스트 중 일부 데이터 제외한 값만 반환 
	 * @param comcodeVO
	 * @return
	 * @throws Exception
	 */
	public List<CodeDTO> getCodeListExcept(CodeDTO codeVO) {
		return selectList(nameSpace + ".getCodeListExcept", codeVO);
	}
    
	/**
	 * 특정 HighCode에 대한 Code 리스트 중 선택된 데이터 값만 반환 
	 * @param comcodeVO
	 * @return
	 * @throws Exception
	 */
	public List<CodeDTO> getCodeListInclude(CodeDTO codeVO) {
		return selectList(nameSpace + ".getCodeListInclude", codeVO);
	}

	/**
	 * 코드목록중  CODE_ETC값에 따른 목록 호출
	 * @param codeDto
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<CodeDTO> getCodeListSpecial(CodeDTO codeVO) {
		return selectList(nameSpace + ".getCodeListSpecial", codeVO);
	}

	/**
	 * 공통코드 중복체크
	 * @param comcodeVO
	 * @return
	 * @throws Exception
	 */
	public CodeDTO selectDuplCodeInfo(CodeDTO codeVO) throws Exception{
		return (CodeDTO) queryForObject(nameSpace + ".selectDuplCodeInfo", codeVO);
	}
	
	/**
	 * 공통코드에 정의된 사용자 메뉴 정보
	 * @return
	 * @throws Exception
	 */
	public List<CodeDTO> getUserMenu() throws Exception{
		return selectList(nameSpace + ".selectUserMenu");
	}

}
