package portal.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import portal.dto.CodeDTO;
import portal.dto.StaffDTO;
import portal.dto.StaffMenuDTO;
import portal.service.StaffMenuService;
import portal.util.BaseUtil;
import portal.util.session.SessionUtil;

@Controller
@SessionAttributes("SESSIONDOMAIN")
public class StaffMenuController {
	
	protected final Log log = LogFactory.getLog(getClass());
	@Resource(name="staffMenuService")
	private StaffMenuService staffMenuService;
	
	@Autowired
	private CodeController codeController;
	
	/**
	 * 메뉴관리 리스트 호출
	 * @param request
	 * @param response
	 * @param staffMenuDTO
	 * @param model
	 * @param sql 
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/staff/staffMenuList.do",method={RequestMethod.GET,RequestMethod.POST})
	public String staffMenuList(HttpServletRequest request
						  	  , HttpServletResponse response
						  	  , StaffMenuDTO staffMenuDTO
						  	  , ModelMap model) throws Exception{
				
	   	model.addAttribute("listVO", staffMenuService.selectStaffMenuList(staffMenuDTO));
		model.addAttribute("codeList", codeController.getCodeList("MENU_FLAG"));
		return BaseUtil.getBaseURI(request);
	}
	
	
	/**
	 * 메뉴 정보 등록 화면
	 * @param request
	 * @param staffMenuVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/staff/staffMenuWrite.do",method=RequestMethod.GET)
	public String staffMenuWrite(HttpServletRequest request
								,StaffMenuDTO staffMenuDTO
								,ModelMap model) throws Exception{ 
		String searchParam = BaseUtil.getParams(request, "&","menu_id","mode");
		
		if(staffMenuDTO.getMode().equals("M")){
			StaffMenuDTO resultVO = staffMenuService.selectStaffMenuInfo(staffMenuDTO);
			model.addAttribute("staffMenuDTO", resultVO);
			resultVO = null;
		}
		
		List<CodeDTO> codeList = codeController.getCodeList("MENU_FLAG");
		model.addAttribute("codeList", codeList);
		model.addAttribute("searchParam", searchParam);
		codeList = null;
		return BaseUtil.getBaseURI(request);
	}
	
	
	
	/**
	 * 메뉴정보 저장
	 * @param request
	 * @param staffMenuVO
	 * @param model
	 * @param bindingResult
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/staff/staffMenuWriteDo.do",method=RequestMethod.POST)
	public String staffMenuWriteDo(HttpServletRequest request
								  ,StaffMenuDTO staffMenuDTO
								  ,ModelMap model) throws Exception{
		String searchParam = BaseUtil.getParams(request, "&","menu_id","mode");
		
		if(staffMenuDTO.getMode().equals("M")){
			staffMenuService.updateStaffMenuInfo(staffMenuDTO);
		} else {
			staffMenuDTO.setMenu_id(java.util.UUID.randomUUID().toString());
			staffMenuService.insertStaffMenuInfo(staffMenuDTO);
		}
		
		return "redirect:/operation/staff/staffMenuList.do?"+searchParam;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value="/operation/staff/staffMenuCheckList.do",method=RequestMethod.POST)
	public void staffMenuCheckList( HttpServletRequest request
									, HttpServletResponse response
									, StaffMenuDTO staffMenuDTO
									, ModelMap model) throws Exception{
		
		JSONArray jsonArr = new JSONArray();
		JSONObject result = new JSONObject();
		
		String parent_cd = request.getParameter("gnb_cd") == null ? "MENU_FLAG" : request.getParameter("gnb_cd");
		String menu_id = request.getParameter("lnb_id") == null ? "" : request.getParameter("lnb_id");
		String staff_id = ((StaffDTO)SessionUtil.getSession(request, "avo", new StaffDTO())).getStaff_id();
		
		
		staffMenuDTO.setParent_cd(parent_cd);
		staffMenuDTO.setMenu_id(menu_id);
		staffMenuDTO.setStaff_id(staff_id);
		List<StaffMenuDTO> list = staffMenuService.staffMenuCheckList(staffMenuDTO);
		staffMenuDTO = staffMenuService.getMenuAuth(staffMenuDTO);
		
		
		for (int i =0; i<list.size(); i++){
		     JSONObject json = new JSONObject();
		     json.put("menu_id" , ((StaffMenuDTO)list.get(i)).getMenu_id());
		     json.put("menu_nm" , ((StaffMenuDTO)list.get(i)).getMenu_nm());
		     json.put("menu_url" , ((StaffMenuDTO)list.get(i)).getMenu_url());
		     if (staffMenuDTO != null){
			     json.put("cChk" , staffMenuDTO.getcChk());
			     json.put("uChk" , staffMenuDTO.getuChk());
			     json.put("rChk" , staffMenuDTO.getrChk());
			     json.put("dChk" , staffMenuDTO.getdChk());
		     }
		     json.put("parent_cd_nm" , ((StaffMenuDTO)list.get(i)).getParent_cd_nm());
		     jsonArr.add(json );
		     
		}
		
		result.put("jList" , jsonArr );
		response.setContentType("application/json; charset=utf-8" );
		response.getWriter().write(result .toString());

	}
	

}


