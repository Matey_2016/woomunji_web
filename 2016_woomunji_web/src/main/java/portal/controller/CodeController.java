package portal.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import portal.dto.CodeDTO;
import portal.service.CodeService;
import portal.util.BaseUtil;


@Controller
@SessionAttributes("SESSIONDOMAIN")
public class CodeController {
	protected final Log log=(Log) LogFactory.getLog(getClass());
	
	@Resource(name ="codeService")
	private CodeService codeService;
	
	
	@Autowired
	private StaffMenuController staffMenuController;

	/**
	 * 공통코드 관리 리스트 호출
	 * @param sessionDomain
	 * @param request
	 * @param response
	 * @param comcodeVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/**/**/codeList.do",method=RequestMethod.GET)
	public String codeList(	HttpServletRequest request,
							HttpServletResponse response,
							CodeDTO codeDTO,
							ModelMap model) throws Exception{
		
		String baseURI = BaseUtil.getBaseURI(request);
		
		//관리자인 경우는 대분류 코드까지 호출함.
		if (baseURI.startsWith("/operation/code")){
			codeDTO.setOperation_yn("Y");
			List<CodeDTO> mainList = codeService.mainCodeList();
			model.addAttribute("mainList", mainList);
			mainList = null;
		}
		
		List<CodeDTO> subList  = codeService.subCodeList(codeDTO);
		model.addAttribute("subList", subList);
		
		subList = null;
		return baseURI;
	}
	

	/**
	 * 공통코드 정보 상세
	 * @param sessionDomain
	 * @param request
	 * @param comcodeVO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/code/codeWrite.do",method=RequestMethod.GET)
	public String codeWrite( HttpServletRequest request
			               ,CodeDTO codeDTO
			               ,ModelMap model) throws Exception{		
		
		//트리용 대분류 코드
		codeDTO.setOperation_yn("Y");
		List<CodeDTO> mainList = codeService.mainCodeList();
		model.addAttribute("mainList", mainList);
		mainList = null;
		
		if (codeDTO.getMode().equals("M")){
			//코드 상세정보
			codeDTO = codeService.codeView(codeDTO);
		    model.addAttribute("codeDTO", codeDTO);
		}
	    codeDTO = null;
		return BaseUtil.getBaseURI(request);
	}
	
	
	
	/**
	 * 공통코드 등록 화면
	 * @param sessionDomain
	 * @param request
	 * @param codeDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/operation/code/codeWriteDo.do",method=RequestMethod.POST)
	public void codeWriteDo( HttpServletRequest request
						    ,CodeDTO codeDTO
						    ,HttpServletResponse response) throws Exception{
		
		JSONObject 	json 	= new JSONObject();
		int 		result 	= 0;
		
		try{
			
			if (codeDTO.getMode().equals("C")){
				result = codeService.codeInsert(codeDTO);
			} else if (codeDTO.getMode().equals("M")){
				result = codeService.codeUpdate(codeDTO);
			} else {
				codeDTO.setUse_yn("N");
				result = codeService.codeUpdate(codeDTO);
			}
			
			if (result > 0) {
				json.put("result", "T");
			} else {
				json.put("result", "F");
			}
		}catch (Exception e){
			json.put("result", "F");
			log.error(e.toString());
		}finally{
			response.setContentType("application/json; charset=utf-8");
			response.getWriter().write(json.toString());
		}
	}

	
	/**
	 * 특정 HighCode에 대한 Code 리스트 반환
	 * @param sessionDomain
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public List<CodeDTO> getCodeList(String MajorCd) throws Exception {
		CodeDTO codeDTO = new CodeDTO();
		codeDTO.setHigh_id( MajorCd );
		List<CodeDTO> codeListVO = codeService.getCodeList(codeDTO);
		codeDTO = null;	
		return codeListVO;
	}
	
	/**
	 * 특정 HighCode에 대한 Code 리스트 반환
	 * @param sessionDomain
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	public List<CodeDTO> getCodeList(String MajorCd, String order) throws Exception {
		CodeDTO codeDTO = new CodeDTO();
		codeDTO.setHigh_id( MajorCd );
		codeDTO.setOrderby(order);
		List<CodeDTO> codeListVO = codeService.getCodeList(codeDTO);
		codeDTO = null;	
		return codeListVO;
	}
	
	
	/**
	 * 특정 HighCode에 대한 Code 리스트 반환
	 * @param sessionDomain
	 * @param request
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/**/**/subCodeList.do",method=RequestMethod.GET)
	public void subCodeList(CodeDTO	 codeDTO, 
							HttpServletResponse response) throws Exception {
		
		JSONArray 	jsonArr = new JSONArray();
		JSONObject 	result 	= new JSONObject();
		
		List<CodeDTO> list = codeService.getCodeList(codeDTO);

		for (int i =0; i<list.size(); i++){
		     JSONObject json = new JSONObject();
		     json.put("code_id" , ((CodeDTO)list.get(i)).getCode_id());
		     json.put("code_nm" , ((CodeDTO)list.get(i)).getCode_nm());
		     json.put("code_etc" , ((CodeDTO)list.get(i)).getCode_etc());
		     jsonArr.add(json );
		}

		result.put("jList" , jsonArr );
		response.setContentType("application/json; charset=utf-8" );
		response.getWriter().write(result .toString());
		
	}

	
	/**
	 * 직무 목록
	 * 
	 * @param sessionDomain
	 * @param request
	 *            , response
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/jsonCodeList.do", method=RequestMethod.GET)
	public void jsonCodeList(HttpServletResponse response,
							 HttpServletRequest request, 
							 CodeDTO codeDTO) throws Exception {

		String jsonCodeList = "";
		List<CodeDTO> codeList = null;
		codeList = codeService.jsonCodeList(codeDTO);

		for (int i = 0; i < codeList.size(); i++) {
			CodeDTO cd = (CodeDTO) codeList.get(i);
			jsonCodeList += cd.getCode_id() + "," + cd.getCode_nm();

			if (i < codeList.size() - 1) {
				jsonCodeList += "^";
			}
		}

		JSONObject json = new JSONObject();
		json.put("jsonCodeList", jsonCodeList);
		response.setContentType("application/json; charset=utf-8");
		response.getWriter().write(json.toString());

	}
	
	
}	