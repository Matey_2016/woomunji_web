package portal.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import portal.dto.ConcertDTO;
import portal.dto.ListDTO;
import portal.service.ConcertService;
import portal.util.BaseUtil;

@Controller
@SessionAttributes("SESSIONDOMAIN")
public class ConcertController {
	protected final Log log = LogFactory.getLog(getClass());
	
	@Resource(name="concertService")
	private ConcertService concertService;
	
	@Autowired
	private CodeController codeController;
	
	
	/**
	 * 공연전시목록
	 * @param request
	 * @param concertDTO
	 * @return
	 * @throws Exception
	 */ 
	@RequestMapping(value="/operation/concert/concertList.do",method={RequestMethod.GET,RequestMethod.POST})
	public String concertList(HttpServletRequest request,
							  ConcertDTO concertDTO,
							  ModelMap model)  throws Exception {
		
		
		ListDTO listVO = concertService.concertList(concertDTO);
		
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&amp;", "seq"));
		model.addAttribute("listVO", listVO);

		listVO = null;
		return "/operation/concert/concertList";   
	}
	
	
	/**
	 * 공연전시상세조회
	 * @param request
	 * @param concertDTO
	 * @return
	 * @throws Exception
	 */ 
	@RequestMapping(value="/operation/concert/concertView.do",method=RequestMethod.GET)
	public String concertView(HttpServletRequest request,
							  ConcertDTO concertDTO,
							  ModelMap model)  throws Exception {
		
		
		concertDTO = concertService.concertView(concertDTO);
		concertDTO.setContents1(concertDTO.getContents1().replaceAll("src=\"/upload", "class=\"remove_imgTag\" src=\"http://www.culture.go.kr/upload"));
		
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&amp;", "seq"));
		model.addAttribute("resultVO", concertDTO);
		
		concertDTO 	= null;
		return "/operation/concert/concertView";   
	}
	
	/**
	 * 게시여부 변경
	 * @param request
	 * @param response
	 * @param concertDTO
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/operation/concert/postUpdate.do",method=RequestMethod.GET)
	public void postUpdate (HttpServletRequest request,
							HttpServletResponse response,
							ConcertDTO concertDTO ) throws Exception {
	
		JSONObject 	json = new JSONObject();
		int result = concertService.postUpdate(concertDTO);
		
		if (result > 0){
			json.put("result", "T");
		} else {
			json.put("result", "F");
		}
		
		response.setContentType("application/json; charset=utf-8");
		response.getWriter().write(json.toString());
	}
}
