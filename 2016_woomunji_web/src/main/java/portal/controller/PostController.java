package portal.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import portal.dto.AttachedFileDTO;
import portal.dto.BoardDTO;
import portal.dto.ListDTO;
import portal.dto.PostDTO;
import portal.dto.StaffDTO;
import portal.service.BoardService;
import portal.service.PostService;
import portal.util.BaseUtil;
import portal.util.SecurityUtil;
import portal.util.file.FileUploadResults;
import portal.util.file.FileUploadUtil;
import portal.util.session.SessionDomain;
import portal.util.session.SessionUtil;

@Controller
@SessionAttributes("SESSIONDOMAIN")
public class PostController {
	protected final Log log = LogFactory.getLog(getClass());
	
	@Resource(name="postService")
	private PostService postService;
	
	@Resource(name="boardService")
	private BoardService boardService;
	
	@Autowired
	private CodeController codeController;
	
	@Autowired
	private StaffMenuController staffMenuController;
	
	@Resource(name = "messageSource")
	private MessageSource messageSource;
	
	
	/**
	 * 게시판 공통페이지 
	 * @param sessionDomain
	 * @param request
	 * @param postDTO
	 * @return
	 * @throws Exception
	 */ 
	@RequestMapping(value="/operation/**/post.do")
	public String qnaPost(	HttpServletRequest request,
							PostDTO postDTO,
							ModelMap model)  throws Exception {
		
		if (request.getParameter("gubun") == null){
			model.addAttribute("gubun", "List");
		} else {
			model.addAttribute("gubun", request.getParameter("gubun"));
		}
		
		
		BoardDTO board = new BoardDTO();
		board.setBoard_id(postDTO.getBoard_id());
		board = boardService.boardView(board);
		
		if (board != null) {
			board.setPageId(postDTO.getPageId());
		} else {
			board = new BoardDTO();
			board.setPageId(postDTO.getPageId());
		}
		model.addAttribute("board", board);
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&amp;"));
		return BaseUtil.getBaseURI(request);   
	}
	
	
	
	/**
	 * 게시물목록 
	 * @param sessionDomain
	 * @param request
	 * @param postDTO
	 * @return
	 * @throws Exception
	 */ 
	@RequestMapping(value="/**/postList.do", method=RequestMethod.GET)
	public String list( HttpServletRequest request,
						PostDTO postDTO,
						ModelMap model)  throws Exception {
		
		String baseURI	= BaseUtil.getBaseURI(request);
		
		BoardDTO board = new BoardDTO();
		board.setBoard_id(postDTO.getBoard_id());
		board = boardService.boardView(board);
		model.addAttribute("board", board);
		
		postDTO.setPageSize(board.getPage_size());
		ListDTO listVO = postService.postAdminList(postDTO); 
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&amp;","gubun","post_id","mode","num"));
		model.addAttribute("listVO", listVO);
		
		//이전/다음글 사용여부
		if(board.getPreNext_yn().equals("Y")){
			List<PostDTO> prevNextList = postService.prevNextList(postDTO);
			model.addAttribute("prevNext", prevNextList);
		}
		
		
		if (baseURI.startsWith("/operation")) {
			baseURI = "/operation/board/postList";
			
		} else {
				
		}
		
		
		listVO 	= null;
		postDTO = null;
		return baseURI;
	}
	
	
	/**
	 * 이미지형 게시물목록 
	 * @param sessionDomain
	 * @param request
	 * @param postDTO
	 * @return
	 * @throws Exception
	 */ 
	@RequestMapping(value="/**/postThumb.do", method=RequestMethod.GET)
	public String thumbList(	HttpServletRequest request,
								PostDTO postDTO,
								ModelMap model)  throws Exception {
		
		String baseURI	= request.getRequestURI().replaceAll(".do", "");
		postDTO.setPageSize(9);
		ListDTO listVO = postService.postThumbList(postDTO); 
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&amp;","gubun","post_id"));
		model.addAttribute("listVO", listVO);
		model.addAttribute("UPLOAD", BaseUtil.getProp(request, "UPLOAD_PATH")+"board");
		
		if (baseURI.indexOf("/operation/") > -1) {
			baseURI = "/operation/board/postThumb";
		} else {
			baseURI = "/comBoard/postThumb";
		}
		
		listVO 	= null;
		postDTO = null;
		return baseURI;   
	}
	
	
	/**
	 * 게시물 등록 및 수정, 답변화면
	 * @param sessionDomain
	 * @param request
	 * @param postDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/**/postWrite.do", method=RequestMethod.GET)
	public String postWrite(	@ModelAttribute("SESSIONDOMAIN")SessionDomain sessionDomain, 
								HttpServletRequest request,
								PostDTO postDTO,
								ModelMap model)  throws Exception {
		
		StaffDTO staffDTO 	= new StaffDTO();
		staffDTO 			= (StaffDTO)SessionUtil.getSession(request, "avo", staffDTO);
		String baseURI		= BaseUtil.getBaseURI(request);
		
		
		//게시판 정보
		BoardDTO board = new BoardDTO();
		board.setBoard_id(postDTO.getBoard_id());
		board = boardService.boardView(board);
		model.addAttribute("board", board);
		
		
		//URL 임의 조작 후 접근한 경우
		if (BaseUtil.isReferer(request) && board.getUrlDeny_yn().equals("Y")){
			return new StringBuffer().append("redirect:/operation/message.do?msgCode=incorrect_path")
									 .append("&forwardUrl=/operation/board/postList")
									 .append("&board_id="+postDTO.getBoard_id()).toString();  
		}
		
		
		//공통코드 사용정보
		if (board.getUse_code1() != ""){
			model.addAttribute("code1", codeController.getCodeList(board.getUse_code1()));
		}
		
		
		//공통코드 사용정보
		if (board.getUse_code2() != "" && board.getCode_relation() != "Y"){
			model.addAttribute("code2", codeController.getCodeList(board.getUse_code2()));
		}
		
		
		
		//수정화면인 경우
		if (postDTO.getMode().equals("M")){
			List<AttachedFileDTO> fileList = new ArrayList<AttachedFileDTO>();
			fileList = postService.fileList(postDTO);
			postDTO = postService.postView(postDTO);
			postDTO.setMode("M");
			model.addAttribute("fileList", fileList);
			fileList 	= null;
		
		//답변화면인 경우
		} else if (postDTO.getMode().equals("R")){
			postDTO = postService.postView(postDTO);
			postDTO.setUser_id(staffDTO.getStaff_id());
			postDTO.setUser_nm(staffDTO.getStaff_nm());
			postDTO.setMode("R");
		
		//등록화면인 경우
		} else {
			
			//관리자인 경우
			if (baseURI.indexOf("/operation/") > -1) {
				postDTO.setUser_id(staffDTO.getStaff_id());
				postDTO.setUser_nm(staffDTO.getStaff_nm());
				
			//사용자인 경우
			} else {
				//HttpSession session = request.getSession();
//				UserDTO userDTO = (UserDTO)session.getAttribute("uvo");
//				if (userDTO != null){
//					postDTO.setUser_id(userDTO.getUser_id());
//					postDTO.setUser_nm(userDTO.getUser_nm());
//					postDTO.setEmail(userDTO.getEmail());
//				}
			}
			postDTO.setMode("C");
		}
		
		if (baseURI.startsWith("/operation")) {
			baseURI = "/operation/board/postWrite";
		} else {
			baseURI = "/comBoard/postWrite";
		}
		
		
		
		model.addAttribute("baseURI", baseURI);
		model.addAttribute("postDTO", postDTO);
		model.addAttribute("gubun", "Write");
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&amp;","gubun"));
		
		postDTO		= null;
		staffDTO 	= null;
		board 		= null;
		return baseURI;   
	}
	
	
	
	/**
	 * 게시물조회
	 * @param sessionDomain
	 * @param request
	 * @param postDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/**/postView.do", method=RequestMethod.GET)
	public String postView(	@ModelAttribute("SESSIONDOMAIN")SessionDomain sessionDomain, 
							HttpServletRequest request,
							PostDTO postDTO,
							ModelMap model)  throws Exception {
		
		//게시판 정보
		BoardDTO board = new BoardDTO();
		board.setBoard_id(postDTO.getBoard_id());
		board = boardService.boardView(board);
		model.addAttribute("board", board);
		
		
		String baseURI    				= BaseUtil.getBaseURI(request);
		List<AttachedFileDTO> fileVO 	= new ArrayList<AttachedFileDTO>();	//DB검색용 리시트
		List<AttachedFileDTO> fileList	= new ArrayList<AttachedFileDTO>();	//return용 리스트
		
		//게시물 정보
		postDTO = postService.postView(postDTO);
		
		
		//게시판이 파일을 사용하는 경우 파일정보
		if (board.getFile_yn().equals("Y") || board.getThumb_yn().equals("Y") || board.getMovie_yn().equals("Y")){
			fileVO = postService.fileList(postDTO);
			for(int i=0;i<fileVO.size();i++){
				AttachedFileDTO fVO = new AttachedFileDTO();
				fVO.setFilePath(SecurityUtil.encode(BaseUtil.getProp(request, "UPLOAD_PATH")+((AttachedFileDTO)fileVO.get(i)).getFilePath()+"\\"+((AttachedFileDTO)fileVO.get(i)).getSysFileName()));
				fVO.setSysFileName(SecurityUtil.encode(((AttachedFileDTO)fileVO.get(i)).getOrgFileName()));
				fVO.setOrgFileName(((AttachedFileDTO)fileVO.get(i)).getOrgFileName());
				fVO.setFileSize(((AttachedFileDTO)fileVO.get(i)).getFileSize());
				fVO.setFileType(((AttachedFileDTO)fileVO.get(i)).getFileType());
				if ("THUMB".equals(((AttachedFileDTO)fileVO.get(i)).getFileType())){
					fVO.setThumbImg(((AttachedFileDTO)fileVO.get(i)).getSysFileName());
				}
				fileList.add(fVO);
			}
			model.addAttribute("fileList", fileList);
			fileList = null;
		}
		
		
		if (baseURI.indexOf("/operation/") > -1) {
			baseURI = "/operation/board/postView";
		} else {
				
		}
		
		
		//이전,다음글을 사용하는 경우
		if(board.getPreNext_yn().equals("Y")){
			List<PostDTO> prevNextList = postService.prevNextList(postDTO);
			model.addAttribute("preNextList", prevNextList);
			prevNextList = null;
		}
		
		//댓글목록
		if (board.getComment_yn().equals("Y")){
			List<PostDTO> commentList = postService.commentList(postDTO);
			model.addAttribute("commentList", commentList);
			commentList = null;
		}
		
		model.addAttribute("resultVO", postDTO);
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&amp;","gubun","post_id","mode"));
		
		fileVO 		= null;
		postDTO 	= null;
		
		return baseURI;   
	}
	
	
	
	/**
	 * 게시물 등록/수정
	 * @param sessionDomain
	 * @param request
	 * @param postDTO
	 * @param model
	 * @param bindingResult
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/**/postWriteDo.do", method=RequestMethod.POST)
	public String postRegist(	HttpServletRequest request,
								PostDTO postDTO,
								ModelMap model) throws Exception{
		
		StaffDTO staffDTO 	= (StaffDTO)SessionUtil.getSession(request, "avo", new StaffDTO());
		String 	baseURI		= BaseUtil.getBaseURI(request,"WriteDo","List");
		String  msgCode		= "";
		int	   	result	  	= 0;	
		
		postDTO.setRemote_ip(BaseUtil.getIp(request));
		postDTO.setPasswd(SecurityUtil.getMD5String(postDTO.getPasswd()));
		
		if (postDTO.getMode().equals("C") || postDTO.getMode().equals("R")) {
			msgCode = "insert";
			postDTO.setReg_id(postDTO.getUser_id());
			result = postService.insertPost(postDTO);
			
			
			if (result > 0 && "".equals(postDTO.getParent_post_id())){
				postDTO.setParent_post_id(postDTO.getPost_id());
				postService.updateParentPostId(postDTO);
			} else {
				postDTO.setParent_post_id(postDTO.getParent_post_id());
				postService.updateParentPostId(postDTO);
			}
		} else {
			msgCode = "modify";
			//수정주체에 따른 구분
			if (baseURI.startsWith("/operation/")){
				postDTO.setMod_id(staffDTO.getStaff_id());
				postDTO.setMod_flag("A");
			} else {
				postDTO.setMod_id(postDTO.getUser_id());
				postDTO.setMod_flag("U");
			}
			
			result = postService.updatePost(postDTO);
		}
		
		if (result > 0){
			//첨부파일 처리
			List<AttachedFileDTO> fileList = new ArrayList<AttachedFileDTO>();
			FileUploadResults fileResult = FileUploadUtil.uploadForDirectory(request, BaseUtil.getProp(request, "UPLOAD_PATH"), "board");
			
			
			for(int i = 0; i<fileResult.getUploadFiles().size(); i++){
				AttachedFileDTO fileVO = new AttachedFileDTO();
				
				
				fileVO.setOrgFileName( fileResult.getOriginalFilenames().get(i));
				fileVO.setSysFileName( fileResult.getNewFilenames().get(i));
				fileVO.setFilePath(fileResult.getUploadRelPath());
				fileVO.setParentId( postDTO.getPost_id() ); // update는 기존정보에서, insert는 insert결과물로 postId 가져온 값을 대입. 
				fileVO.setFileSize(Integer.parseInt(fileResult.getFileSize().get(i)));
				
				if ("thumb".equals(fileResult.getRequestName().get(i))){
					fileVO.setFileType("THUMB");
				} else {
					fileVO.setFileType("FILE");
				}
				
				
				fileList.add(fileVO);
				
			}
			
			if (postDTO.getMode().equals("C")) {
				postService.insertAttFile(fileList);
			} else {
				//파일정보 등록
				postService.insertAttFile(fileList);	
				
				String[] delfileId = request.getParameterValues("delfileId");
				postService.deleteAttFile(postDTO, delfileId);
			}
			msgCode = "success."+msgCode;
		} else {
			msgCode = "fail."+msgCode;
		}
		
		if (baseURI.startsWith("/operation/")){
			return new StringBuffer().append("redirect:/operation/message.do?msgCode="+msgCode)
					  .append("&")
					  .append("forwardUrl=").append(baseURI)
					  .append("&board_id="+postDTO.getBoard_id())
					  .toString();
		} else{
			return new StringBuffer().append("redirect:/message.do?msgCode="+msgCode)
					  .append("&")
					  .append("forwardUrl=").append("/academy/idea")
					  .append("&gubun=List&board_id="+postDTO.getBoard_id())
					  .toString();
		}
	}
	
	
	/**
	 * 게시물,파일, 댓글 일괄삭제
	 * @param sessionDomain
	 * @param request
	 * @param postDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/**/postDelete.do", method=RequestMethod.GET)
	public String postDelete(	HttpServletRequest request,
								PostDTO postDTO,
								ModelMap model) throws Exception{
		
		String baseURI	= request.getRequestURI().replaceAll("postDelete.do", "");
		String msgCode	= "";
		int	   result	= 0;	

		
		//게시판 정보
		BoardDTO board = new BoardDTO();
		board.setBoard_id(postDTO.getBoard_id());
		board = boardService.boardView(board);
		
		//URL 임의 조작 후 접근한 경우
		if (BaseUtil.isReferer(request)){
			return new StringBuffer().append("redirect:/operation/message.do?msgCode=incorrect_path")
									 .append("&forwardUrl=/operation/board/postList")
									 .append("&board_id="+postDTO.getBoard_id()).toString();  
		}
		
		result = postService.deletePost(postDTO);
		
		if (result > 0) {
			msgCode = "success.delete";
		} else {	
			msgCode = "fail.delete";
		}
		
		
		if (baseURI.startsWith("/operation/")){
			return new StringBuffer().append("redirect:/operation/message.do?msgCode=").append(msgCode)
					  .append("&")
					  .append("forwardUrl=").append("/operation/board/postList")
					  .append("&board_id="+postDTO.getBoard_id())
					  .toString();
		} else {
			return new StringBuffer().append("redirect:/message.do?msgCode=").append(msgCode)
					  .append("&")
					  .append("forwardUrl=").append("/board/postList")
					  .append("&board_id="+postDTO.getBoard_id())
					  .toString();
		}
	}
	
	
	
	/**
	 *댓글등록
	 */
	@RequestMapping(value="/**/commentWriteDo.do", method=RequestMethod.POST)
	public String commentWriteDo(HttpServletRequest request,
								 PostDTO postDTO,
								 ModelMap model) throws Exception{
		
		String msgCode = "";
		
		//URL 임의 조작 후 접근한 경우
		if (BaseUtil.isReferer(request)){
			return new StringBuffer().append("redirect:/operation/message.do?msgCode=incorrect_path")
									 .append("&forwardUrl=/operation/board/postView")
									 .append("&board_id="+postDTO.getBoard_id())
									 .append("&post_id="+postDTO.getPost_id()).toString();  
		}
		
		StaffDTO staffDTO = new StaffDTO();
		staffDTO = (StaffDTO)SessionUtil.getSession(request, "avo", staffDTO);
		
		postDTO.setUser_id(staffDTO.getStaff_id());
		postDTO.setUser_nm(staffDTO.getStaff_nm());
		int result = postService.commentInsert(postDTO);
		
		if (result > 0){
			msgCode = "success.insert";
		} else {
			msgCode = "fail.insert";
		}
		
		staffDTO = null;
		return new StringBuffer().append("redirect:/operation/message.do?msgCode="+msgCode)
								 .append("&forwardUrl=/operation/board/postView")
								 .append("&board_id="+postDTO.getBoard_id())
								 .append("&post_id="+postDTO.getPost_id()).toString(); 
	}
	
	
	/**
	 * 
	 * @param request
	 * @param postDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/**/commentDelete.do", method=RequestMethod.GET)
	public String commentDelete(HttpServletRequest request,
								 PostDTO postDTO,
								 ModelMap model) throws Exception{
		
		String msgCode = "";
		
		//URL 임의 조작 후 접근한 경우
		if (BaseUtil.isReferer(request)){
			return new StringBuffer().append("redirect:/operation/message.do?msgCode=incorrect_path")
									 .append("&forwardUrl=/operation/board/postView")
									 .append("&board_id="+postDTO.getBoard_id())
									 .append("&post_id="+postDTO.getPost_id()).toString();  
		}
		
		int result = postService.commentDelete(postDTO);
		
		if (result > 0){
			msgCode = "success.delete";
		} else {
			msgCode = "fail.delete";
		}
		
		return new StringBuffer().append("redirect:/operation/message.do?msgCode="+msgCode)
								 .append("&forwardUrl=/operation/board/postView")
								 .append("&board_id="+postDTO.getBoard_id())
								 .append("&post_id="+postDTO.getPost_id()).toString(); 
	}
}
