package portal.controller;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import portal.dto.StaffDTO;
import portal.dto.StaffMenuDTO;
import portal.service.StaffMenuService;
import portal.util.BaseUtil;
import portal.util.SecurityUtil;
import portal.util.session.SessionUtil;

@Controller
@SessionAttributes("SESSIONDOMAIN")
public class CommonController {
	
	protected final Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private StaffMenuController staffMenuController;
	
	@Autowired
	private CodeController codeController;
	
	
	@Resource(name="staffMenuService")
	private StaffMenuService staffMenuService;
	
	/** 컨텐츠 페이지 이동
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/**/*Page.do", method=RequestMethod.GET)
    public String goContentsPage(	HttpServletRequest request) {
		String requestURI = request.getRequestURI();
        String page = requestURI.replaceAll("(.*)\\.*Page.do","$1");
        return page;
    }
	
	
	/** 관리자 메인 메세지 페이지 이동
	 * @return
	 */
	@RequestMapping(value="/operation/message.do", method=RequestMethod.GET)
	public String forwardAdminMainMessagePage(HttpServletRequest request
			  								, ModelMap model) throws Exception{
		return "/operation/include/message";
	}
	
	
	/** 관리자단  top 이동
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/operation/top.do", method=RequestMethod.GET)
    public String header( HttpServletRequest request
						, ModelMap model) throws Exception{
		
		
		StaffMenuDTO staffMenuDTO = new StaffMenuDTO();
		staffMenuDTO.setStaff_id(((StaffDTO)SessionUtil.getSession(request, "avo", new StaffDTO())).getStaff_id());
		
		List<StaffMenuDTO> menuList = staffMenuService.getTopMenu(staffMenuDTO);
		model.addAttribute("menuList", menuList); 
		menuList = null;
		return "/operation/include/top";
    }
	
	/** 관리자단  left 이동
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/operation/left.do", method=RequestMethod.GET)
    public String left(  HttpServletRequest request
						, HttpServletResponse response
						, StaffMenuDTO staffMenuDTO
						, ModelMap model) throws Exception{
		
		
		HttpSession session = request.getSession();		
		session = request.getSession(true);
		String session_lnb_id	= session.getAttribute("lnb_id") == null ? "" : (String)session.getAttribute("lnb_id");
		
		String parent_cd = request.getParameter("gnb_cd") == null ? "MENU_FLAG" : request.getParameter("gnb_cd");
		String menu_id = request.getParameter("lnb_id") == null ? session_lnb_id : request.getParameter("lnb_id");
		String staff_id = ((StaffDTO)SessionUtil.getSession(request, "avo", new StaffDTO())).getStaff_id();

		staffMenuDTO.setParent_cd(parent_cd);
		
		staffMenuDTO.setMenu_id(menu_id);
		staffMenuDTO.setStaff_id(staff_id);
		List<StaffMenuDTO> list = staffMenuService.staffMenuCheckList(staffMenuDTO);
		staffMenuDTO = staffMenuService.getMenuAuth(staffMenuDTO);
		
		model.addAttribute("menuList", list);
		model.addAttribute("pageAuth", staffMenuDTO);

		session_lnb_id 	= null;
		parent_cd 		= null;
		menu_id 		= null;
		staff_id 		= null;
		list 			= null;
		staffMenuDTO 	= null;
		
		return "/operation/include/left";
		
    }
	
	
	/** 관리자단  foot 이동
	 * @param request
	 * @return
	 */
	@RequestMapping(value="/operation/foot.do", method=RequestMethod.GET)
    public String foot(	HttpServletRequest request) {
		return "/operation/include/foot";
    }
	
	
	@RequestMapping(value="/**/download.do", method=RequestMethod.GET)
	public void download(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// 암호화 풀기
		String filePath = request.getParameter("filePath") == null ? "" : SecurityUtil.decode(request.getParameter("filePath"));
		String fileName = request.getParameter("fileName") == null ? "" : SecurityUtil.decode(request.getParameter("fileName"));
		
		File file = new File(filePath);
		  
		  response.setContentType("application/octet-stream");
		  response.setHeader("Content-Disposition","attachment;filename=\"" +URLEncoder.encode(fileName,"utf-8")+"\";");
		  
		  FileInputStream fis=new FileInputStream(file);
		  BufferedInputStream bis=new BufferedInputStream(fis);
		  ServletOutputStream so=response.getOutputStream();
		  BufferedOutputStream bos=new BufferedOutputStream(so);
		  
		  byte[] data=new byte[2048];
		  int input=0;
		  while((input=bis.read(data))!=-1){
		   bos.write(data,0,input);
		   bos.flush();
		  }
		  
		  if(bos!=null) bos.close();
		  if(bis!=null) bis.close();
		  if(so!=null) so.close();
		  if(fis!=null) fis.close();
	}
	
	
	/** 에디터 이미지 업로드 요청 처리
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@RequestMapping(value="/ckeditor/uploadfile.do", method=RequestMethod.POST)
	public String uploadCKEditorsFile(MultipartHttpServletRequest request, HttpServletResponse response, ModelMap modelMap) throws IOException {
		
		
		String webRootPath = BaseUtil.getProp(request, "EDITOR_UPLOAD_PATH");		//각자 경로에 맞게 설정할것. 단, WEB-APP디렉토리 안에 존재해야함.
		String absUplaodPath = webRootPath;
		String funcNum = "";
		String fileUrl = "";
		Iterator<String> iter = request.getFileNames();
		if (iter.hasNext()) {  
			
			MultipartFile multiFile = request.getFile(iter.next());
			String fileName = multiFile.getOriginalFilename();
			fileName = createNewFileName(fileName);

			File uploadDir = new File(absUplaodPath);
			if (!uploadDir.exists() || !uploadDir.isDirectory()) {
				uploadDir.mkdirs();
			}
			
			File newFile = new File(absUplaodPath, fileName);
			// 중복파일 체크
			while (newFile.exists()) {
				newFile = new File(absUplaodPath, createNewFileName(fileName));
			}
			
			multiFile.transferTo(newFile);
			funcNum = request.getParameter("CKEditorFuncNum");
			fileUrl = BaseUtil.getProp(request, "EDITOR_IMAGE_PATH") + fileName;
			
		}
		modelMap.addAttribute("fileUrl", fileUrl);
		modelMap.addAttribute("funcNum", funcNum);
		
		return "/common/uploadfile";
	}
	
	/** TODO 위치이동 - currentTimeMillis 기반 파일명 반환
	 * @param orgFileName
	 * @return String
	 */
	private String createNewFileName(String orgFileName) {
		int idx = 0;
		if ((idx = orgFileName.indexOf(".")) == -1) {
			return orgFileName;
		}
		String extension = orgFileName.substring(idx, orgFileName.length());
		return System.currentTimeMillis() + extension;
	}
}

