package portal.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import portal.dto.CodeDTO;
import portal.dto.CultureDTO;
import portal.dto.ListDTO;
import portal.dto.StaffDTO;
import portal.service.CultureService;
import portal.util.BaseUtil;
import portal.util.SecurityUtil;
import portal.util.session.SessionUtil;

@Controller
@SessionAttributes("SESSIONDOMAIN")
public class CultureController {
	protected final Log log = LogFactory.getLog(getClass());
	
	@Resource(name="cultureService")
	private CultureService cultureService;
	
	@Autowired
	private CodeController codeController;
	
	
	/**
	 * 문화재목록
	 * @param request
	 * @param cultureDTO
	 * @return
	 * @throws Exception
	 */ 
	@RequestMapping(value="/operation/culture/cultureList.do",method={RequestMethod.GET,RequestMethod.POST})
	public String cultureList(HttpServletRequest request,
							  CultureDTO cultureDTO,
							  ModelMap model)  throws Exception {
		
		
		String[] arrAge_cd  =  request.getParameterValues("age_cds") == null ? request.getParameter("age_cds")  == null ? null: request.getParameter("age_cds").split(",")  : request.getParameterValues("age_cds");
		String[] arrCtrd_cd = request.getParameterValues("ctrd_cds") == null ? request.getParameter("ctrd_cds") == null ? null: request.getParameter("ctrd_cds").split(",") : request.getParameterValues("ctrd_cds");
		String[] arrItem_cd = request.getParameterValues("item_cds") == null ? request.getParameter("item_cds") == null ? null: request.getParameter("item_cds").split(",") : request.getParameterValues("item_cds");
		
		String age_cds  = "";
		String ctrd_cds = "";
		String item_cds = "";
		
		if (arrAge_cd != null){
			for (int i=0;i<arrAge_cd.length;i++){
				age_cds += arrAge_cd[i]+",";
			}
		}
		
		
		if (arrCtrd_cd != null){
			for (int i=0;i<arrCtrd_cd.length;i++){
				ctrd_cds += arrCtrd_cd[i]+",";
			}
		}
		
		
		if (arrItem_cd != null){
			for (int i=0;i<arrItem_cd.length;i++){
				item_cds += arrItem_cd[i]+",";
			}
		}
		
		
		cultureDTO.setArrAge_cd(arrAge_cd);
		cultureDTO.setArrCtrd_cd(arrCtrd_cd);
		cultureDTO.setArrItem_cd(arrItem_cd);
		
		ListDTO listVO = cultureService.cultureList(cultureDTO);
		
		List<CodeDTO> ageList = codeController.getCodeList("AGE","code_id");
		List<CodeDTO> cultureList = codeController.getCodeList("CULTURAL","code_id");
		List<CodeDTO> areaList = codeController.getCodeList("AREA","code_id");
		
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&amp;", "culture_id","item_cd", "age_cd", "ctrd_cd"));
		model.addAttribute("listVO", listVO);
		model.addAttribute("ageList", ageList);
		model.addAttribute("cultureList", cultureList);
		model.addAttribute("areaList", areaList);
		model.addAttribute("age_cd", age_cds);
		model.addAttribute("ctrd_cd", ctrd_cds);
		model.addAttribute("item_cd", item_cds);
		
		listVO = null;
		return "/operation/culture/cultureList";   
	}
	
	
	/**
	 * 문화재상세조회
	 * @param request
	 * @param cultureDTO
	 * @return
	 * @throws Exception
	 */ 
	@RequestMapping(value="/operation/culture/cultureView.do",method=RequestMethod.GET)
	public String cultureView(HttpServletRequest request,
							  CultureDTO cultureDTO,
							  ModelMap model)  throws Exception {
		
		
		String[] arrAge_cd = request.getParameterValues("age_cds");
		String[] arrCtrd_cd = request.getParameterValues("ctrd_cds");
		String[] arrItem_cd = request.getParameterValues("item_cds");
		
		String age_cds  = "";
		String ctrd_cds = "";
		String item_cds = "";
		
		if (arrAge_cd != null){
			for (int i=0;i<arrAge_cd.length;i++){
				age_cds += arrAge_cd[i]+",";
			}
		}
		
		
		if (arrCtrd_cd != null){
			for (int i=0;i<arrCtrd_cd.length;i++){
				System.out.println(arrCtrd_cd[i]);
				ctrd_cds += arrCtrd_cd[i]+",";
			}
		}
		
		
		if (arrItem_cd != null){
			for (int i=0;i<arrItem_cd.length;i++){
				item_cds += arrItem_cd[i]+",";
			}
		}
		
		
		
		cultureDTO = cultureService.cultureView(cultureDTO);
		List<CultureDTO> imageList = cultureService.cultureImageList(cultureDTO);
		
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&amp;", "ctrd_cd", "item_cd", "crlts_no","ctrd_cds", "item_cds", "age_cds"));
		model.addAttribute("resultVO", cultureDTO);
		model.addAttribute("imageList", imageList);
		model.addAttribute("age_cd", age_cds);
		model.addAttribute("ctrd_cd", ctrd_cds);
		model.addAttribute("item_cd", item_cds);
		
		age_cds  	= null;
		ctrd_cds 	= null;
		item_cds 	= null;
		cultureDTO 	= null;
		imageList  	= null;
		return "/operation/culture/cultureView";   
	}
	
	/**
	 * 게시여부 변경
	 * @param request
	 * @param response
	 * @param cultureDTO
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/operation/culture/postUpdate.do",method=RequestMethod.GET)
	public void postUpdate (HttpServletRequest request,
							HttpServletResponse response,
							CultureDTO cultureDTO ) throws Exception {
	
		JSONObject 	json = new JSONObject();
		int result = cultureService.postUpdate(cultureDTO);
		
		if (result > 0){
			json.put("result", "T");
		} else {
			json.put("result", "F");
		}
		
		response.setContentType("application/json; charset=utf-8");
		response.getWriter().write(json.toString());
	}
}
