package portal.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import portal.dto.CodeDTO;
import portal.dto.ListDTO;
import portal.dto.UserDTO;
import portal.service.UserService;
import portal.util.BaseUtil;
import portal.util.ExcelUtil;
import portal.util.SecurityUtil;
import portal.util.session.SessionDomain;
import portal.util.session.SessionUtil;

@Controller
@SessionAttributes("SESSIONDOMAIN")
@RequestMapping(value={"/m", "/"})
public class UserController {
	protected final Log log = LogFactory.getLog(getClass());
	
	@Resource(name="userService")
	private UserService userService;
	
	@Autowired
	private StaffMenuController staffMenuController;
	
	@Autowired
	private CodeController codeController;
	
	
	/**
	 * 회원목록(관리자) 
	 * @param sessionDomain
	 * @param request
	 * @param userDTO
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/user/userList.do", method=RequestMethod.GET)
	public String userList( HttpServletRequest request,
							UserDTO userDTO,
							ModelMap model)  throws Exception {
		
		ListDTO listVO = userService.userList(userDTO);
		
		model.addAttribute("listVO", listVO);
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&amp;", "user_id"));
		
		listVO = null;
		return BaseUtil.getBaseURI(request);  
	}
	
	
	/**
	 * 회원정보 조회(관리자) 
	 * @param request
	 * @param userDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/user/userView.do", method=RequestMethod.GET)
	public String userView( HttpServletRequest request,
							UserDTO userDTO,
							ModelMap model)  throws Exception {
		userDTO = userService.userView(userDTO);		
		
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&amp;", "user_id"));
		model.addAttribute("userDTO", userDTO);
		userDTO = null;
		return "/operation/user/userView";  
	}
	
	
	/**
	 * 사용자 로그인 
	 * @param sessionDomain
	 * @param request
	 * @param userDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/mypage/login/login_proc.do", method=RequestMethod.POST)
	public String login_proc(HttpServletRequest request,
							 UserDTO userDTO,
							 ModelMap model)  throws Exception {
		
		String rtn_msg 		= "";
		String baseURI    	= request.getRequestURI().replaceAll("_proc.do", "");
		String mobiURI		= "";
		if ( baseURI.indexOf( "/m/" ) >= 0 ) {
			mobiURI    = "/m";
		}
		
		userDTO.setPasswd(SecurityUtil.getMD5String(userDTO.getPasswd()));
		userDTO = userService.login(userDTO);
		
		if (userDTO != null){
			if (userDTO.getLeave_dt() != null){
				rtn_msg = "탈퇴한 회원정보입니다.";
			
			} else {
				
				HttpSession session = request.getSession();	
				session = request.getSession(true);
		        session.setAttribute("uvo", userDTO); 
		        userService.lastLogin(userDTO);
		        
		        return "redirect:"+mobiURI+"/main.do";
				        	
			}
			
		} else {
			rtn_msg = "잘못된 아이디 또는 비밀번호입니다.";
		}
		
		model.addAttribute("rtn_msg", rtn_msg);
		return baseURI;
	}
	
	
	/**
	 * 사용자 로그인 화면
	 * @param sessionDomain
	 * @param request
	 * @param userDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/mypage/login/login.do", method=RequestMethod.GET)
	public String login(HttpServletRequest request,
						UserDTO userDTO,
						ModelMap model)  throws Exception {
		
		String baseURI = request.getRequestURI().replaceAll(".do", "");
		return baseURI;
	}
	
		
	/**
	 * 사용자 로그아웃
	 * @param sessionDomain
	 * @param request
	 * @param userDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value={"/user/logout.do","/m/user/logout.do"}, method=RequestMethod.GET)
	public String logout(HttpServletRequest request,
						 UserDTO userDTO,
						 ModelMap model)  throws Exception {
		
		HttpSession session = request.getSession();		
		session = request.getSession(true);
        session.setAttribute("uvo", null); 
        
        return "redirect:/main.do";
	}
	
	
	/**
	 * 사용자 비밀번호 초기화(관리자) 
	 * @param sessionDomain
	 * @param request
	 * @param userDTO
	 * @param model
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/operation/user/initPasswd.do")
	public void initPasswd( HttpServletResponse response,
							UserDTO userDTO,
							Model model)  throws Exception {
		
		JSONObject json	= new JSONObject(); 
		userDTO.setPasswd(SecurityUtil.getMD5String(userDTO.getUser_id()));
		json.put("result", userService.initPasswd(userDTO));
		
		response.setContentType("application/json; charset=utf-8");
		response.getWriter().write(json.toString());
	}
	
	
	/**
	 * 회원가입페이지
	 * @param request
	 * @param response
	 * @param userDTO
	 * @throws Exception
	 */
	@RequestMapping(value="/mypage/join/join03.do")
	public String join03Page(	HttpServletRequest request
						  		, HttpServletResponse response
						  		, UserDTO userDTO
						  		, ModelMap model) throws Exception{
		
		String baseURI    = request.getRequestURI().replaceAll(".do", "");
		
		List<CodeDTO> phoneCode 	= codeController.getCodeList("uPhone");
		List<CodeDTO> mobileCode 	= codeController.getCodeList("uMobile");
		List<CodeDTO> emailCode 	= codeController.getCodeList("uEmail");
		
		model.addAttribute("phoneCode"	, phoneCode);
		model.addAttribute("mobileCode"	, mobileCode);
		model.addAttribute("emailCode"	, emailCode);
		model.addAttribute("userDTO"	, userDTO);
		
		return baseURI;
	}
	
	
	/**
	 * 회원가입 처리
	 * @param request
	 * @param response
	 * @param userDTO
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/mypage/join/join03Rgst.do")
	public void join03Rgst( HttpServletRequest request
						  	, HttpServletResponse response
						  	, UserDTO userDTO ) throws Exception{
		
		JSONObject 	json		= new JSONObject();
		
		userDTO.setEmail(userDTO.getEmail_id()+"@"+userDTO.getEmail_domain());
		userDTO.setPhone(userDTO.getPhone1()+"-"+userDTO.getPhone2()+"-"+userDTO.getPhone3());
		userDTO.setMobile(userDTO.getMobile1()+"-"+userDTO.getMobile2()+"-"+userDTO.getMobile3());
		userDTO.setPasswd(SecurityUtil.getMD5String(userDTO.getPasswd()));
		userDTO.setRemote_ip(BaseUtil.getIp(request));

		int result = userService.insertUser(userDTO);
        
        if (result > 0){
        	json.put("result", "1");
        } else {
        	json.put("result", "0");
        }
        
		response.setContentType("application/json; charset=utf-8");
		response.getWriter().write(json.toString());
	}
	
	/**
	 * 아이디 중복검사
	 * @param request
	 * @param response
	 * @param userDTO
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/mypage/join/idCheck.do")
	public void idCheck(	HttpServletRequest request
						  , HttpServletResponse response
						  , UserDTO userDTO ) throws Exception{
		
		JSONObject 	json		= new JSONObject();
		int result = userService.idCheck(userDTO);
        
        if (result > 0){
        	json.put("result", "0");
        } else {
        	json.put("result", "1");
        }
        
		response.setContentType("application/json; charset=utf-8");
		response.getWriter().write(json.toString());
	}
	
	
	/**
	 * 휴면해제
	 * @param request
	 * @param response
	 * @param userDTO
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/mypage/login/dormant.do")
	public void changeDormant(	HttpServletRequest request
						  			, HttpServletResponse response
						  			, UserDTO userDTO ) throws Exception{
		
		JSONObject 	json = new JSONObject();
		int result = userService.lastLogin(userDTO);
		
		
		if (result > 0){
        	json.put("result", "0");
        } else {
        	json.put("result", "1");
        }
		response.setContentType("application/json; charset=utf-8");
		response.getWriter().write(json.toString());
 	}
	
	
	/**
	 * 마이페이지 > 회원정보
	 * @param request
	 * @param response
	 * @param userDTO
	 * @throws Exception
	 */
	@RequestMapping(value="/mypage/member/memberChk.do")
	public String memberChk(	HttpServletRequest request
						  		, HttpServletResponse response
						  		, UserDTO userDTO
						  		, ModelMap model) throws Exception{
		
		String baseURI    = request.getRequestURI().replaceAll(".do", "");
		
		HttpSession session = request.getSession();		
		session = request.getSession(true);
		userDTO = (UserDTO)session.getAttribute("uvo");
		userDTO = userService.userView(userDTO);	
		
		List<CodeDTO> interestMainCode	= codeController.getCodeList("N1000");
		List<CodeDTO> interestSubCode 		= codeController.getCodeList("N2000");
		
		model.addAttribute("interestMainCode"	, interestMainCode);
		model.addAttribute("interestSubCode"	, interestSubCode);
		model.addAttribute("userDTO"			, userDTO);
		
		return baseURI;
	}
	
	
	/**
	 * 마이페이지 > 비밀번호 변경화면
	 * @param request
	 * @param response
	 * @param userDTO
	 * @throws Exception
	 */
	@RequestMapping(value="/mypage/member/pwModify.do")
	public String pwModify(	HttpServletRequest request
						  		, HttpServletResponse response
						  		, UserDTO userDTO
						  		, ModelMap model) throws Exception{
		
		String baseURI    = request.getRequestURI().replaceAll(".do", "");
		return baseURI;
	}
	
	
	/**
	 * 마이페이지 > 비밀번호 변경
	 * @param request
	 * @param response
	 * @param userDTO
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/mypage/member/pwUpdate.do")
	public void pwUpdate(	HttpServletRequest request
						  		, HttpServletResponse response
						  		, UserDTO userDTO
						  		, ModelMap model) throws Exception{
		
		int result 			= 0;
		JSONObject 	json		= new JSONObject();
		HttpSession session 	= request.getSession();		
		session 				= request.getSession(true);
		UserDTO user 			= (UserDTO)session.getAttribute("uvo");
		user					= userService.userView(user);
		//현재 사용중인 비밀번호와 일치하지 않는 경우
		
		if (!user.getPasswd().equals(SecurityUtil.getMD5String(userDTO.getCurrent_passwd()))){
			json.put("result", "0");
			json.put("msg", "현재 사용중인 비밀번호와 일치하지 않습니다.");
			
		} else {
			userDTO.setUser_id(user.getUser_id());
			userDTO.setPasswd(SecurityUtil.getMD5String(userDTO.getPasswd()));
			result = userService.initPasswd(userDTO);
	        
	        json.put("result", result);
			
		}
		response.setContentType("application/json; charset=utf-8");
		response.getWriter().write(json.toString());
	}
	
	
	/**
	 * 마이페이지 > 회원정보 변경
	 * @param request
	 * @param response
	 * @param userDTO
	 * @throws Exception
	 */
	@RequestMapping(value={"/mypage/member/memberModify.do", "/operation/user/userModify.do"})
	public String memberModify(@ModelAttribute(value="SESSIONDOMAIN") SessionDomain sessionDomain
								, HttpServletRequest request
						  		, HttpServletResponse response
						  		, UserDTO userDTO
						  		, ModelMap model) throws Exception{
		
		String baseURI = BaseUtil.getBaseURI(request);
		
		if ( baseURI.indexOf( "/operation/" ) < 0 ) {
			userDTO = (UserDTO)SessionUtil.getSession(request, "uvo", userDTO);
		}
		userDTO = userService.userView(userDTO);	
		
		List<CodeDTO> phoneCode 	= codeController.getCodeList("uPhone");
		List<CodeDTO> mobileCode 	= codeController.getCodeList("uMobile");
		List<CodeDTO> emailCode 	= codeController.getCodeList("uEmail");
		
		model.addAttribute("phoneCode"	, phoneCode);
		model.addAttribute("mobileCode"	, mobileCode);
		model.addAttribute("emailCode"	, emailCode);
		model.addAttribute("userDTO"	, userDTO);
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&amp;", "user_id"));
		
		phoneCode	 = null;
		mobileCode   = null;
		emailCode    = null;
		userDTO      = null;
		return baseURI;
	}
	
	
	/**
	 * 마이페이지 > 회원정보 변경처리
	 * @param request
	 * @param response
	 * @param userDTO
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/mypage/member/memberUpdate.do")
	public void memberUpdate(	HttpServletRequest request
						  		, HttpServletResponse response
						  		, UserDTO userDTO
						  		, ModelMap model) throws Exception{
		
		HttpSession session = request.getSession();		
		session = request.getSession(true);
        //UserDTO userDTO = (UserDTO)session.getAttribute("uvo"); 
		
		JSONObject 	json		= new JSONObject();
		
		userDTO.setRemote_ip(BaseUtil.getIp(request));
		userDTO.setMod_flag("U");
		userDTO.setMod_id(userDTO.getUser_id());
		userDTO.setEmail(userDTO.getEmail_id()+"@"+userDTO.getEmail_domain());
		userDTO.setPhone(userDTO.getPhone1()+"-"+userDTO.getPhone2()+"-"+userDTO.getPhone3());
		userDTO.setMobile(userDTO.getMobile1()+"-"+userDTO.getMobile2()+"-"+userDTO.getMobile3());
		
		int result  = userService.updateUser(userDTO);
		
		if (result > 0){
			
			session.setAttribute("uvo", userDTO);
		}
		json.put("result", result);
		
		response.setContentType("application/json; charset=utf-8");
		response.getWriter().write(json.toString());
	}
	
	/**
	 * 회원정보 수정
	 * @param sessionDomain
	 * @param request
	 * @param userDTO
	 * @param model
	 * @param bindingResult
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/user/userRegist.do")
	public String userRegist(@ModelAttribute(value="SESSIONDOMAIN") SessionDomain sessionDomain
						    ,HttpServletRequest request
						    ,UserDTO userDTO
						    ,ModelMap model
						    ,BindingResult bindingResult) throws Exception{
		String requestURI = request.getRequestURI();
		String baseURI    = requestURI.replaceAll("Regist.do", "");
		
		userDTO.setRemote_ip(BaseUtil.getIp(request));
		userDTO.setMod_flag("A");
		userDTO.setMod_id(userDTO.getUser_id());
		userDTO.setEmail(userDTO.getEmail_id()+"@"+userDTO.getEmail_domain());
		userDTO.setPhone(userDTO.getPhone1()+"-"+userDTO.getPhone2()+"-"+userDTO.getPhone3());
		userDTO.setMobile(userDTO.getMobile1()+"-"+userDTO.getMobile2()+"-"+userDTO.getMobile3());
	
		int result  = userService.updateUser(userDTO);
		String msgCode		= userDTO.getMode().equals("C") ? "insert" : "modify";
		
		if(result > 0){
			msgCode = "success."+msgCode;
		}else{
			msgCode = "fail."+msgCode;
		}
		
		return new StringBuffer().append("redirect:/operation/message.do?msgCode=").append(msgCode)
				  .append("&")
				  .append("forwardUrl=").append(baseURI+"List")
				  .append("&")
				  .toString();
		
	}
	
	/**
	 * 마이페이지 > 관심분야 변경
	 * @param request
	 * @param response
	 * @param userDTO
	 * @throws Exception
	 */
	@RequestMapping(value="/mypage/member/fieldModify.do")
	public String fieldModify(	HttpServletRequest request
						  		, HttpServletResponse response
						  		, UserDTO userDTO
						  		, ModelMap model) throws Exception{
		
		String baseURI    = request.getRequestURI().replaceAll(".do", "");
		
		HttpSession session = request.getSession();		
		session = request.getSession(true);
		userDTO = (UserDTO)session.getAttribute("uvo");
		userDTO = userService.userView(userDTO);	
		
		List<CodeDTO> interestMainCode	= codeController.getCodeList("N1000");
		List<CodeDTO> interestSubCode 		= codeController.getCodeList("N2000");
		
		model.addAttribute("interestMainCode"	, interestMainCode);
		model.addAttribute("interestSubCode"	, interestSubCode);
		model.addAttribute("userDTO"			, userDTO);
		
		return baseURI;
	}
	
	
	/**
	 * 관심분야 저장
	 * @param request
	 * @param response
	 * @param userDTO
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/mypage/member/fieldUpdate.do")
	public void fieldUpdate( HttpServletRequest request
						  		, HttpServletResponse response
						  		, UserDTO userDTO ) throws Exception{
		
		JSONObject 	json	= new JSONObject();
        int 		result	=0;
		userService.interestDelete(userDTO);
		
		for(int i=0;i<userDTO.getInterest_cd().split(",").length;i++){
			UserDTO u = new UserDTO();
			
			u.setUser_id(userDTO.getUser_id());
			u.setInterest_cd(userDTO.getInterest_cd().split(",")[i]);
			result = userService.interestInsert(u);
		}
        
		json.put("result", result);
		response.setContentType("application/json; charset=utf-8");
		response.getWriter().write(json.toString());
	}
	
	
	/**
	 * 마이페이지 > 회원탈퇴
	 * @param request
	 * @param response
	 * @param userDTO
	 * @throws Exception
	 */
	@RequestMapping(value="/mypage/member/memberDrop.do")
	public String memberDrop(	HttpServletRequest request
						  		, HttpServletResponse response
						  		, UserDTO userDTO
						  		, ModelMap model) throws Exception{
		
		String baseURI    = request.getRequestURI().replaceAll(".do", "");
		
		HttpSession session = request.getSession();		
		session = request.getSession(true);
		userDTO = (UserDTO)session.getAttribute("uvo");
		userDTO = userService.userView(userDTO);	
		
		model.addAttribute("userDTO", userDTO);
		
		return baseURI;
	}
	
	
	/**
	 * 마이페이지 > 회원탈퇴처리
	 * @param request
	 * @param response
	 * @param userDTO
	 * @throws Exception
	 */
	@RequestMapping(value="/mypage/member/memberDropProc.do")
	public String memberDropProc(	HttpServletRequest request
						  		, HttpServletResponse response
						  		, UserDTO userDTO
						  		, ModelMap model) throws Exception{
		
		String baseURI    = request.getRequestURI().replaceAll("Proc.do", "");
		
		HttpSession session = request.getSession();		
		session = request.getSession(true);
		userDTO = (UserDTO)session.getAttribute("uvo");
		//회원탈퇴 메일 발송
		model.addAttribute("userDTO", userDTO);
		userDTO = userService.leaveUser(userDTO);	
		session.setAttribute("uvo", null);
		return baseURI+"Complete";
	}
	
	
	/**
	 * 회원목록 엑셀 다운로드
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/operation/user/userExcelDown.do")
	public void userListExcelDown(	HttpServletRequest request, 
									UserDTO userDTO, 
									HttpServletResponse response) throws Exception {
		
		userDTO.setPageSize(999999);
		List<UserDTO> list = userService.userList(userDTO).getPageList();
		List<Object> excelList = new ArrayList<Object>();
		
		for(UserDTO uDTO : list){
			
			UserDTO u = new UserDTO();
			u.setUser_id	(uDTO.getUser_id());
			u.setUser_id	(uDTO.getUser_id());		
			u.setUser_nm	(uDTO.getUser_nm());         
			u.setEmail		(uDTO.getEmail());           
			u.setPhone		(uDTO.getPhone());           
			u.setMobile		(uDTO.getMobile());          
			u.setEmail_yn	(uDTO.getEmail_yn());        
			u.setSms_yn		(uDTO.getSms_yn());          
			u.setLogin_dt	(uDTO.getLogin_dt());        
			u.setLeave_dt	(uDTO.getLeave_dt());        
			u.setDel_yn		(uDTO.getDel_yn());          
			u.setReg_dt		(uDTO.getReg_dt());          
			u.setBirth		(uDTO.getBirth());    
			excelList.add(u);
			
		}
		
		ExcelUtil.makeExcel(excelList, "user_list", request, response);
		list 		= null;
		excelList 	= null;
	}
}
