package portal.controller;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import portal.dto.BannerDTO;
import portal.dto.ListDTO;
import portal.dto.StaffDTO;
import portal.service.BannerService;
import portal.util.BaseUtil;
import portal.util.SecurityUtil;
import portal.util.file.FileUploadResults;
import portal.util.file.FileUploadUtil;
import portal.util.session.SessionUtil;

@Controller
public class BannerController {
	protected final Log log = LogFactory.getLog(getClass());
	
	@Resource(name="bannerService")
	private BannerService bannerService;
	
	@Autowired
	private StaffMenuController staffMenuController;
	
	@Autowired
	private CodeController codeController;
	
	
	/**
	 * 베너 리스트
	 * @param sessionDomain
	 * @param request
	 * @param response
	 * @param bannerDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/banner/bannerList.do", method=RequestMethod.GET)
	public String bannerList(HttpServletRequest request
						   , HttpServletResponse response
						   , BannerDTO bannerDTO
						   , ModelMap model) throws Exception{
		
		ListDTO listVO = bannerService.selectBannerList(bannerDTO);
		
		model.addAttribute("listVO", listVO);
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&","banner_id","mode"));
		
		listVO = null;
		return BaseUtil.getBaseURI(request);
	}
	
	/**
	 * 베너 상세페이지
	 * @param sessionDomain
	 * @param request
	 * @param bannerDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/banner/bannerView.do", method=RequestMethod.GET)
	public String bannerView(HttpServletRequest request
						  	,BannerDTO bannerDTO
						  	,ModelMap model) throws Exception{
		
		
		// 정보 조회
		BannerDTO resultVO = bannerService.selectBannerInfo(bannerDTO);
		resultVO.setFile_path(SecurityUtil.encode(BaseUtil.getProp(request, "UPLOAD_PATH")+resultVO.getFile_path()+"\\"+resultVO.getSystem_file_nm()));
		resultVO.setSystem_file_nm(SecurityUtil.encode(resultVO.getFile_nm()));

		model.addAttribute("bannerDTO", resultVO);
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&", new String[]{"banner_id","mode"}));
		
		resultVO   = null;
		return BaseUtil.getBaseURI(request);
	}
	
	/**
	 * 배너 등록/수정화면
	 * @param sessionDomain
	 * @param request
	 * @param response
	 * @param trainingDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/banner/bannerWrite.do", method=RequestMethod.GET)
	public String bannerWrite( 	HttpServletRequest request
						    	,BannerDTO bannerDTO
						    	,ModelMap model) throws Exception{
		
		if("M".equals(bannerDTO.getMode())){
			// 정보 조회
			BannerDTO resultVO = bannerService.selectBannerInfo(bannerDTO);
			model.addAttribute("bannerDTO", resultVO);
			resultVO = null;
		}

		model.addAttribute("searchParam", BaseUtil.getParams(request, "&","banner_id","mode"));
		return BaseUtil.getBaseURI(request);
	}
	
	/**
	 * 배너 등록/수정 트랜잭션
	 * @param sessionDomain
	 * @param request
	 * @param response
	 * @param bannerDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/banner/bannerWriteDo.do", method=RequestMethod.POST)
	public String bannerWriteDo(HttpServletRequest request
							    ,BannerDTO bannerDTO
							    ,ModelMap model )throws Exception {
		String msgCode		= "";
		int	result			= 0;
		StaffDTO staffDTO 	= (StaffDTO)SessionUtil.getSession(request, "avo", new StaffDTO());
		
		bannerDTO.setReg_id( staffDTO.getStaff_id());
		bannerDTO.setMod_id( staffDTO.getStaff_nm());
		bannerDTO.setRemote_ip(BaseUtil.getIp(request));
		
		if(bannerDTO.getLink_url().equals("")){
			bannerDTO.setLink_url("#none");
		}
		//배너 수정인 경우
		if("M".equals(bannerDTO.getMode())){
			result  = bannerService.updateBannerInfo(bannerDTO);
 			msgCode = "modify";
		
		//배너 등록인 경우
		}else{
			result  = bannerService.insertBannerInfo(bannerDTO);
			msgCode = "insert";				
		}
		
		
		//파일등록
		if (result > 0){
			//첨부파일 처리
			FileUploadResults fileResult = FileUploadUtil.uploadForDirectory(request, BaseUtil.getProp(request, "UPLOAD_PATH"), "banner");
			
			for(int i = 0; i<fileResult.getUploadFiles().size(); i++){
				bannerDTO.setFile_nm(fileResult.getOriginalFilenames().get(i));
				bannerDTO.setSystem_file_nm(fileResult.getNewFilenames().get(i));
				bannerDTO.setFile_path(fileResult.getUploadRelPath());
			}
			
			if(fileResult.getUploadFiles().size() > 0){
			bannerService.updateBannerFile(bannerDTO);
			msgCode = "success."+msgCode;
			}
			
		} else {
			msgCode = "fail."+msgCode;
		}
		
		return new StringBuffer().append("redirect:/operation/message.do?msgCode="+msgCode)
				  .append("&")
				  .append("forwardUrl=/operation/banner/bannerList").toString();
		
	}
	
}
