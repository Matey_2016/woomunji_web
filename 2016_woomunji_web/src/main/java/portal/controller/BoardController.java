package portal.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import portal.dto.BoardDTO;
import portal.dto.CodeDTO;
import portal.dto.ListDTO;
import portal.dto.StaffDTO;
import portal.service.BoardService;
import portal.util.BaseUtil;
import portal.util.session.SessionUtil;

@Controller
@SessionAttributes("SESSIONDOMAIN")
public class BoardController {
	protected final Log log = LogFactory.getLog(getClass());
	
	@Resource(name="boardService")
	private BoardService boardService;
	
	@Autowired
	private CodeController codeController;
	
	
	/**
	 * 게시판목록(관리자) 
	 * @param sessionDomain
	 * @param request
	 * @param boardDTO
	 * @return
	 * @throws Exception
	 */ 
	@RequestMapping(value="/operation/board/boardList.do",method=RequestMethod.GET)
	public String boardList(HttpServletRequest request,
							BoardDTO boardDTO,
							ModelMap model)  throws Exception {
		
		ListDTO listVO = boardService.boardList(boardDTO);
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&amp;", "board_id"));
		model.addAttribute("listVO", listVO);
		
		listVO = null;
		return "/operation/board/boardList";   
	}
	
	/**
	 * 게시판 등록 및 수정화면
	 * @param sessionDomain
	 * @param request
	 * @param boardDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/board/boardWrite.do",method=RequestMethod.GET)
	public String boardWrite(HttpServletRequest request,
							 BoardDTO boardDTO,
							 ModelMap model)  throws Exception {
		
		if (boardDTO.getMode().equals("M")){
			boardDTO = boardService.boardView(boardDTO);
			boardDTO.setMode("M");
		} 
		
		List<CodeDTO> codeList = codeController.getCodeList("CODE");
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&amp;", "mode","board_id"));
		model.addAttribute("resultVO", boardDTO);
		model.addAttribute("codeList", codeList);
		
		boardDTO = null;
		codeList = null;
		return "/operation/board/boardWrite";   
	}
	
	/**
	 * 게시판 등록
	 * @param sessionDomain
	 * @param request
	 * @param boardDTO
	 * @param model
	 * @param bindingResult
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/board/boardWriteDo.do", method=RequestMethod.POST)
	public String boardWriteDo( HttpServletRequest request,
								BoardDTO boardDTO,
								ModelMap model) throws Exception{
		
		String msgCode	  = boardDTO.getMode().equals("C") ? "insert" : "modify";
		int		result	  = 0;	
		
		boardDTO.setRemote_ip(BaseUtil.getIp(request));
		boardDTO.setReg_id(((StaffDTO)SessionUtil.getSession(request, "avo", new StaffDTO())).getStaff_id());
		boardDTO.setMod_id(((StaffDTO)SessionUtil.getSession(request, "avo", new StaffDTO())).getStaff_id());
		
		if (boardDTO.getMode().equals("C")) {
			result = boardService.insertBoard(boardDTO);
		} else {
			result = boardService.updateBoard(boardDTO);
		}
		
		
		if (result > 0) {
			msgCode = "success."+msgCode;  
		} else {	
			msgCode = "fail."+msgCode;  
		}
		
		return new StringBuffer().append("redirect:/operation/message.do?msgCode="+msgCode)
								 .append("&forwardUrl=").append("/operation/board/boardList")
								 .append("&")
								 .append(BaseUtil.getSearchParams(request, "&","board_id")).toString();   
		
	}
	
}
