package portal.controller;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import portal.dto.CodeDTO;
import portal.dto.ListDTO;
import portal.dto.StaffDTO;
import portal.service.StaffService;
import portal.util.BaseUtil;
import portal.util.SecurityUtil;
import portal.util.session.SessionUtil;

@Controller
public class StaffController {
	protected final Log log = LogFactory.getLog(getClass());
	
	@Resource(name ="staffService")
	private StaffService staffService;
	
	@Autowired
	private CodeController codeController;
	
	/**
	 * 관리자 로그인 페이지
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/login.do",method=RequestMethod.GET)
	public String staffLogin(	HttpServletRequest request,
								StaffDTO staffDTO ) throws Exception {
		
		
		// 로그인 기록이 없는 경우
		if(SessionUtil.getSession(request, "avo", staffDTO) == null){   
			return "/operation/login";
			
		// 로그인 상태로 판단
		}else{  				
			return "/operation/include/contents";
			
		}
		
	}
	
	
	/**
	 * 관리자 로그인 처리
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/operation/login.do",method=RequestMethod.POST)
	public void staffLoginDo(HttpServletRequest request,
								HttpServletResponse response,
								StaffDTO staffDTO ) throws Exception {
		
		JSONObject 	json = new JSONObject();
		staffDTO.setPasswd(SecurityUtil.getMD5String(staffDTO.getPasswd()));
		staffDTO = staffService.staffLoginDo(staffDTO);
		
        if (staffDTO != null){
        	json.put("result", "T");
        	SessionUtil.setSession(request, "avo", staffDTO);
        	
        } else {
        	json.put("result", "F");
        }
        
		response.setContentType("application/json; charset=utf-8");
		response.getWriter().write(json.toString());
		
	}
	
	
	/**
	 * 관리자 메인페이지
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/contents.do",method=RequestMethod.GET)
	public String staffMain(HttpServletRequest request,
							StaffDTO staffDTO,
							ModelMap model) throws Exception {
		
		return "/operation/include/contents";
	}
	
	
	/**
	 * 운영자관리 리스트 호출
	 * @param sessionDomain
	 * @param request
	 * @param response
	 * @param staffDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/staff/staffList.do",method={RequestMethod.GET,RequestMethod.POST})
	public String staffList(HttpServletRequest request
						  , HttpServletResponse response
						  , StaffDTO staffDTO
						  , ModelMap model) throws Exception{
		
		ListDTO listVO = staffService.selectStaffList(staffDTO);
		
		model.addAttribute("listVO", listVO);
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&"));
		listVO = null;
		
		return BaseUtil.getBaseURI(request);
	}
	
	
	/**
	 * 운영자 정보 
	 * @param request
	 * @param staffDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/staff/staffView.do",method=RequestMethod.GET)
	public String staffView(HttpServletRequest request
							,StaffDTO staffDTO
							,ModelMap model) throws Exception{
		
		StaffDTO resultVO = staffService.selectStaffInfo(staffDTO);
		
		// 운영자별 관리권한 정보
		List<StaffDTO> authInfoDTO = staffService.selectStaffAuthInfoList(staffDTO);
		model.addAttribute("authInfoList", authInfoDTO);
		model.addAttribute("staffDTO", resultVO);
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&", "staff_id"));
		
		resultVO = null;
		return BaseUtil.getBaseURI(request);
	}
	
	
	/**
	 * 운영자 정보 등록 화면
	 * @param request
	 * @param staffDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/staff/staffWrite.do",method=RequestMethod.GET)
	public String staffWrite(HttpServletRequest request
						    ,StaffDTO staffDTO
						    ,ModelMap model) throws Exception{
		
		
		if(staffDTO.getMode().equals("M")){
			StaffDTO resultVO = staffService.selectStaffInfo(staffDTO);
			model.addAttribute("staffDTO", resultVO);
			resultVO = null;
		}
		
		// 운영자별 관리권한 정보
		List<StaffDTO> authInfoDTO = staffService.selectStaffAuthInfoList(staffDTO);
		model.addAttribute("authInfoList", authInfoDTO);
		List<CodeDTO> codeListDTO = codeController.getCodeList("ADMIN_FLAG");
		model.addAttribute("codeList", codeListDTO);
		
		authInfoDTO	= null;
		codeListDTO = null;
		
		model.addAttribute("searchParam", BaseUtil.getParams(request, "&","staff_id","mode"));
		return BaseUtil.getBaseURI(request);
	}
	
	
	
	
	/**
	 * 운영자 정보 등록 화면
	 * @param request
	 * @param staffDTO
	 * @param model
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/operation/staff/staffWriteDo.do",method=RequestMethod.POST)
	public String staffWriteDo(HttpServletRequest request
							  ,StaffDTO staffDTO
							  ,ModelMap model) throws Exception{
		
		String[] menu_id = request.getParameterValues("menu_id");
		int		rsltCnt = 0;
		
		staffDTO.setRemote_ip(BaseUtil.getIp(request));
		staffDTO.setReg_id(((StaffDTO)SessionUtil.getSession(request, "avo", staffDTO)).getStaff_id());
		
		
		if (staffDTO.getMode().equals("M")){
			if (!staffDTO.getPasswd().equals("")){
				staffDTO.setPasswd(SecurityUtil.getMD5String(staffDTO.getPasswd()));
			}
			rsltCnt = staffService.updateStaffInfo(staffDTO);
		} else {
			if (staffService.checkStaffId(staffDTO) == 0) {
				staffDTO.setPasswd(SecurityUtil.getMD5String(staffDTO.getPasswd()));
				rsltCnt = staffService.insertStaffInfo(staffDTO);
			}
		}
		
		if (rsltCnt > 0){
			staffService.initStaffAuth(staffDTO);
			for (int i=0; i<menu_id.length; i++){
				StaffDTO sDTO = new StaffDTO();
				sDTO.setcChk(request.getParameter("cChk"+i)==null?"N":request.getParameter("cChk"+i));
				sDTO.setrChk(request.getParameter("rChk"+i)==null?"N":request.getParameter("rChk"+i));
				sDTO.setuChk(request.getParameter("uChk"+i)==null?"N":request.getParameter("uChk"+i));
				sDTO.setdChk(request.getParameter("dChk"+i)==null?"N":request.getParameter("dChk"+i));
				sDTO.setMenu_id(menu_id[i]);
				sDTO.setStaff_id(staffDTO.getStaff_id());
				staffService.insertStaffAuth(sDTO);
			}
		
		}
		
		return "redirect:"+BaseUtil.getBaseURI(request, "WriteDo","List.do");
	}
	
	
	@RequestMapping(value="/operation/login/logout.do",method=RequestMethod.GET)
	public String logout(HttpServletRequest request
							  ,StaffDTO staffDTO
							  ) throws Exception{
		HttpSession session = request.getSession();		
		session = request.getSession(true);
		session.setAttribute("avo", null);
		session.setAttribute("lnb_id", null);
		return "redirect:/operation/login.do";
	}
}
