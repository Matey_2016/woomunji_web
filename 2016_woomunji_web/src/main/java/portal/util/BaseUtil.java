package portal.util;

import java.io.FileInputStream;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.text.NumberFormat;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

public class BaseUtil {
	/** 컨텐츠에 HTML 태그가 포함되어 있는지 여부를 비교하는 정규표현식 문자열 */
	private static String COMPARE_HTML_REGEX;
	
	
	/** 
	 * 파라미터 util 시작
	 * */
	
	public static String getParameter(HttpServletRequest request, String paramName) {
		return getRplcAll(request.getParameter(paramName) == null? "" : request.getParameter(paramName));
	}
	
	public static String getParameter(HttpServletRequest request, String paramName, String setValue) {
		return getRplcAll(request.getParameter(paramName) == null? setValue : request.getParameter(paramName));
	}
	
	public static String getParams(HttpServletRequest request) {
		return getParams(request, null, "");
	}
	
	public static String getParams(HttpServletRequest request, String ampChar) {
		return getParams(request, ampChar, "");
	}
	
	/**
	 * int형 파라미터 리턴
	 * 
	 * @param request
	 * @param parameter
	 * @return 파라미터가 없으면 0 리턴
	 */
	public static int getIntParameter(HttpServletRequest request, String parameter) {
		return getIntParameter(request, parameter, 0);
	}
	
	
	/**
	 * int형 파라미터 리턴
	 * 
	 * @param request
	 * @param parameter
	 * @param defaultValue
	 * @return 파라미터가 없으면 defaultValue 리턴
	 */
	public static int getIntParameter(HttpServletRequest request, String parameter, int defaultValue) {
		int value = defaultValue;
		if ( request.getParameter(parameter) != null ) {
			try {
				value = Integer.parseInt(request.getParameter(parameter));
			} catch (NumberFormatException ex) { }
		}
		return value;
	}
	
	/**
	 * exclusiveParam 과 내장된 검사조건에 걸리는 파라미터만 뺀 나머지 파라미터 리턴.
	 * @param request
	 * @param ampChar
	 * @param exclusiveParam
	 * @return
	 */
	public static String getParams(HttpServletRequest request, String ampChar, String exclusiveParam) {
		Enumeration paramNames = request.getParameterNames();
		ampChar = (ampChar == null)? "&amp;" : ampChar;
		exclusiveParam = (exclusiveParam == null)? "" : exclusiveParam;
		StringBuffer params = new StringBuffer();
		while (paramNames.hasMoreElements()) {
			try {
				String k = (String) paramNames.nextElement();
				String v = java.net.URLEncoder.encode(request.getParameter(k), "UTF-8");
				if (!k.equals(exclusiveParam) && (v != null && v.trim().equals("") == false)) {
					if (params.length() > 0) {
						params.append(ampChar).append(k).append("=").append(v);
					} else {
						params.append(k).append("=").append(v);
					}
				}
			} catch (Exception ex) { ex.printStackTrace(); }
		}
		return params.toString();
	}
	
	
	public static String getBaseURI(HttpServletRequest request, String ... params){
		
		String baseURI 	= request.getAttribute("baseURI").toString().replaceAll(".do","");
		String append	= "";
		
		
		if (params.length > 0) {
			for (int i = 0; i < params.length; i++) {
				if (i == 0){
					baseURI = baseURI.replaceAll(params[0], "");
				}
				
				if (i == 1){
					append = params[i];
				}
			}
			
		}
		return baseURI+append;
	}
	
	
	/**
	 * exclusiveParam 과 내장된 검사조건에 걸리는 다수의 파라미터만 뺀 나머지 파라미터 리턴.
	 * @param request
	 * @param ampChar
	 * @param exclusiveParams
	 * @return
	 */
	public static String getParams(HttpServletRequest request, String ampChar, String ... exclusiveParams) {
		Enumeration paramNames = request.getParameterNames();
		ampChar = (ampChar == null)? "&amp;" : ampChar;
		StringBuffer params = new StringBuffer();
		while (paramNames.hasMoreElements()) {
			try {
				String k = (String) paramNames.nextElement();
				String v = java.net.URLEncoder.encode(request.getParameter(k), "UTF-8");
				if (isNotExistinExclusiveParams(k,exclusiveParams)) {
					if (v != null && v.trim().equals("") == false) {
						if (params.length() > 0) {
							params.append(ampChar).append(k).append("=").append(v);
						} else {
							params.append(k).append("=").append(v);
						}
					}
				}
			} catch (Exception ex) { ex.printStackTrace(); }
		}		
		return params.toString();
	}
	
	public static String getAllParams(HttpServletRequest request, String exclusiveParam, String ampChar) throws UnsupportedEncodingException {
		Enumeration paramNames = request.getParameterNames();
		String[] exclusiveParams = {};
		if (exclusiveParam != null) {
			exclusiveParams = exclusiveParam.split(",");
		}
		StringBuffer param = new StringBuffer();
		while (paramNames.hasMoreElements()) {
			String k = (String) paramNames.nextElement();
			String v = URLEncoder.encode(request.getParameter(k), "UTF-8");
			if (isExists(exclusiveParams, k) == false && !k.equals("resultCode")) { 
				param.append(ampChar + k + "=" + v);
			}
		}
		return param.toString();
	}
    public static String getAllParams(HttpServletRequest request, String exclusiveParam) throws UnsupportedEncodingException {
    	return getAllParams(request, exclusiveParam, "&amp;");
	}
	public static String getAllParams(HttpServletRequest request) throws UnsupportedEncodingException {
		return getAllParams(request, null);
	}
	/** 접두어가 s_ 로 시작되는 게시판 조회용 파라미터만 리턴
	 * @param request
	 * @param ampChar
	 * @return String
	 */
	public static String getSearchParams(HttpServletRequest request, String ampChar) {
		return getSearchParams(request, ampChar, "");
	}
	
	public static String getSearchParams(HttpServletRequest request, String ampChar, String ... exclusiveParams) {
		Enumeration paramNames = request.getParameterNames();
		ampChar = (ampChar == null)? "&amp;" : ampChar;
		StringBuffer params = new StringBuffer();
		while (paramNames.hasMoreElements()) {
			try {
				String k = (String) paramNames.nextElement();
				String v = java.net.URLEncoder.encode(request.getParameter(k), "UTF-8");
				if (
						(k.equals("cpage") || k.startsWith("s_"))
						&& (v != null && v.trim().equals("") == false)
						&& isNotExistinExclusiveParams(k, exclusiveParams)
				) {
					if (params.length() > 0) {
						params.append(ampChar).append(k).append("=").append(v);
					} else {
						params.append(k).append("=").append(v);
					}
				}
			} catch (Exception ex) { ex.printStackTrace(); }
		}
		return params.toString();
	}
	
	public static boolean isNotExistinExclusiveParams(String value, String[] exclusiveParams) {
		boolean result = true;
		if (exclusiveParams == null) { return result; }
		
		for (String compareValue : exclusiveParams) {
			if (value.equals(compareValue) || value.equals("auth")) {
				return false;
			}
		}
		return result;
	}
	
    private static boolean isExists(String[] array, String key) {
    	for (String a : array) {
    		if (a.equals(key)) {
    			return true;
    		}
    	}
    	return false;
    }
	/** 
	 * 파라미터 util 끝
	 * */
	
	
	/**
	 * 문자열을 html 태그로 변환 시작
	 *  
	 */
	
	/** JAVA String 을 HTML 형식으로 변환.
	 * @param value
	 * @param escapeSpace
	 * @return
	 */
	public static String convertHTML(String value, String escapeSpace) {
		if (value == null || value.isEmpty()) return "";
		char[] arr = value.toCharArray();
		StringBuffer sb = new StringBuffer();
		
		for (char c : arr) {
			switch (c) {
				case '\n'		: sb.append("<br />"); break;
				case '\t'		: if (escapeSpace.toLowerCase().equals("y")) { sb.append(c); break; } sb.append("&nbsp;&nbsp;&nbsp;"); break;
				default		: sb.append(c);
			}
		}
		return sb.toString();
	}
	
	/** 에디터 엔터(<p>&amp;</p>), 빈문자열(&amp;) 변환
	 * @param str
	 * @return
	 */
	public static String editorsTrim(String str) {
		if (str == null) { return ""; }
		return str.trim().replace("\n","").replace("&nbsp;", "").replace("<P></P>", "");
	}
	
	/**
	 * String 문자열을 html 문자열로 변경
	 * @param msg
	 * @return
	 */
	public static String clobToHtml(String msg){
		// smart editor사용할 때에는 필요없다.
		msg = rplc(msg,"\n","<br>");
		msg = rplc(msg," ","&nbsp;");
		msg = rplc(msg,"\t","&nbsp;&nbsp;&nbsp;&nbsp;");
		
		return msg;		
	}
	
	/** 문자열에 HTML 태그가 포함되어 있는지 여부 확인 후 결과 boolean 리턴
	 * @param value
	 * @return boolean
	 */
	private static boolean hasHTMLTag (String value) {
		if (COMPARE_HTML_REGEX == null) {
			setCompareHtmlRegex();
		}
		if (value != null && value.contains("<")) {
			return value.matches(COMPARE_HTML_REGEX);
		}
		return false;
	}
	
    /** 문자열을 HTML 표현 태그로 변환 후 리턴
	 * @param str
	 * @return String
	 */
	public static String convertStringToHTMLTag (String str) {
		if (str != null && !str.trim().equals("")) {
			if (hasHTMLTag(str)) {
				return str;
			}
			char[] arr = str.toCharArray();
			StringBuffer sb = new StringBuffer();

			for (char c : arr) {
				switch (c) {
				    case '\r' : break;
					case '\n'	: sb.append("<br />");	break;
					case '<'	: sb.append("&lt;");	break;
					case '>'	: sb.append("&gt;");	break;
					case ' '	: sb.append("&nbsp;");	break;
					case '\t'	: sb.append("&nbsp;&nbsp;&nbsp;"); break;
					default	: sb.append(c);
				}
			}
			str = sb.toString();
		}
		return str;
	}
	
	
	/**
	 * 문자열을 html 태그로 변환 끝
	 */
	
	
	/**
	 * 아이디 및 비밀번호 생성 및 유효성 체크 시작
	 */
	/** 사용자 ID 유효성체크 [첫글자 영소문자, 영소문자+숫자조합, 6~20자리]
	 * @param userId
	 * @return
	 * @throws Exception
	 */
	public static boolean isUsableUserId(String userId) throws Exception {
		if (userId == null 
				|| userId.trim().equals("")
				|| (userId.length() < 6 || userId.length() > 20)) { 
			return false; 
		}
		
		int strCount = 0;
		int intCount = 0;
		int etcCount = 0;
		
		char[] charArr = userId.toCharArray();
		for (int i=0; i < charArr.length; i++) {
			char c = charArr[i];
			if (i == 0 && (c < 97 || c > 122)) { 
				return false; 
			}
			if (c >= 97 && c <= 122) { 
				strCount++; 
			} else if (c >= 48 && c <= 57) { 
				intCount++; 
			} else {
				etcCount++;
			}
		}
		
		if (0 < strCount && 0 < intCount && etcCount == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	private static char[] charArr = {'a','b','c','d','e','f','g','h','i'
									,'j','k','l','m','n','o','p','q','r'
									,'s','t','u','v','w','x','y','z'};
	/**
	 * 난수 발생을 하여 문자 조합
	 * @param random
	 * @return
	 */
	private static String getRandomValue(Random random) {
		String result = "";
		int type = random.nextInt(2);
		if (type == 0) {
			result = charArr[random.nextInt(26)] + "";
		} else {
			result = random.nextInt(10) + "";
		}
		return result;
	}
	
	/** 임시비밀번호 생성
	 * @return
	 */
	public static String createNewPassword() {
		Random random = new Random();
		StringBuffer sb = new StringBuffer();
		sb.append(charArr[random.nextInt(26)])
		  .append(getRandomValue(random))
		  .append(getRandomValue(random))
		  .append(getRandomValue(random))
		  .append(getRandomValue(random))
		  .append(getRandomValue(random))
		  .append(getRandomValue(random))
		  .append(getRandomValue(random));
		return sb.toString();
	}
	/**
	 * 아이디 및 비밀번호 생성 및 유효성 체크 끝
	 */
	
	
	
	/**
	 * 문자열 중 특정 문자을 찾아 변경
	 * @param str
	 * @param n1
	 * @param n2
	 * @return
	 */
	public static String rplc(String str, String n1, String n2) {
		int itmp = 0;
		if (str==null) return "";
		
		String tmp = str;
		StringBuffer sb = new StringBuffer();
		sb.append("");
		while (tmp.indexOf(n1)>-1) {
			itmp = tmp.indexOf(n1);
			sb.append(tmp.substring(0,itmp));
			sb.append(n2);
			tmp = tmp.substring(itmp+n1.length());
		}
		sb.append(tmp);
		return sb.toString();
	}
	
	/**
	 * 바이트 수치의 파일 크기를 kb,mb,gb 수치로 변경
	 * @param size
	 * @return
	 */
	public static String byteTranslater(long size) {
	  NumberFormat nf = NumberFormat.getIntegerInstance();
	  java.text.DecimalFormat df = new java.text.DecimalFormat("#,##0.00");
	  int intSize = 0;
	  int kbyteSize = 1024;
	  double doubleSize = 0;
	  String returnSize = null;
	 
	  if (size >= (1000 * 1024 * 1024)) {
	      intSize = new Long(size / (1000 * 1024 * 1024)).intValue();
	 
	      return nf.format(intSize) + "GB";
	  } else if (size > (kbyteSize * 1024)) {
	      intSize = (int) (((double) size) / ((double) (kbyteSize * 1024)) * 100);
	      doubleSize = (double) (((double) intSize) / 100);
	      returnSize = df.format(doubleSize);
	 
	      if (returnSize.lastIndexOf(".") != -1) {
	   if ("00".equals(returnSize.substring(returnSize.length() - 2,
	    returnSize.length()))) {
	       returnSize = returnSize.substring(0, returnSize
	        .lastIndexOf("."));
	   }
	      }
	 
	      return returnSize + "MB";
	  } else if (size > kbyteSize) {
	      intSize = new Long(size / kbyteSize).intValue();
	      return nf.format(intSize) + "KB";
	  } else {
	      // return nf.format(size) + "Byte";
	      return "1KB";
	  }
	}
	
	public static String getRplcAll(String param){
		if ( param != null && param.length() > 0 ) {
			param = param.replaceAll("&" ,"&amp;");
			param = param.replaceAll("<","&lt;" );
			param = param.replaceAll(">","&gt;");
			param = param.replaceAll("\\(","&#40;" );
			param = param.replaceAll("\\)","&#41;" );
			param = param.replaceAll("\"" ,"&quot;" );
			param = param.replaceAll("\'" ,"&apos;");
		}		
		return param;
	}
	
	public static String getDeRplcAll(String param){
		if ( param != null && param.length() > 0 ) {
			param = param.replaceAll("&amp;", "&");
			param = param.replaceAll("&lt;", "<");
			param = param.replaceAll("&gt;",">");		
			param = param.replaceAll("&#40;", "\\(");
			param = param.replaceAll("&#41;", "\\)");
			param = param.replaceAll("&quot;", "\"");
			param = param.replaceAll("&apos;","\'");
		}
		return param;
	}
	
	// 
	public static String getFileNameRplcAll(String filename){
		if ( filename != null && filename.length() > 0 ) {
			filename = filename.replaceAll("/", "");
		    filename = filename.replaceAll("\\", "");
		    filename = filename.replaceAll(".", " ");
		    filename = filename.replaceAll("&", " ");
		}
		return filename;
	}

	
	
	
	/**
	 * 파일 해쉬값 추출
	 * @param fileName
	 * @return
	 */
	public static String getFileHash(String fileName){
		String SHA = ""; 
		int buff = 16384;
		try {
			RandomAccessFile file = new RandomAccessFile(fileName, "r");
			MessageDigest hashSum = MessageDigest.getInstance("MD5");
			byte[] buffer = new byte[buff];
			byte[] partialHash = null;
			long read = 0;
			long offset = file.length();
			int unitsize;
			while (read < offset) {
				unitsize = (int) (((offset - read) >= buff) ? buff : (offset - read));
				file.read(buffer, 0, unitsize);
				hashSum.update(buffer, 0, unitsize);
				read += unitsize;
			}
			file.close();
			partialHash = new byte[hashSum.getDigestLength()];
			partialHash = hashSum.digest();
			StringBuffer sb = new StringBuffer(); 
			for(int i = 0 ; i < partialHash.length ; i++){
				sb.append(Integer.toString((partialHash[i]&0xff) + 0x100, 16).substring(1));
			}
			SHA = sb.toString();
		} catch (Exception e) {
		e.printStackTrace();
		}
		return SHA;
	}
	
    private static void setCompareHtmlRegex() {
		COMPARE_HTML_REGEX = "(?is).*<(br|table|img|p|div|span|frame|iframe|http)[^<>]*[/]?>.*";
	}
    
    
	
	public static boolean regExpCheck(String param, String regExp) {
		Pattern pattern = Pattern.compile(regExp);
		Matcher matcher = pattern.matcher(param);
		
		return matcher.matches();
	}
	
	public static String getBoardHeaderSubString(String boardHeard){
		int cnt 				= 0;
		String retBoardHeard 	= "";
		try{
		if(boardHeard.indexOf("\n") == -1){
			retBoardHeard = boardHeard;
		}else{
			while (boardHeard != null) {
				cnt = cnt + 1;
				
				String subStrHeard = boardHeard.substring(0,boardHeard.indexOf("\n"));
				boardHeard = boardHeard.substring(boardHeard.indexOf("\n")+1);
				
				int subStrHeardLength  = subStrHeard.length();
				int ni = subStrHeardLength/38;
				int nii = subStrHeardLength%38;
				if(nii==0){
					cnt = cnt + (ni-1);
				}else if(nii > 0){
					cnt = cnt + ni;
				}
				if(cnt >= 7 && subStrHeard.length() > 38){
					subStrHeard = subStrHeard.substring(0, 38);
				}
				retBoardHeard = retBoardHeard+subStrHeard+"\n";
				if(cnt >= 7 || boardHeard.indexOf("\n") == -1){
					break;
				}
			}
		}
		}catch(Exception e){
			
		}
		return retBoardHeard;
	}
	
	public static String getBoardMainHeaderSubString(String boardHeard){
		int cnt 				= 0;
		String retBoardHeard 	= "";
		try{
		if (boardHeard != null && !boardHeard.equals("")) {
			retBoardHeard = boardHeard.substring(0,boardHeard.indexOf("\n"));
		}
		}catch(Exception e){
			
		}
		return retBoardHeard;
	}
	
	/**
	 * 본인인증 모듈 
	 * @param paramValue
	 * @param gubun
	 * @return
	 */
	public static String requestReplace (String paramValue, String gubun) {
        String result = "";
        
        if (paramValue != null) {
        	
        	paramValue = paramValue.replaceAll("<", "&lt;").replaceAll(">", "&gt;");

        	paramValue = paramValue.replaceAll("\\*", "");
        	paramValue = paramValue.replaceAll("\\?", "");
        	paramValue = paramValue.replaceAll("\\[", "");
        	paramValue = paramValue.replaceAll("\\{", "");
        	paramValue = paramValue.replaceAll("\\(", "");
        	paramValue = paramValue.replaceAll("\\)", "");
        	paramValue = paramValue.replaceAll("\\^", "");
        	paramValue = paramValue.replaceAll("\\$", "");
        	paramValue = paramValue.replaceAll("'", "");
        	paramValue = paramValue.replaceAll("@", "");
        	paramValue = paramValue.replaceAll("%", "");
        	paramValue = paramValue.replaceAll(";", "");
        	paramValue = paramValue.replaceAll(":", "");
        	paramValue = paramValue.replaceAll("-", "");
        	paramValue = paramValue.replaceAll("#", "");
        	paramValue = paramValue.replaceAll("--", "");
        	paramValue = paramValue.replaceAll("-", "");
        	paramValue = paramValue.replaceAll(",", "");
        	
        	if(gubun != "encodeData"){
        		paramValue = paramValue.replaceAll("\\+", "");
        		paramValue = paramValue.replaceAll("/", "");
            paramValue = paramValue.replaceAll("=", "");
        	}
        	
        	result = paramValue;
            
        }
        return result;
	}

	public static String splitLast(String paramValue, String gubun) {
		String splitData [] = paramValue.split("\\"+gubun);
		String splitLastValue = splitData [(splitData .length)-1];
		return splitLastValue.toUpperCase();
	}
	
	/**
	 * null 체크
	 * @param str
	 * @return
	 */
	public static String nchk(String str) {
		return (str==null)?"": str;
	}
	
	@SuppressWarnings("deprecation")
	public static String getProp(HttpServletRequest request, String prop){
		
		try{
			String propFile = request.getRealPath("/")+"WEB-INF\\config\\config\\config.properties";
            Properties props = new Properties();// 프로퍼티 객체 생성
            FileInputStream fis = new FileInputStream(propFile);// 프로퍼티 파일 스트림에 담기
            props.load(new java.io.BufferedInputStream(fis));// 프로퍼티 파일 로딩
            return props.getProperty(prop);
			
		}catch (Exception e){
			
		}
		
		
		return "";
	}
	
	/**
	 * 사용자 아이피 호출
	 * @param request
	 * @return
	 */
	public static String getIp(HttpServletRequest request) {
		return request.getRemoteAddr();
	}
	
	
	// url 입력 후 페이지 접근시 제어 함수
	public static boolean isReferer(HttpServletRequest request){
		String referer = request.getHeader("referer");
		
		if (referer == null){
			return true;
		} else {
			return false;
		}
	}
	
	public static String removeTag(String str){  
		  Matcher mat;  
		  // script 처리
		  Pattern script = Pattern.compile("<(no)?script[^>]*>.*?</(no)?script>",Pattern.DOTALL); 
		  mat = script.matcher(str); 
		  str = mat.replaceAll(""); 
		  // style 처리
		  Pattern style = Pattern.compile("<style[^>]*>.*</style>",Pattern.DOTALL); 
		  mat = style.matcher(str); 
		  str = mat.replaceAll(""); 
		  // tag 처리
		  Pattern tag = Pattern.compile("<(\"[^\"]*\"|\'[^\']*\'|[^\'\">])*>"); 
		  mat = tag.matcher(str); 
		  str = mat.replaceAll(""); 
		  // ntag 처리
		  Pattern ntag = Pattern.compile("<\\w+\\s+[^<]*\\s*>"); 
		  mat = ntag.matcher(str); 
		  str = mat.replaceAll(""); 
		  // entity ref 처리
		  Pattern Eentity = Pattern.compile("&[^;]+;"); 
		  mat = Eentity.matcher(str); 
		  str = mat.replaceAll("");
		  // whitespace 처리
		  Pattern wspace = Pattern.compile("\\s\\s+"); 
		  mat = wspace.matcher(str);
		  str = mat.replaceAll("");           
		  return str ;  
		  }
}