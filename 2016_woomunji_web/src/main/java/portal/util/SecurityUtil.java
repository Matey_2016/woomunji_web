/**
 * 파일 경로를 암복화하는 모듈
 */
package portal.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

/** 3DES 알고리즘을 이용한 암/복호화
 * @author scaler
 *
 */
public class SecurityUtil {

	private static Key key = null;	// JVM에 따라 다른 encrytion코드를 발행한다. 사용불가
	private static final String SALT="SomewhereOverTheRainbow#@";
	private static final ShaPasswordEncoder pe=new ShaPasswordEncoder();

	static {
		if (key == null) {
			// Key 초기화
			KeyGenerator keyGenerator;
			try {
				keyGenerator = KeyGenerator.getInstance("TripleDES");
				keyGenerator.init(168);
				key = keyGenerator.generateKey();
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 인코딩
	 * @param inStr
	 * @return
	 */
	public static String _encode(String inStr) {
		if (inStr==null) return null;
		StringBuffer sb = null;
		try {
			Cipher cipher = Cipher.getInstance("TripleDES/ECB/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, key);
			byte[] plaintext = inStr.getBytes("UTF8");
			byte[] ciphertext = cipher.doFinal(plaintext);

			sb = new StringBuffer(ciphertext.length * 2);
			for (int i = 0; i < ciphertext.length; i++) {
				String hex = "0" + Integer.toHexString(0xff & ciphertext[i]);
				sb.append(hex.substring(hex.length() - 2));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	/**
	 * 디코딩
	 * @param inStr
	 * @return
	 */
	public static String _decode(String inStr) {
		if (inStr==null) return null;
		String text = null;
		try {
			byte[] b = new byte[inStr.length() / 2];
			Cipher cipher = Cipher.getInstance("TripleDES/ECB/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, key);
			for (int i = 0; i < b.length; i++) {
				b[i] = (byte) Integer.parseInt(
						inStr.substring(2 * i, 2 * i + 2), 16);
			}
			byte[] decryptedText = cipher.doFinal(b);
			text = new String(decryptedText, "UTF8");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return text;
	}
	
	/**
	 * 암호화
	 * @param inStr
	 * @return
	 */
	public static String encrypt(String inStr) {
		return pe.encodePassword(inStr, SALT);
	}

	/**
	 * 인코딩
	 * @param inStr
	 * @return
	 */
	public static String encode(String inStr) {
		if (StringUtils.isBlank(inStr)) return inStr;
		DeEncrypter de=DeEncrypter.getInstance();
		String result=null;
		try {
			result=URLEncoder.encode(de.encrypt(inStr), "UTF-8").replaceAll("%", "_");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		return result;
	}
	/**
	 * 디코딩
	 * @param inStr
	 * @return
	 */
	public static String decode(String inStr) {
		if (StringUtils.isBlank(inStr)) return inStr;
		DeEncrypter de=DeEncrypter.getInstance();
		String result=null;
		try {
			result=de.decrypt(URLDecoder.decode(inStr.replaceAll("_", "%"), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	/** MD5로 암호화된 문자열 반환.
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public static String getMD5String(String str) throws Exception {
    	if (str == null) return str;
    	MessageDigest md = MessageDigest.getInstance("MD5");
    	byte[] diArr = md.digest(str.getBytes());
    	StringBuffer sb = new StringBuffer();
    	for (byte b : diArr) {
    		char c = (char)b;
    		String md5Str = String.format("%02x", 0xff&c);
    		sb.append(md5Str);
    	}
    	return sb.toString();
    }
	
	
	public static void main(String[] args) throws Exception {
//		String seed="한글_abc 1234##";
		String seed="_1!한글 # aby\\t";
		String enc=encode(seed);
		String dec=decode(enc);

		System.out.println(enc); 
		System.out.println(dec);
	}
}
