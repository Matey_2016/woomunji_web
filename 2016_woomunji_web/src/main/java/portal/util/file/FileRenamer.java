package portal.util.file;

import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** 업로드시 파일명 중복되는 경우 어떻게 할지 결정하는 로직
 * @author scaler
 *
 */
public class FileRenamer {
	private File file;
	private String baseDir;
	private String newName;
	private String oldName;

	/** 생성자
	 * @param f
	 */
	public FileRenamer(File f) {
		file=f;
		oldName=f.getName();
		doRename();
	}
	/** 바뀌기 이전의 이름
	 * @return
	 */
	public String getOldName() {
		return oldName;
	}
	/** Policy 적용후 바뀐 이름
	 * @return
	 */
	public String getNewName() {
		return newName;
	}
	/**
	 * 이름 바꾸기 작업 실행 내부 프록시
	 */
	private void doRename() {
		if (!file.exists()) {
			setNewName(newName);
		} else {
			try {
				baseDir=file.getCanonicalPath().replaceAll(file.getName()+"$", "");
				doRename(file.getName());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/** 새이름 지정
	 * @param newName
	 */
	private void setNewName(String newName) {
		this.newName=newName;
	}
	/** 실제 이름 바꾸기 작업 실행
	 * @param fname
	 */
	private void doRename(String fname) {
		File f=new File(baseDir, fname);

		if (!f.exists()) {
			setNewName(fname);
		} else {
			Pattern p=Pattern.compile("^(.+)_(\\d+)(\\.\\w+)$");
			Matcher m=p.matcher(fname);
			String newOne=null;
			if (m.find()) {
				int no=Integer.parseInt(m.group(2));
				no++;
				newOne=m.group(1)+"_"+no+m.group(3);
				doRename(newOne);
			} else {
				Pattern p2=Pattern.compile("^(.+)(\\.\\w+)$");
				Matcher m2=p2.matcher(fname);
				if (m2.find()) {
					newOne=m2.group(1)+"_1"+m2.group(2);
					doRename(newOne);
				} else {
					throw new NullPointerException("FileRenamer.doRename() regex Error!");
				}
			}

		}
	}
}