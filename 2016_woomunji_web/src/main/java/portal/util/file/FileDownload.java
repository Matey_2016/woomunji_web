package portal.util.file;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import portal.util.SecurityUtil;


/** 파일 다운로드 처리
 * @author Matey
 */
@SuppressWarnings("serial")
public class FileDownload extends HttpServlet implements Servlet {
	protected final Log log = LogFactory.getLog(getClass());
	private MessageSourceAccessor commonMessageSource;
	private String[] xssArr;
	private String baseEncoding;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
	}

	/** XSS 검사
	 * @param fileName
	 * @return
	 */
	@SuppressWarnings("unused")
	private boolean isContainsXSS(String fileName) {
		if (xssArr == null) {
			String xssStr = commonMessageSource.getMessage("app_download_xss");
			if (xssStr != null)
				xssArr = xssStr.split(",");
			for (String xss : xssArr) {
				if (fileName.contains(xss)) return true;
			}
		}
		return false;
	}
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		WebApplicationContext wac = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
		
		if (commonMessageSource == null) {
			commonMessageSource = (MessageSourceAccessor)wac.getBean("commonMessageSource",MessageSourceAccessor.class);
		}
		
		if (baseEncoding == null) {
			this.baseEncoding = commonMessageSource.getMessage("app_baseEncoding");
		} else {
			request.setCharacterEncoding(baseEncoding);
		}
		
		/* TODO DB 조회해서 파일 경로 불러와야함.
		Object obj = (Object)wac.getBean("DAO Bean Name");
		*/
		String filePath = null;
		String fileName = null;
		String operYn = "N";
		String requestURI = request.getRequestURI();
		String baseURI    = requestURI;
		
		// 암호화 풀기
		if(request.getParameter("filePath") != null && !request.getParameter("filePath").equals("")){
			filePath = SecurityUtil.decode(request.getParameter("filePath"));
		}else{
			filePath = "";
		}
		
		if(request.getParameter("fileName") != null && !request.getParameter("fileName").equals("")){
			fileName = SecurityUtil.decode(request.getParameter("fileName"));
		}else{
			fileName = "";
		}
		
		if(request.getParameter("operYn") != null && !request.getParameter("operYn").equals("")){
			operYn = SecurityUtil.decode(request.getParameter("operYn"));
		}else{
			operYn = "";
		}
		
		if ( baseURI.startsWith("/download/operation/") ) {
			operYn = "Y";			
		}
		
		File file = new File(filePath);
		if ( "".equals(fileName)) {
			fileName = file.getName();
		}
		if(file.exists())  {
        	String Agent = request.getHeader("USER-AGENT");

        	// MSIE는 ~IE10까지 존재, Trident는 IE8~IE11까지 존재. 따라서 11이상인 경우 Trident만 검출됨.
            if(Agent != null && ( Agent.indexOf("MSIE") >= 0 || Agent.indexOf("Trident") >= 0) ) {
                response.setHeader("Pragma", "public");
                response.setHeader("Expires", "0");
                response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
                response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(fileName,baseEncoding).replace("+", " ") + "\";");
                response.setHeader("Content-Type", "application-download; charset=UTF-8");
                response.setHeader("Content-Length", ""+file.length());
                response.setHeader("Content-Transfer-Encoding", "binary");
            } else {
            	response.setHeader("Content-Type", "application/download");
            	response.setHeader("Content-Length", ""+file.length());
    			response.setHeader("Content-Disposition", "attachment; filename=\"" + new String(fileName.getBytes(baseEncoding), "ISO-8859-1") + "\"");
            }
            
            BufferedInputStream fin = new BufferedInputStream(new FileInputStream(file));
            BufferedOutputStream fout = new BufferedOutputStream(response.getOutputStream());
            
            int leng = 0;
            byte b[] = new byte[5120];
            
            while( (leng = fin.read(b, 0, 5120)) != -1 ){
    			fout.write(b,0,leng);
    		}
            
            try {
            	fin.close(); fout.flush(); fout.close();
            } catch (Exception e) {
            	e.printStackTrace();
            } finally {
            	if (fout != null) { fout.close(); }
            	if (fin != null) { fin.close(); }
            }
        } else {
        	if ( "Y".equals(operYn) ) {
        		response.sendRedirect("/operation/message.do?msgCode=msg_nofile&backFlag=true");
        	}else{
        		response.sendRedirect("/common/message.do?msgCode=msg_nofile&backFlag=true"); 
        	}
		}
	}
	
}
