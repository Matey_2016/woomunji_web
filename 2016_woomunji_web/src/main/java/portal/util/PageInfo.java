package portal.util;

import java.util.ArrayList;
import java.util.List;

public class PageInfo {
	
	protected int recordCount;
	protected int totalPageNo;
	protected int pageNo;
	protected int commentPageNo;
	protected int pageSize;
	protected List pageList;
	protected boolean success;
	protected String message;
	
	public PageInfo() {
		pageList = new ArrayList();
		success = true;
	}

	public List getPageList() {
		return pageList;
	}

	public void setPageList(List pageList) {
		this.pageList = pageList;
	}

	public int getPageNo() {
		return pageNo != 0 ? pageNo : 1;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
		calculateTotalPage();
	}

	public int getCommentPageNo() {
		return commentPageNo != 0 ? commentPageNo : 1;
	}

	public void setCommentPageNo(int commentpageNo) {
		this.commentPageNo = commentpageNo;
		calculateTotalPage();
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(int recordCount) {
		this.recordCount = recordCount;
		calculateTotalPage();
	}

	private void calculateTotalPage() {
		int cnt = (recordCount > 0) ? (recordCount - 1) / pageSize + 1 : 0;
		totalPageNo = recordCount > 0 ? (recordCount - 1) / pageSize + 1 : 0;
	}

	public int getTotalPageNo() {
		return totalPageNo;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
