package portal.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;

import org.springframework.util.FileCopyUtils;

public class ExcelUtil {
	
	/**
	 * 브라우저 구분 얻기.
	 * 
	 * @param request
	 * @return ""
	 */
	public static String getBrowser(HttpServletRequest request) {
		String header = request.getHeader("User-Agent");
		if (header.indexOf("MSIE") > -1) {
			return "MSIE";
		} else if (header.indexOf("Chrome") > -1) {
			return "Chrome";
		} else if (header.indexOf("Opera") > -1) {
			return "Opera";
		}
		return "Firefox";
	}

	/**
	 * Disposition 지정하기.
	 * 
	 * @param filename
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public static void setDisposition(String filename, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		String browser = getBrowser(request);

		String dispositionPrefix = "attachment; filename=";
		String encodedFilename = null;

		if (browser.equals("MSIE")) {
			encodedFilename = URLEncoder.encode(filename, "UTF-8").replaceAll("\\+", "%20");
		} else if (browser.equals("Firefox")) {
			encodedFilename = "\""+ new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		} else if (browser.equals("Opera")) {
			encodedFilename = "\""+ new String(filename.getBytes("UTF-8"), "8859_1") + "\"";
		} else if (browser.equals("Chrome")) {
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < filename.length(); i++) {
				char c = filename.charAt(i);
				if (c > '~') {
					sb.append(URLEncoder.encode("" + c, "UTF-8"));
				} else {
					sb.append(c);
				}
			}
			encodedFilename = sb.toString();
		} else {
			// throw new RuntimeException("Not supported browser");
			throw new IOException("Not supported browser");
		}

		response.setHeader("Content-Disposition", dispositionPrefix
				+ encodedFilename);

		if ("Opera".equals(browser)) {
			response.setContentType("application/octet-stream;charset=UTF-8");
		}
	}  
	
	/**
	 * 엑셀파일 생성 및 다운로드
	 * @param list
	 * @param file_name
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public static void makeExcel(List<Object> list, String file_name, HttpServletRequest request, HttpServletResponse response) throws Exception {
		//엑셀템플릿 경로
		String _storePath 	= BaseUtil.getProp(request, "excelTemplet");
		String templateFile	= _storePath + File.separator + file_name+"_template.xls";
		String destFile		= _storePath + File.separator + file_name+".xls";

		
		Map<String, Object> beans = new HashMap<String, Object>();
		beans.put("result", list);

		XLSTransformer transformer = new XLSTransformer();
		transformer.transformXLS(templateFile, beans, destFile);
		
		String mimetype = "application/x-msdownload";
		
		response.setBufferSize((int) destFile.length());
		response.setContentType(mimetype);
		
		setDisposition(file_name+".xls", request, response);
		
		BufferedInputStream in = null;
		BufferedOutputStream out = null;
	
		
		try {
			
			in  = new BufferedInputStream(new FileInputStream(destFile));
			out = new BufferedOutputStream(response.getOutputStream());
			
			FileCopyUtils.copy(in, out);
			out.flush();
			
		} catch (ParsePropertyException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				try {
					in.close();
				} catch (Exception ignore) {
					ignore.printStackTrace();
				}
			}
			if (out != null) {
				try {
					out.close();
				} catch (Exception ignore) {
					ignore.printStackTrace();
				}
			}
		}
	}

}
