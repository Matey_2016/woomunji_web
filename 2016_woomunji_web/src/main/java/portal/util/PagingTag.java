package portal.util;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.tagext.TagSupport;

public class PagingTag extends TagSupport {

	private static final long serialVersionUID = -38723657405906100L;

	public final static int DEFAULT_GROUP_SIZE = 10;

	/** 현재 페이지. */
	private int pageNo;

	/** 마지막 페이지. */
	private int lastPageNo;

	/** 페이지 목록 갯수. */
	private int groupSize;

	/** 페이지 표시 Style Class명 접미어. */
	@SuppressWarnings("unused")
	private String stylePrefix;

	/** 패턴에서 페이지번호로 교체할 변수. */
	private String pageKey;

	/** 페이지 번호에 걸 링크 패턴. */
	private String linkPattern;

	/** 링크 페이지를 표시할 패턴. */
	private String linkedPageDisplayPattern;

	/** 현재 페이지를 표시할 패턴. */
	private String currentPageDisplayPattern;

	/** 첫번째 페이지로 이동 표시 패턴. */
	private String firstPageDisplayPattern;

	/** 마지막 페이지로 이동 표시 패턴. */
	private String lastPageDisplayPattern;

	/** 이전 그룹으로 이동 표시 패턴. */
	private String prevGroupDisplayPattern;

	/** 다음 그룹으로 이동 표시 패턴. */
	private String nextGroupDisplayPattern;


	public PagingTag() {
		this.groupSize = DEFAULT_GROUP_SIZE;

		this.stylePrefix = "";

		this.pageKey   = "#pageNo";

		this.linkedPageDisplayPattern  = "#pageNo";
		this.currentPageDisplayPattern = "#pageNo";
		this.firstPageDisplayPattern   = "맨앞 페이지";
		this.lastPageDisplayPattern    = "맨뒤 페이지";
		this.prevGroupDisplayPattern   = "이전 페이지";
		this.nextGroupDisplayPattern   = "다음 페이지";
	}


	/**
	 * @param currentPageDisplayPattern 설정하려는 currentPageDisplayPattern
	 */
	public void setCurrentPageDisplayPattern(String currentPageDisplayPattern) {
		this.currentPageDisplayPattern = currentPageDisplayPattern;
	}


	/**
	 * @param firstPageDisplayPattern 설정하려는 firstPageDisplayPattern
	 */
	public void setFirstPageDisplayPattern(String firstPageDisplayPattern) {
		this.firstPageDisplayPattern = firstPageDisplayPattern;
	}


	/**
	 * @param groupSize 설정하려는 groupSize
	 */
	public void setGroupSize(int groupSize) {
		this.groupSize = groupSize;
	}


	/**
	 * @param lastPageDisplayPattern 설정하려는 lastPageDisplayPattern
	 */
	public void setLastPageDisplayPattern(String lastPageDisplayPattern) {
		this.lastPageDisplayPattern = lastPageDisplayPattern;
	}


	/**
	 * @param lastPageNo 설정하려는 lastPageNo
	 */
	public void setLastPageNo(int lastPageNo) {
		this.lastPageNo = lastPageNo;
	}


	/**
	 * @param linkedPageDisplayPattern 설정하려는 linkedPageDisplayPattern
	 */
	public void setLinkedPageDisplayPattern(String linkedPageDisplayPattern) {
		this.linkedPageDisplayPattern = linkedPageDisplayPattern;
	}


	/**
	 * @param linkPattern 설정하려는 linkPattern
	 */
	public void setLinkPattern(String linkPattern) {
		this.linkPattern = linkPattern;
	}


	/**
	 * @param nextGroupDisplayPattern 설정하려는 nextGroupDisplayPattern
	 */
	public void setNextGroupDisplayPattern(String nextGroupDisplayPattern) {
		this.nextGroupDisplayPattern = nextGroupDisplayPattern;
	}


	/**
	 * @param pageKey 설정하려는 pageKey
	 */
	public void setPageKey(String pageKey) {
		this.pageKey = pageKey;
	}


	/**
	 * @param pageNo 설정하려는 pageNo
	 */
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}


	/**
	 * @param prevGroupDisplayPattern 설정하려는 prevGroupDisplayPattern
	 */
	public void setPrevGroupDisplayPattern(String prevGroupDisplayPattern) {
		this.prevGroupDisplayPattern = prevGroupDisplayPattern;
	}


	/**
	 * @param stylePrefix 설정하려는 stylePrefix
	 */
	public void setStylePrefix(String stylePrefix) {
		this.stylePrefix = stylePrefix;
	}
 
	/**
	 * 페이지 링크 HTML 생성하여 표시한다.
	 */
	public int doStartTag_OLD	() throws JspException {
		/* 페이지 링크 생성시 필요한 값들을 계산한다. */
		int lastPageLinkGroupIndex = (lastPageNo - 1) / groupSize;
		int currentPageLinkGroupIndex = (pageNo - 1) / groupSize;
		
		boolean hasFirstPageLink = currentPageLinkGroupIndex > 0;
		boolean hasLastPageLink = currentPageLinkGroupIndex < lastPageLinkGroupIndex;

		boolean hasPreviousPageLinkGroup = hasFirstPageLink;
		boolean hasNextPageLinkGroup = hasLastPageLink;

		int firstPageNoOfGroup = currentPageLinkGroupIndex * groupSize + 1;
		int lastPageNoOfGroup = (currentPageLinkGroupIndex + 1) * groupSize;
		
		/* 현재 링크 그룹의 마지막 페이지가 전체 마지막 페이지보다 클 수 없다. */
		if (lastPageNoOfGroup > lastPageNo) {
			lastPageNoOfGroup = lastPageNo;
		}

		StringBuffer buff = new StringBuffer();
		/* 첫 페이지로 이동 링크 생성. */
		if ( hasFirstPageLink ) {
			buff.append("<a class=\"btn first\" href=\"").append(parsePagePattern(this.linkPattern, 1)).append("\" ")
				.append("title=\"첫 페이지로 이동합니다.\">")
				.append("처음")
				//.append(1)
				.append("</a>\n");
		}

		/* 이전 페이지 그룹 링크 생성. */
		if ( hasPreviousPageLinkGroup ) {
			buff.append("<a class=\"btn prev\" href=\"").append(parsePagePattern(this.linkPattern, firstPageNoOfGroup - 1)).append("\" ")
				.append("title=\"").append(firstPageNoOfGroup - 1).append(" 페이지로 이동합니다.\">")
				.append("이전")
				//.append(firstPageNoOfGroup - 1)
				.append("</a>\n");
		}

		/* 현재 그룹 페이지 링크들 생성. */
		for (int i = firstPageNoOfGroup; i <= lastPageNoOfGroup; i++) {
			if (i == pageNo) {
				/* 현재 페이지일 때. */
				buff.append("<span ")
				.append("title=\"현재페이지\" class=\"pie\">")	
				.append("<strong>")
				.append(parsePagePattern(this.currentPageDisplayPattern, i))
				.append("</strong>")
				.append("</span>\n");
			} else if(i == lastPageNoOfGroup) {
				/* 마지막 페이지 일 때. */
				buff.append("<a href=\"").append(parsePagePattern(this.linkPattern, i)).append("\" ")
					.append("title=\"").append(i).append(" 페이지로 이동합니다.\">")
					.append(parsePagePattern(this.linkedPageDisplayPattern, i))
					.append("</a>\n");
			} else {
				/* 다른 페이지 일 때. */
				buff.append("<a href=\"").append(parsePagePattern(this.linkPattern, i)).append("\" ")
					.append("title=\"").append(i).append(" 페이지로 이동합니다.\">")
					.append(parsePagePattern(this.linkedPageDisplayPattern, i))
					.append("</a>\n");
			}
		}


		/* 다음 그룹 링크 생성. */
		if ( hasNextPageLinkGroup ) {
			buff.append("<a class=\"btn next\" href=\"").append(parsePagePattern(this.linkPattern, lastPageNoOfGroup + 1)).append("\" ")
				.append("title=\"").append(lastPageNoOfGroup + 1).append(" 페이지로 이동합니다.\">")
				.append("다음")
				//.append(lastPageNoOfGroup + 1)
				.append("</a>\n");
		}

		/* 마지막 페이지 링크 생성. */
		if ( hasLastPageLink ) {
			buff.append("<a class=\"btn last\" href=\"").append(parsePagePattern(this.linkPattern, this.lastPageNo)).append("\" ")
				.append("title=\"").append(this.lastPageNo).append(" 페이지로 이동합니다.\">")
				.append("끝")
				//.append(this.lastPageNo)
				.append("</a>\n");
		}
		
		/* 내용이 전혀 없다면 잘라야 할 Index를 0으로 한다. */
		int lastIndex = buff.length() > 0 ? buff.length() - 1 : 0;
		try {
			pageContext.getOut().print(buff.substring(0, lastIndex));
		}
		catch (IOException e) {
			throw new JspTagException("사용자에게 응답중 오류 발생: " + e, e);
		}
		finally {
			buff.delete(0, buff.length());
			buff = null;
		}

		return SKIP_BODY;
	}
	
	/**
	 * 페이지 링크 HTML 생성하여 표시한다.
	 */
	public int doStartTag() throws JspException {
		/* 페이지 링크 생성시 필요한 값들을 계산한다. */
		int lastPageLinkGroupIndex = (lastPageNo - 1) / groupSize;
		int currentPageLinkGroupIndex = (pageNo - 1) / groupSize;
		
		boolean hasFirstPageLink = currentPageLinkGroupIndex > 0;
		boolean hasLastPageLink = currentPageLinkGroupIndex < lastPageLinkGroupIndex;

		boolean hasPreviousPageLinkGroup = hasFirstPageLink;
		boolean hasNextPageLinkGroup = hasLastPageLink;

		int firstPageNoOfGroup = currentPageLinkGroupIndex * groupSize + 1;
		int lastPageNoOfGroup = (currentPageLinkGroupIndex + 1) * groupSize;
		
		/* 현재 링크 그룹의 마지막 페이지가 전체 마지막 페이지보다 클 수 없다. */
		if (lastPageNoOfGroup > lastPageNo) {
			lastPageNoOfGroup = lastPageNo;
		}

		StringBuffer buff = new StringBuffer();
		/* 첫 페이지로 이동 링크 생성. */
		buff.append("<div class='inBox'>");
		if ( hasFirstPageLink ) {
			buff.append("<a href=\"").append(parsePagePattern(this.linkPattern, 1)).append("\" ")
				.append("title=\"첫 페이지로 이동합니다.\" class=\"firstPage\">")
				.append("<span class=\"icoPaging1_1\">")
				.append(firstPageDisplayPattern)
				.append("</span>")
				.append("</a>");
		}

		
		
		
		
		/* 이전 페이지 그룹 링크 생성. */
		if ( hasPreviousPageLinkGroup ) {
			buff.append("<a href=\"").append(parsePagePattern(this.linkPattern, firstPageNoOfGroup - 1)).append("\" ")
				.append("title=\"").append(firstPageNoOfGroup - 1).append(" 페이지로 이동합니다.\" class=\"prevPage\">")
				.append("<span class=\"icoPaging1\">")
				.append(prevGroupDisplayPattern)
				.append("</span>")
				.append("</a>");
		}

		/* 현재 그룹 페이지 링크들 생성. */
		
		if ( lastPageNoOfGroup == 0 ) {
			buff.append("&nbsp;");   // 목록 결과값이 없을 때 page부분이 없어서 버튼의 위치가 위에 붙는 것을 방지하기 위한 목적
		}
		for (int i = firstPageNoOfGroup; i <= lastPageNoOfGroup; i++) {
			if (i == pageNo) {
				/* 현재 페이지일 때. */
				buff.append("<a href=\"#none\" ")
				.append("title=\"현재페이지\" class=\"on\">")	
				.append(parsePagePattern(this.currentPageDisplayPattern, i))
				.append("</a>");
			} else {
				/* 다른 페이지 일 때. */
				buff.append("<a href=\"").append(parsePagePattern(this.linkPattern, i)).append("\" ")
					.append("title=\"").append(i).append(" 페이지로 이동합니다.\">")
					.append(parsePagePattern(this.linkedPageDisplayPattern, i))
					.append("</a>\n");
			}
		}

		/* 다음 그룹 링크 생성. */
		if ( hasNextPageLinkGroup ) {
			buff.append("<a href=\"").append(parsePagePattern(this.linkPattern, lastPageNoOfGroup + 1)).append("\" ")
				.append("title=\"").append(lastPageNoOfGroup + 1).append(" 페이지로 이동합니다.\" class=\"nextPage\">")
				.append("<span class=\"icoPaging2\">")
				.append(nextGroupDisplayPattern)
				.append("</span>")
				.append("</a>");
		}

		/* 마지막 페이지 링크 생성. */
		if ( hasLastPageLink ) {
			buff.append("<a href=\"").append(parsePagePattern(this.linkPattern, this.lastPageNo)).append("\" ")
				.append("title=\"").append(this.lastPageNo).append(" 페이지로 이동합니다.\" class=\"lastPage\">")
				.append("<span class=\"icoPaging2_1\">")
				.append(lastPageDisplayPattern)
				.append("</span>")
				.append("</a>");
		}
		
		buff.append("</div>");
		/* 내용이 전혀 없다면 잘라야 할 Index를 0으로 한다. */
		int lastIndex = buff.length() > 0 ? buff.length(): 0;
		try {
			pageContext.getOut().print(buff.substring(0, lastIndex));
		}
		catch (IOException e) {
			throw new JspTagException("사용자에게 응답중 오류 발생: " + e, e);
		}
		finally {
			buff.delete(0, buff.length());
			buff = null;
		}

		return SKIP_BODY;
	}


	/**
	 * 주어진 문자열 내에 있는  this.pageKey 값 부분을 페이지 번호로 변경하여 돌려 준다.
	 *
	 * @param pattern
	 * @param pageNo
	 * @return
	 */
	private String parsePagePattern(String pattern, int pageNo) {
		String pat = pattern.replaceAll(this.pageKey, String.valueOf(pageNo));
		return pat;
	}
}
