package portal.util;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import portal.util.BaseUtil;

public class HtmlStringTag extends TagSupport {
	private static final long serialVersionUID = -5278163630229232491L;
	
	private String 	value;
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	@Override
	public int doStartTag() throws JspException {
		JspWriter out = pageContext.getOut();
		value = BaseUtil.convertHTML(value, "");
		try {
			out.print(value); 
			return SKIP_BODY; 
		} catch(Exception e) {
			throw new JspException("Error: "+e.getMessage());
		}
		
	}
	
	
}