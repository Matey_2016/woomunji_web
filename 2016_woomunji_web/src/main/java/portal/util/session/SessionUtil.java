package portal.util.session;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Repository;

@Repository("baseUtil")
public class SessionUtil {
	
	protected final Log log = LogFactory.getLog(getClass());
		
		
	/**
	 * 세션을 DTO단위로 생성함	
	 * @param request
	 * @param sessionName	세션명
	 * @param object		세션에 담을 DTO
	 */
	public static void setSession(	HttpServletRequest request,
										String sessionName,
										Object object){
		
		if (object != null){
			HttpSession session = request.getSession();		
			session = request.getSession(true);
			session.setAttribute(sessionName, object); 
		}
	}
	
	
	/**
	 * 세션을 DTO단위로 반환함
	 * @param request
	 * @param sessionName	세션명
	 * @param object		세션을 담을 DTO
	 * @return
	 */
	public static Object getSession(	HttpServletRequest request,
										String sessionName,
										Object object){

		HttpSession session = request.getSession();		
		session = request.getSession(true);
		object	= session.getAttribute(sessionName); 
		
		return object;
	}
	
	
	/**
	 * 세션 초기화
	 * @param request
	 * @param sessionName	세션명
	 * @param object		세션을 담을 DTO
	 * @return
	 */
	public static void clearSession(	HttpServletRequest request,
										String sessionName,
										Object object){

		HttpSession session = request.getSession();		
		session = request.getSession(true);
		session.setAttribute(sessionName, null); 
	}
	
}
