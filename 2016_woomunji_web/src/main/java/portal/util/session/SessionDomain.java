package portal.util.session;

public class SessionDomain {
	private String language;				/* 기본언어 (다국어 지원 관련 변수) */
	private String nextUrl;					/* 처리 후 이동 페이지 */
	
	private int admnAuth;
	private String admnIp;
	private String admnNm;
	private String admnDetp;
	private String regDt;
	private String admnId;
	private String admnLvl;
	private String admnLvlNm;
	private String[] admnMenu;
	private String admnGnb; 
	
	private String nm;
	private String email;

	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public String getNextUrl() {
		return nextUrl;
	}
	public void setNextUrl(String nextUrl) {
		this.nextUrl = nextUrl;
	}
	public int getAdmnAuth() {
		return admnAuth;
	}
	public void setAdmnAuth(int admnAuth) {
		this.admnAuth = admnAuth;
	}
	public String getAdmnIp() {
		return admnIp;
	}
	public void setAdmnIp(String admnIp) {
		this.admnIp = admnIp;
	}
	public String getAdmnNm() {
		return admnNm;
	}
	public void setAdmnNm(String admnNm) {
		this.admnNm = admnNm;
	}
	public String getAdmnDetp() {
		return admnDetp;
	}
	public void setAdmnDetp(String admnDetp) {
		this.admnDetp = admnDetp;
	}
	public String getRegDt() {
		return regDt;
	}
	public void setRegDt(String regDt) {
		this.regDt = regDt;
	}
	public String getAdmnId() {
		return admnId;
	}
	public void setAdmnId(String admnId) {
		this.admnId = admnId;
	}
	public String getAdmnLvl() {
		return admnLvl;
	}
	public void setAdmnLvl(String admnLvl) {
		this.admnLvl = admnLvl;
	}
	public String[] getAdmnMenu() {
		return admnMenu;
	}
	public String getAdmnLvlNm() {
		return admnLvlNm;
	}
	public void setAdmnLvlNm(String admnLvlNm) {
		this.admnLvlNm = admnLvlNm;
	}
	public void setAdmnMenu(String[] admnMenu) {
		this.admnMenu = admnMenu;
	}
	public String getAdmnGnb() {
		return admnGnb;
	}
	public void setAdmnGnb(String admnGnb) {
		this.admnGnb = admnGnb;
	}
	public String getNm() {
		return nm;
	}
	public void setNm(String nm) {
		this.nm = nm;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
