package portal.util;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.web.context.support.WebApplicationContextUtils;

import portal.util.BaseUtil;

@SuppressWarnings("serial")
public class ConvertHTMLTag extends TagSupport {

	private String value;
	private String COMPARE_HTML_REGEX;
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	

	private void setCompareHtmlRegex() {
		MessageSourceAccessor messageSource = (MessageSourceAccessor)WebApplicationContextUtils
																							.getWebApplicationContext(pageContext.getServletContext())
																							.getBean("commMessageSource");
		COMPARE_HTML_REGEX = messageSource.getMessage("compare_html_regex");
	}

	
	private boolean hasHTMLTag (String value) {
		if (COMPARE_HTML_REGEX == null) {
			setCompareHtmlRegex();
		}
		if (value != null && value.contains("<")) {
			return value.matches(COMPARE_HTML_REGEX);
		}
		return false;
	}

	@Override
	public int doStartTag() throws JspException {
		JspWriter out = pageContext.getOut();
		try {
			if (!hasHTMLTag(value)) {
				value = BaseUtil.convertStringToHTMLTag(value);
			}
			out.print(value); return SKIP_BODY;
		} catch (Exception e) {
			throw new JspException(e.getMessage());
		}
	}

}
