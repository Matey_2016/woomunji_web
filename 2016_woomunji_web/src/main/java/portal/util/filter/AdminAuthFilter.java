package portal.util.filter;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.ui.ModelMap;

import portal.dto.StaffDTO;

public class AdminAuthFilter implements Filter {

	public void destroy() {}

	public void init(FilterConfig arg0) throws ServletException {}

	@SuppressWarnings("rawtypes")
	public void doFilter(	ServletRequest 	request, 
							ServletResponse response,
							FilterChain 	chain) throws IOException, ServletException {
		
		HttpSession session = ((HttpServletRequest)request).getSession();
		StaffDTO avo = (StaffDTO)session.getAttribute("avo");
		
		//LNB 처리를 위한 메뉴 아이디 관리
		Enumeration paramNames = ((HttpServletRequest)request).getParameterNames();
		while (paramNames.hasMoreElements()) {
			String k = (String) paramNames.nextElement();
			String v = java.net.URLEncoder.encode(request.getParameter(k), "UTF-8");
			
			if (k.equals("lnb_id") && !v.equals("")){
				session.setAttribute("lnb_id", v);
				break;
			}
		}
		
		//모든 컨트롤러에서 공통으로 사용하는 baseURI셋팅
		String uri = ((HttpServletRequest)request).getRequestURI().replaceAll(".do", "");
		request.setAttribute("baseURI", uri);
		request.setAttribute("isOperation", true);
		
		
		if (!uri.startsWith("/operation/login")) {
			if (avo == null) {	// 로그인안된회원처리
				((HttpServletResponse)response).sendRedirect("/operation/login.do");
				return;
			} 
		}
		
		chain.doFilter(request, response);
		
	}
}
