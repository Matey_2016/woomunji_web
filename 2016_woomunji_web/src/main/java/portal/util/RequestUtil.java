package portal.util;

import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/** Request에 관련된 정보, Controller.Method-signature에 HttpServletRequest가 없어도 본 유틸을 이용해서 request정보를 알아낼 수 있다.
 * @author scaler
 *
 */
public class RequestUtil {
	private static final String[] internalIps={"172.20", "172.29", "172.30"};

	/**
	 * 내부적으로 ThreadLocal을 사용하여 다수의 동시 Request에 대해 안전(Thread-safe)
	 * @return
	 */
	public static HttpServletRequest getRequest() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

		return request;
	}
	/**
	 * @return 현재페이지의 도메인과 파일명 제외 경로
	 */
	public static String baseURI() {
		String uri = getRequest().getRequestURI().replaceAll("[^\\/]+\\.do$", "");
		return uri;
	}
	/**
	 * @return 현재페이지의 도메인과 확장자 제외 경로
	 */
	public static String forwardUrl() {
		String uri = getRequest().getRequestURI().replaceAll("\\.do$", "");
		return uri;
	}
	/** equal to forwardUrl()
	 * @return
	 */
	public static String jsp() {
		return forwardUrl();
	}
	
	/**
	 * 아이피 체크
	 * @return
	 */
	public static boolean isInternalRequest() {
		HttpServletRequest request=getRequest();
		String ip=request.getRemoteAddr();
		boolean result=false;
		for (String ipPre : internalIps) {
			if (ip.startsWith(ipPre)) {
				result=true;
				break;
			}
		}

		return result;
	}
	
	
}