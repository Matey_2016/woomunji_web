package portal.dto;

			 
@SuppressWarnings("serial")
public class UserDTO extends BaseDTO{
	
	private String user_id;
	private String user_nm;
	private String passwd;
	private String current_passwd;
	private String email;
	private String email_id;
	private String email_domain;
	private String phone;
	private String phone1;
	private String phone2;
	private String phone3;
	private String mobile;
	private String mobile1;
	private String mobile2;
	private String mobile3;
	private String email_yn;
	private String sms_yn;
	private String addr_desc;
	private String login_dt;
	private String leave_dt;
	private String leave_confirm_dt;
	private String del_yn;
	private String reg_dt;
	private String ip;
	private String interest_cd;
	private String interest_nm;
	private String birth;
	private String dormancy_yn;
	
	
	public String getDormancy_yn() {
		return dormancy_yn;
	}
	public void setDormancy_yn(String dormancy_yn) {
		this.dormancy_yn = dormancy_yn;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getUser_nm() {
		return user_nm;
	}
	public void setUser_nm(String user_nm) {
		this.user_nm = user_nm;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getCurrent_passwd() {
		return current_passwd;
	}
	public void setCurrent_passwd(String current_passwd) {
		this.current_passwd = current_passwd;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail_id() {
		return email_id;
	}
	public void setEmail_id(String email_id) {
		this.email_id = email_id;
	}
	public String getEmail_domain() {
		return email_domain;
	}
	public void setEmail_domain(String email_domain) {
		this.email_domain = email_domain;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getPhone1() {
		return phone1;
	}
	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}
	public String getPhone2() {
		return phone2;
	}
	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}
	public String getPhone3() {
		return phone3;
	}
	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getMobile1() {
		return mobile1;
	}
	public void setMobile1(String mobile1) {
		this.mobile1 = mobile1;
	}
	public String getMobile2() {
		return mobile2;
	}
	public void setMobile2(String mobile2) {
		this.mobile2 = mobile2;
	}
	public String getMobile3() {
		return mobile3;
	}
	public void setMobile3(String mobile3) {
		this.mobile3 = mobile3;
	}
	public String getEmail_yn() {
		return email_yn;
	}
	public void setEmail_yn(String email_yn) {
		this.email_yn = email_yn;
	}
	public String getSms_yn() {
		return sms_yn;
	}
	public void setSms_yn(String sms_yn) {
		this.sms_yn = sms_yn;
	}
	public String getAddr_desc() {
		return addr_desc;
	}
	public void setAddr_desc(String addr_desc) {
		this.addr_desc = addr_desc;
	}
	public String getLogin_dt() {
		return login_dt;
	}
	public void setLogin_dt(String login_dt) {
		this.login_dt = login_dt;
	}
	public String getLeave_dt() {
		return leave_dt;
	}
	public void setLeave_dt(String leave_dt) {
		this.leave_dt = leave_dt;
	}
	public String getLeave_confirm_dt() {
		return leave_confirm_dt;
	}
	public void setLeave_confirm_dt(String leave_confirm_dt) {
		this.leave_confirm_dt = leave_confirm_dt;
	}
	public String getDel_yn() {
		return del_yn;
	}
	public void setDel_yn(String del_yn) {
		this.del_yn = del_yn;
	}
	public String getReg_dt() {
		return reg_dt;
	}
	public void setReg_dt(String reg_dt) {
		this.reg_dt = reg_dt;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getInterest_cd() {
		return interest_cd;
	}
	public void setInterest_cd(String interest_cd) {
		this.interest_cd = interest_cd;
	}
	public String getInterest_nm() {
		return interest_nm;
	}
	public void setInterest_nm(String interest_nm) {
		this.interest_nm = interest_nm;
	}
	public String getBirth() {
		return birth;
	}
	public void setBirth(String birth) {
		this.birth = birth;
	}
	
}
