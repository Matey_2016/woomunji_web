package portal.dto;

import portal.dto.BaseDTO;
			 
@SuppressWarnings("serial")
public class BoardDTO extends BaseDTO{
	private int boardId;    		//게시판 아이디     
	private int board_id;    		//게시판 아이디     
	private String board_nm;    	//게시판 명         
	private String type_cd;     	//게시판유형 코드   
	private String file_yn;     	//첨부파일 사용여부 
	private int file_cnt;     		//첨부파일 사용여부 
	private String reply_yn;    	//답변 사용여부     
	private String notice_yn;   	//공지사용 여부     
	private String comment_yn;  	//댓글 사용여부    
	private String summary_yn;  	//요약글 사용여부    
	private String pass_yn;			//비밀글여부
	private String thumb_yn;		//썸네일 기능 사용여부
	private String exLink_yn;		//외부링크 사용여부
	private String urlDeny_yn;		//url조작 금지
	private String use_yn;      	//사용여부          
	private String reg_dt;      	//등록일 
	private String language;		//사용언어
	private String use_code1;		//공통코드사용여부
	private String use_code2;		//공통코드사용여부
	private String use_code1_nm;	//공통코드사용여부
	private String use_code2_nm;	//공통코드사용여부
	private String code_relation;	//코드 연관 여부
	private int	new_day;			//새글표시 사용여부
	private String preNext_yn;		//이전/다음글 사용여부
	private String movie_yn;		//동영상 사용여부
	private String del_yn;			//삭제여부
	private int page_size;			//목록갯수
	private String staff_id	;
	private String email;
	private int postCnt;
	
	public BoardDTO() {
		file_cnt 		= 1;
		new_day	 		= 1;
		page_size		= 10; 
		file_yn			= "";     	//첨부파일 사용여부 
		reply_yn    	= "";
		notice_yn   	= "";
		comment_yn  	= "";
		summary_yn  	= "";
		pass_yn			= "";
		thumb_yn		= "";
		exLink_yn		= "";
		urlDeny_yn		= "";
		use_yn      	= ""; 
		language		= "";
		use_code1		= "";
		use_code2		= "";
		use_code1_nm	= "";
		use_code2_nm	= "";
		code_relation	= "";
		preNext_yn		= "";
		movie_yn		= "";
	}

	public int getBoardId() {
		return boardId;
	}

	public void setBoardId(int boardId) {
		this.boardId = boardId;
	}

	public int getBoard_id() {
		return board_id;
	}

	public void setBoard_id(int board_id) {
		this.board_id = board_id;
	}

	public String getBoard_nm() {
		return board_nm;
	}

	public void setBoard_nm(String board_nm) {
		this.board_nm = board_nm;
	}

	public String getType_cd() {
		return type_cd;
	}

	public void setType_cd(String type_cd) {
		this.type_cd = type_cd;
	}

	public String getFile_yn() {
		return file_yn;
	}

	public void setFile_yn(String file_yn) {
		this.file_yn = file_yn;
	}

	public int getFile_cnt() {
		return file_cnt;
	}

	public void setFile_cnt(int file_cnt) {
		this.file_cnt = file_cnt;
	}

	public String getReply_yn() {
		return reply_yn;
	}

	public void setReply_yn(String reply_yn) {
		this.reply_yn = reply_yn;
	}

	public String getNotice_yn() {
		return notice_yn;
	}

	public void setNotice_yn(String notice_yn) {
		this.notice_yn = notice_yn;
	}

	public String getComment_yn() {
		return comment_yn;
	}

	public void setComment_yn(String comment_yn) {
		this.comment_yn = comment_yn;
	}

	public String getSummary_yn() {
		return summary_yn;
	}

	public void setSummary_yn(String summary_yn) {
		this.summary_yn = summary_yn;
	}

	public String getPass_yn() {
		return pass_yn;
	}

	public void setPass_yn(String pass_yn) {
		this.pass_yn = pass_yn;
	}

	public String getThumb_yn() {
		return thumb_yn;
	}

	public void setThumb_yn(String thumb_yn) {
		this.thumb_yn = thumb_yn;
	}

	public String getExLink_yn() {
		return exLink_yn;
	}

	public void setExLink_yn(String exLink_yn) {
		this.exLink_yn = exLink_yn;
	}

	public String getUrlDeny_yn() {
		return urlDeny_yn;
	}

	public void setUrlDeny_yn(String urlDeny_yn) {
		this.urlDeny_yn = urlDeny_yn;
	}

	public String getUse_yn() {
		return use_yn;
	}

	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}

	public String getReg_dt() {
		return reg_dt;
	}

	public void setReg_dt(String reg_dt) {
		this.reg_dt = reg_dt;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getUse_code1() {
		return use_code1;
	}

	public void setUse_code1(String use_code1) {
		this.use_code1 = use_code1;
	}

	public String getUse_code2() {
		return use_code2;
	}

	public void setUse_code2(String use_code2) {
		this.use_code2 = use_code2;
	}

	public String getUse_code1_nm() {
		return use_code1_nm;
	}

	public void setUse_code1_nm(String use_code1_nm) {
		this.use_code1_nm = use_code1_nm;
	}

	public String getUse_code2_nm() {
		return use_code2_nm;
	}

	public void setUse_code2_nm(String use_code2_nm) {
		this.use_code2_nm = use_code2_nm;
	}

	public String getCode_relation() {
		return code_relation;
	}

	public void setCode_relation(String code_relation) {
		this.code_relation = code_relation;
	}

	public int getNew_day() {
		return new_day;
	}

	public void setNew_day(int new_day) {
		this.new_day = new_day;
	}

	public String getPreNext_yn() {
		return preNext_yn;
	}

	public void setPreNext_yn(String preNext_yn) {
		this.preNext_yn = preNext_yn;
	}

	public String getMovie_yn() {
		return movie_yn;
	}

	public void setMovie_yn(String movie_yn) {
		this.movie_yn = movie_yn;
	}

	public String getDel_yn() {
		return del_yn;
	}

	public void setDel_yn(String del_yn) {
		this.del_yn = del_yn;
	}

	public int getPage_size() {
		return page_size;
	}

	public void setPage_size(int page_size) {
		this.page_size = page_size;
	}

	public String getStaff_id() {
		return staff_id;
	}

	public void setStaff_id(String staff_id) {
		this.staff_id = staff_id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getPostCnt() {
		return postCnt;
	}

	public void setPostCnt(int postCnt) {
		this.postCnt = postCnt;
	}
	
	
	
	
}
