package portal.dto;

			 
@SuppressWarnings("serial")
public class PostDTO extends BoardDTO{
	
	private String post_id;
	private String title;
	private String user_id;
	private String user_nm;
	private String passwd;
	private String email;
	private String etc_cd1;
	private String etc_nm1;
	private String etc_cd2;
	private String etc_nm2;
	private String contents;
	private String sumry;
	private int hit;
	private String parent_post_id;
	private String file_id;
	private String file_nm;
	private String file_path;
	private String system_file_nm;
	private String comment_id;
	private String comment;
	private String gubun;
	private String mode;
	private int fileCnt;
	private int fileLimit;
	private int new_interval;
	private int commentsCnt;
	private String movie_url;
	private String movie_file;
	private String link_yn;
	private String link;
	private String tag;
	private String category;
	private String s_gubun;
	private String s_type;
	private String board_nm;
	private String org_title;
	private String org_contents;
	private String s_sort;
	private String preNext;
	
	
	public String getPasswd() {
		return passwd;
	}


	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}


	public String getPreNext() {
		return preNext;
	}


	public void setPreNext(String preNext) {
		this.preNext = preNext;
	}


	public String getS_sort() {
		return s_sort;
	}


	public void setS_sort(String s_sort) {
		this.s_sort = s_sort;
	}


	public PostDTO() {
		gubun = "List";
		mode  = "C";
		fileCnt = 0;
		fileLimit = 5;
	}


	public String getEtc_nm1() {
		return etc_nm1;
	}


	public void setEtc_nm1(String etc_nm1) {
		this.etc_nm1 = etc_nm1;
	}


	public String getEtc_nm2() {
		return etc_nm2;
	}


	public void setEtc_nm2(String etc_nm2) {
		this.etc_nm2 = etc_nm2;
	}


	public String getPost_id() {
		return post_id;
	}


	public void setPost_id(String post_id) {
		this.post_id = post_id;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getUser_id() {
		return user_id;
	}


	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}


	public String getUser_nm() {
		return user_nm;
	}


	public void setUser_nm(String user_nm) {
		this.user_nm = user_nm;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getEtc_cd1() {
		return etc_cd1;
	}


	public void setEtc_cd1(String etc_cd1) {
		this.etc_cd1 = etc_cd1;
	}


	public String getEtc_cd2() {
		return etc_cd2;
	}


	public void setEtc_cd2(String etc_cd2) {
		this.etc_cd2 = etc_cd2;
	}


	public String getContents() {
		return contents;
	}


	public void setContents(String contents) {
		this.contents = contents;
	}


	public String getSumry() {
		return sumry;
	}


	public void setSumry(String sumry) {
		this.sumry = sumry;
	}


	public int getHit() {
		return hit;
	}


	public void setHit(int hit) {
		this.hit = hit;
	}


	public String getParent_post_id() {
		return parent_post_id;
	}


	public void setParent_post_id(String parent_post_id) {
		this.parent_post_id = parent_post_id;
	}


	public String getFile_id() {
		return file_id;
	}


	public void setFile_id(String file_id) {
		this.file_id = file_id;
	}


	public String getFile_nm() {
		return file_nm;
	}


	public void setFile_nm(String file_nm) {
		this.file_nm = file_nm;
	}


	public String getFile_path() {
		return file_path;
	}


	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}


	public String getSystem_file_nm() {
		return system_file_nm;
	}


	public void setSystem_file_nm(String system_file_nm) {
		this.system_file_nm = system_file_nm;
	}


	public String getComment_id() {
		return comment_id;
	}


	public void setComment_id(String comment_id) {
		this.comment_id = comment_id;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}


	public String getGubun() {
		return gubun;
	}


	public void setGubun(String gubun) {
		this.gubun = gubun;
	}


	public String getMode() {
		return mode;
	}


	public void setMode(String mode) {
		this.mode = mode;
	}


	public int getFileCnt() {
		return fileCnt;
	}


	public void setFileCnt(int fileCnt) {
		this.fileCnt = fileCnt;
	}


	public int getFileLimit() {
		return fileLimit;
	}


	public void setFileLimit(int fileLimit) {
		this.fileLimit = fileLimit;
	}


	public int getNew_interval() {
		return new_interval;
	}


	public void setNew_interval(int new_interval) {
		this.new_interval = new_interval;
	}


	public int getCommentsCnt() {
		return commentsCnt;
	}


	public void setCommentsCnt(int commentsCnt) {
		this.commentsCnt = commentsCnt;
	}


	public String getMovie_url() {
		return movie_url;
	}


	public void setMovie_url(String movie_url) {
		this.movie_url = movie_url;
	}


	public String getMovie_file() {
		return movie_file;
	}


	public void setMovie_file(String movie_file) {
		this.movie_file = movie_file;
	}


	public String getLink_yn() {
		return link_yn;
	}


	public void setLink_yn(String link_yn) {
		this.link_yn = link_yn;
	}


	public String getLink() {
		return link;
	}


	public void setLink(String link) {
		this.link = link;
	}


	public String getTag() {
		return tag;
	}


	public void setTag(String tag) {
		this.tag = tag;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getS_gubun() {
		return s_gubun;
	}


	public void setS_gubun(String s_gubun) {
		this.s_gubun = s_gubun;
	}


	public String getS_type() {
		return s_type;
	}


	public void setS_type(String s_type) {
		this.s_type = s_type;
	}


	public String getBoard_nm() {
		return board_nm;
	}


	public void setBoard_nm(String board_nm) {
		this.board_nm = board_nm;
	}


	public String getOrg_title() {
		return org_title;
	}


	public void setOrg_title(String org_title) {
		this.org_title = org_title;
	}


	public String getOrg_contents() {
		return org_contents;
	}


	public void setOrg_contents(String org_contents) {
		this.org_contents = org_contents;
	}
	
}
