package portal.dto;

import java.util.List;

/** 게시판 데이터 조회를 위한 최상위 VO 
 * 
 * @author	정재욱
 * @since	2014.05.20
 *
 * <pre>
 * << 개정이력 >>
 * 수정자      수정일          수정내용
 * ----------   ---------------   --------------------
 * 정재욱      2014. 8. 5.     최초생성
 * 
 * </pre>
 */
public class ListDTO {
	private long recordCount;
	private long totalPageNo;
	private int pageNo;
	private int pageSize;
	@SuppressWarnings("rawtypes")
	private List pageList;
	
	public ListDTO() {
		this.pageSize = 10;
	}
	
	public long getRecordCount() {
		return recordCount;
	}
	public void setRecordCount(long recordCount) {
		this.recordCount = recordCount;
		this.calculateTotalPage();
	}
	public long getTotalPageNo() {
		return totalPageNo;
	}
	public void setTotalPageNo(long totalPageNo) {
		this.totalPageNo = totalPageNo;
	}
	public int getPageNo() {
		return (pageNo != 0)? pageNo : 1;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	@SuppressWarnings("rawtypes")
	public List getPageList() {
		return pageList;
	}
	@SuppressWarnings("rawtypes")
	public void setPageList(List pageList) {
		this.pageList = pageList;
	}
	private void calculateTotalPage() {
		totalPageNo = (recordCount > 0)? (recordCount - 1) / pageSize+1 : 0; 
	}
}
