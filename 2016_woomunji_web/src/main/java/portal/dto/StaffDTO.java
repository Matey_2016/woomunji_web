package portal.dto;

/**
 * 관리자(운영자) 정보
 * @author jongsun
 *
 */

@SuppressWarnings("serial")
public class StaffDTO extends BaseDTO{
	private String staff_id;				// 관리자 ID
	private String staff_nm;				// 관리자 명
	private String passwd;					// 비밀번호
	private String dept_cd;             	// 소속부서코드
	private String dept_nm;					// 소속부서명
	private String use_yn;					// 계정 사용여부
	private String use_yn_nm;				// 계정 사용여부
	private String email;					// 이메일
	private String staff_lvl;				// 관리자 등급
	private String staff_lvl_nm;			// 관리자 등급
	private String phone;					// 연락처
	private String mobile;					// 휴대전화
	
	private String menu_id;					// 메뉴 ID		
	private String menu_nm;					// 메뉴 명
	private String menu_chk;				// 메뉴 허용여부
	private String auth;					// 부여권한 (CRUD)
	private String cChk;					// 등록 권한 ( Y / N )
	private String rChk;					// 읽기 권한 ( Y / N )
	private String uChk;					// 수정 권한 ( Y / N )
	private String dChk;					// 삭제 권한 ( Y / N )
	
	private String parent_cd;				// 상위 메뉴 코드
	private String parent_cd_nm;			// 상위 메뉴 명
	
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	public String getStaff_id() {
		return staff_id;
	}
	public void setStaff_id(String staff_id) {
		this.staff_id = staff_id;
	}
	public String getStaff_nm() {
		return staff_nm;
	}
	public void setStaff_nm(String staff_nm) {
		this.staff_nm = staff_nm;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getDept_cd() {
		return dept_cd;
	}
	public void setDept_cd(String dept_cd) {
		this.dept_cd = dept_cd;
	}
	public String getDept_nm() {
		return dept_nm;
	}
	public void setDept_nm(String dept_nm) {
		this.dept_nm = dept_nm;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public String getUse_yn_nm() {
		return use_yn_nm;
	}
	public void setUse_yn_nm(String use_yn_nm) {
		this.use_yn_nm = use_yn_nm;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getStaff_lvl() {
		return staff_lvl;
	}
	public void setStaff_lvl(String staff_lvl) {
		this.staff_lvl = staff_lvl;
	}
	public String getStaff_lvl_nm() {
		return staff_lvl_nm;
	}
	public void setStaff_lvl_nm(String staff_lvl_nm) {
		this.staff_lvl_nm = staff_lvl_nm;
	}
	public String getMenu_id() {
		return menu_id;
	}
	public void setMenu_id(String menu_id) {
		this.menu_id = menu_id;
	}
	public String getMenu_nm() {
		return menu_nm;
	}
	public void setMenu_nm(String menu_nm) {
		this.menu_nm = menu_nm;
	}
	public String getMenu_chk() {
		return menu_chk;
	}
	public void setMenu_chk(String menu_chk) {
		this.menu_chk = menu_chk;
	}
	public String getAuth() {
		return auth;
	}
	public void setAuth(String auth) {
		this.auth = auth;
	}
	public String getcChk() {
		return cChk;
	}
	public void setcChk(String cChk) {
		this.cChk = cChk;
	}
	public String getrChk() {
		return rChk;
	}
	public void setrChk(String rChk) {
		this.rChk = rChk;
	}
	public String getuChk() {
		return uChk;
	}
	public void setuChk(String uChk) {
		this.uChk = uChk;
	}
	public String getdChk() {
		return dChk;
	}
	public void setdChk(String dChk) {
		this.dChk = dChk;
	}
	public String getParent_cd() {
		return parent_cd;
	}
	public void setParent_cd(String parent_cd) {
		this.parent_cd = parent_cd;
	}
	public String getParent_cd_nm() {
		return parent_cd_nm;
	}
	public void setParent_cd_nm(String parent_cd_nm) {
		this.parent_cd_nm = parent_cd_nm;
	}
}
