package portal.dto;

import portal.dto.BaseDTO;

@SuppressWarnings("serial")
public class ConcertDTO extends BaseDTO{
	
	private String seq;            //일련번호        
	private String title;          //제목          
	private String start_date;     //시작일         
	private String end_date;       //마감일         
	private String place;          //장소          
	private String realm_name;     //분류명         
	private String area;           //지역          
	private String thumbnail;      //썸네일         
	private String subtitle;       //공연부제목       
	private String price;          //티켓요금        
	private String contents1;      //내용1         
	private String contents2;      //내용2         
	private String url;            //관람URL       
	private String phone;          //문의처         
	private String gpsx;           //GPS-X좌표     
	private String gpsy;           //GPS-Y좌표     
	private String img_url;        //이미지         
	private String place_url;      //공연장URL      
	private String place_addr;     //공연장 주소      
	private String place_seq;      //문화예술공간 일련번호 
	private String view_cnt;       //조회수         
	private String reg_date;       //등록시간        
	private String upt_date;       //수정시간      
	private String post_yn;        //수정시간
	
	
	public String getPost_yn() {
		return post_yn;
	}
	public void setPost_yn(String post_yn) {
		this.post_yn = post_yn;
	}
	public String getSeq() {
		return seq;
	}
	public void setSeq(String seq) {
		this.seq = seq;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}
	public String getRealm_name() {
		return realm_name;
	}
	public void setRealm_name(String realm_name) {
		this.realm_name = realm_name;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getContents1() {
		return contents1;
	}
	public void setContents1(String contents1) {
		this.contents1 = contents1;
	}
	public String getContents2() {
		return contents2;
	}
	public void setContents2(String contents2) {
		this.contents2 = contents2;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getGpsx() {
		return gpsx;
	}
	public void setGpsx(String gpsx) {
		this.gpsx = gpsx;
	}
	public String getGpsy() {
		return gpsy;
	}
	public void setGpsy(String gpsy) {
		this.gpsy = gpsy;
	}
	public String getImg_url() {
		return img_url;
	}
	public void setImg_url(String img_url) {
		this.img_url = img_url;
	}
	public String getPlace_url() {
		return place_url;
	}
	public void setPlace_url(String place_url) {
		this.place_url = place_url;
	}
	public String getPlace_addr() {
		return place_addr;
	}
	public void setPlace_addr(String place_addr) {
		this.place_addr = place_addr;
	}
	public String getPlace_seq() {
		return place_seq;
	}
	public void setPlace_seq(String place_seq) {
		this.place_seq = place_seq;
	}
	public String getView_cnt() {
		return view_cnt;
	}
	public void setView_cnt(String view_cnt) {
		this.view_cnt = view_cnt;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public String getUpt_date() {
		return upt_date;
	}
	public void setUpt_date(String upt_date) {
		this.upt_date = upt_date;
	}
	
	
	
}
