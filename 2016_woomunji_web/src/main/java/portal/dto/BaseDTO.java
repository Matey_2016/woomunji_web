package portal.dto;

import java.io.Serializable;

public abstract class BaseDTO implements Serializable {
	
	static final long serialVersionUID = 7295040260084560120L;
	
	//검색 (아래 변수는 사용자제할것)
	protected String queryStr0;					// 카테고리
	protected String queryStr1;					// 구분
	protected String queryStr2;					// 검색어
	protected String queryStr3;					// 기타
	
	
	
	protected String s_type;					// 검색구분(selectbox)
	protected String s_text;					// 검색어
	protected String date_format;				// 날짜형식	
	
	
	//정렬
	protected String orderBy1;
	protected String orderBy2;
	
	//날짜 검색
	protected String searchYear;
	protected String searchMonth;
	protected String searchDay;
	protected String searchDate;
	
	//게시판 페이징 관련
	protected int total;
	protected int start;
	protected int end;
	protected int pageSize;
	protected int pageTotal;
	protected int cpage;
	protected int rowNum;
	protected int pageGrpSize;
	protected String page;
	protected String pg;
	protected String startDate;
	protected String endDate;
	
	protected String mode;
	protected String operation_yn;
	
	protected String reg_dt;
	protected String reg_id;
	protected String mod_dt;
	protected String mod_id;
	protected String mod_flag;
	protected String excelName;
	
	private String allwId;				// 페이지 접근자 ID
	private String pageId;				// 페이지ID
	private String pageAllw;			// 페이지 접근여부( Y/N )
	private String pageAuth;			// 페이지별 권한( CRUD )	
	
	private String sqlNameSpace;		// 각 DAO별 NameSpace정보 ( 첨부파일 정보 관련 사용 )
	private String baseURI;				
	private String remote_ip;		
	
	public BaseDTO() {
		queryStr0			= "";
		queryStr1			= "";
		queryStr2			= "";
		queryStr3			= "";
		s_type				= "";
		s_text				= "";
		orderBy1			= "";
		orderBy2			= "";
		searchYear			= "";
		searchMonth			= "";
		searchDay			= "";
		total				= 0;
		start				= 0;
		pageSize			= 10;
		end					= pageSize;
		pageTotal			= 0;
		cpage				= 1;
		rowNum				= 0;
		pageGrpSize			= 10;
		pg					= "";
		startDate			= "";
		endDate				= "";
		mode				= "C";
		baseURI				= "";
		remote_ip			= "";
		date_format			= "'%Y-%m-%d'";
	}
	
	
	public String getRemote_ip() {
		return remote_ip;
	}


	public void setRemote_ip(String remote_ip) {
		this.remote_ip = remote_ip;
	}


	public String getS_type() {
		return s_type;
	}
	public void setS_type(String s_type) {
		this.s_type = s_type;
	}
	public String getS_text() {
		return s_text;
	}


	public void setS_text(String s_text) {
		this.s_text = s_text;
	}


	public String getOperation_yn() {
		return operation_yn;
	}
	public void setOperation_yn(String operation_yn) {
		this.operation_yn = operation_yn;
	}
	public String getBaseURI() {
		return baseURI;
	}
	public void setBaseURI(String baseURI) {
		this.baseURI = baseURI;
	}
	public String getExcelName() {
		return excelName;
	}
	public void setExcelName(String excelName) {
		this.excelName = excelName;
	}
	public String getReg_dt() {
		return reg_dt;
	}

	public void setReg_dt(String reg_dt) {
		this.reg_dt = reg_dt;
	}

	public String getReg_id() {
		return reg_id;
	}

	public void setReg_id(String reg_id) {
		this.reg_id = reg_id;
	}

	public String getMod_dt() {
		return mod_dt;
	}

	public void setMod_dt(String mod_dt) {
		this.mod_dt = mod_dt;
	}

	public String getMod_id() {
		return mod_id;
	}

	public void setMod_id(String mod_id) {
		this.mod_id = mod_id;
	}

	public String getMod_flag() {
		return mod_flag;
	}

	public void setMod_flag(String mod_flag) {
		this.mod_flag = mod_flag;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getQueryStr0() {
		return queryStr0;
	}

	public void setQueryStr0(String queryStr0) {
		this.queryStr0 = queryStr0;
	}

	public String getQueryStr1() {
		return queryStr1;
	}

	public void setQueryStr1(String queryStr1) {
		this.queryStr1 = queryStr1;
	}

	public String getQueryStr2() {
		return queryStr2;
	}

	public void setQueryStr2(String queryStr2) {
		this.queryStr2 = queryStr2;
	}
	
	public String getQueryStr3() {
		return queryStr3;
	}

	public void setQueryStr3(String queryStr3) {
		this.queryStr3 = queryStr3;
	}

	public String getOrderBy1() {
		return orderBy1;
	}

	public void setOrderBy1(String orderBy1) {
		this.orderBy1 = orderBy1;
	}

	public String getOrderBy2() {
		return orderBy2;
	}

	public void setOrderBy2(String orderBy2) {
		this.orderBy2 = orderBy2;
	}

	public String getSearchYear() {
		return searchYear;
	}	
	
	public void setSearchYear(String searchYear) {
		this.searchYear = searchYear;
	}
	
	public String getSearchMonth() {
		return searchMonth;
	}

	public void setSearchMonth(String searchMonth) {
		this.searchMonth = searchMonth;
	}
	
	public String getSearchDay() {
		return searchDay;
	}

	public void setSearchDay(String searchDay) {
		this.searchDay = searchDay;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getStart() {
		//return start + 1;				// Oracle, MSSQL
		return start;					// MySQL
	}

	public void setStart(int start) {
		//this.start = cpage * pageSize;	
		this.start = (cpage -1) * pageSize;
	}

	public int getEnd() {
		return end;
	}

	public void setEnd(int end) {
		this.end = start + pageSize ;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
		this.setStart(0);
	}

	public int getPageTotal() {
		return pageTotal;
	}

	public void setPageTotal(int pageTotal) {
		this.pageTotal = (total - 1) / pageSize;
	}

	public int getCpage() {
		return cpage;
	}

	public void setCpage(int cpage) {
		if (cpage < 1) {
			cpage = 1;
		}
		
		this.cpage = cpage;

		this.setStart( (cpage -1) * pageSize );
		this.setEnd(cpage);
	}

	public int getRowNum() {
		return rowNum;
	}

	public void setRowNum(int rowNum) {
		this.rowNum = total - (cpage) * pageSize;
	}
	
	public int getPageGrpSize() {
		return pageGrpSize;
	}

	public void setPageGrpSize(int pageGrpSize) {
		if ( pageGrpSize > 0) {
			this.pageGrpSize = pageGrpSize;
		}
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		if ( page == null ) {
			this.page = "0";
		}else{
			this.page = page;
		}
	}

	public String getPg() {
		return pg;
	}

	public void setPg(String pg) {
		this.pg = (pg.equals("")) ? "0" : pg;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate.replace("-", "").replace("/", "").replace("_", "");
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate.replace("-", "").replace("/", "").replace("_", "");
	}

	public String toDBStyleVariableName(String id) {
		StringBuilder buff = new StringBuilder();
		for (int i = 0; i < id.length(); i++) {
			char c = id.charAt(i);
			if (isUpperCase(c))
				buff.append("_");
			buff.append(c);
		}

		return buff.toString().toUpperCase();
	}
	
	private boolean isUpperCase(char c) {
		String _UPPERCASE_ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		int val = _UPPERCASE_ALPHABET.indexOf(c);
		return val > -1;
	}

	public String getPageId() {
		return pageId;
	}

	public String getAllwId() {
		return allwId;
	}

	public void setAllwId(String allwId) {
		this.allwId = allwId;
	}

	public void setPageId(String pageId) {
		this.pageId = pageId;
	}

	public String getPageAllw() {
		return pageAllw;
	}

	public void setPageAllw(String pageAllw) {
		this.pageAllw = pageAllw;
	}

	public String getPageAuth() {
		return pageAuth;
	}

	public void setPageAuth(String pageAuth) {
		this.pageAuth = pageAuth;
	}

	public String getSqlNameSpace() {
		return sqlNameSpace;
	}

	public void setSqlNameSpace(String sqlNameSpace) {
		this.sqlNameSpace = sqlNameSpace;
	}
}