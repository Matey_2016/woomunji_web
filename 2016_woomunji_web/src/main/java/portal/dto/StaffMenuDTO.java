package portal.dto;

@SuppressWarnings("serial")
public class StaffMenuDTO extends StaffDTO {
	private String menu_id;
	private String menu_nm;
	private String parent_cd;
	private String parent_cd_nm;
	private String menu_url;
	private String use_yn;
	private String board_id;
	
	
	public String getMenu_id() {
		return menu_id;
	}
	public void setMenu_id(String menu_id) {
		this.menu_id = menu_id;
	}
	public String getMenu_nm() {
		return menu_nm;
	}
	public void setMenu_nm(String menu_nm) {
		this.menu_nm = menu_nm;
	}
	public String getParent_cd() {
		return parent_cd;
	}
	public void setParent_cd(String parent_cd) {
		this.parent_cd = parent_cd;
	}
	public String getParent_cd_nm() {
		return parent_cd_nm;
	}
	public void setParent_cd_nm(String parent_cd_nm) {
		this.parent_cd_nm = parent_cd_nm;
	}
	public String getMenu_url() {
		return menu_url;
	}
	public void setMenu_url(String menu_url) {
		this.menu_url = menu_url;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public String getBoard_id() {
		return board_id;
	}
	public void setBoard_id(String board_id) {
		this.board_id = board_id;
	}
	
	
}
