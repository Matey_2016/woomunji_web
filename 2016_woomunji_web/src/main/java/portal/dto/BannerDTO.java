package portal.dto;

import portal.dto.BaseDTO;

@SuppressWarnings("serial")
public class BannerDTO extends BaseDTO{
	
	private String banner_id;        //배너아이디
	private String title;            //배너제목
	private String start_dt;         //게시시작일
	private String end_dt;           //게시종료일
	private String target;           //링크타겟(O:opener,B:blank)
	private String position;         //배너위치
	private String file_nm;          //이미지파일명
	private String system_file_nm;   //이미지시스템파일명
	private String file_path;        //파일경로
	private String link_url;         //링크url
	private String use_yn;           //사용여부
	private String img_yn;
	private String gubun;			  //상,하단 구분	
	private String summary;			  //요약글	
	
	
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getGubun() {
		return gubun;
	}
	public void setGubun(String gubun) {
		this.gubun = gubun;
	}
	public String getBanner_id() {
		return banner_id;
	}
	public void setBanner_id(String banner_id) {
		this.banner_id = banner_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getStart_dt() {
		return start_dt;
	}
	public void setStart_dt(String start_dt) {
		this.start_dt = start_dt;
	}
	public String getEnd_dt() {
		return end_dt;
	}
	public void setEnd_dt(String end_dt) {
		this.end_dt = end_dt;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public String getFile_nm() {
		return file_nm;
	}
	public void setFile_nm(String file_nm) {
		this.file_nm = file_nm;
	}
	public String getSystem_file_nm() {
		return system_file_nm;
	}
	public void setSystem_file_nm(String system_file_nm) {
		this.system_file_nm = system_file_nm;
	}
	public String getFile_path() {
		return file_path;
	}
	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}
	public String getLink_url() {
		return link_url;
	}
	public void setLink_url(String link_url) {
		this.link_url = link_url;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public String getImg_yn() {
		return img_yn;
	}
	public void setImg_yn(String img_yn) {
		this.img_yn = img_yn;
	}
	
}
