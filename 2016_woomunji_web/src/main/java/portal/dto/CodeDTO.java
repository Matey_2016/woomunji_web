/*------------------------------------------------------------------------------
 * NAME : CodeDTO.
 * DESC : 공통코드 관리를 위한 DTO.
 * VER  : v1.0
 * PROJ : Portal 공통
 * Copyright Matey All rights reserved
 *------------------------------------------------------------------------------
 *                  변         경         사         항
 *------------------------------------------------------------------------------
 *    DATE     AUTHOR                      DESCRIPTION
 *-----------  ------  ---------------------------------------------------------
 * 2016.03.21  정종선(메이티)   최초 프로그램 작성
 *------------------------------------------------------------------------------*/

package portal.dto;

public class CodeDTO extends BaseDTO{
	
	static final long serialVersionUID = 3290212334902619349L;
	
	private String code_id;
	private String high_id;
	private String code_nm;
	private String high_code_nm;
	private String code_desc;
	private String code_etc;
	private String use_yn;
	private String orderby;
	
	public String getOrderby() {
		return orderby;
	}
	public void setOrderby(String orderby) {
		this.orderby = orderby;
	}
	public String getHigh_code_nm() {
		return high_code_nm;
	}
	public void setHigh_code_nm(String high_code_nm) {
		this.high_code_nm = high_code_nm;
	}
	public String getCode_id() {
		return code_id;
	}
	public void setCode_id(String code_id) {
		this.code_id = code_id;
	}
	public String getHigh_id() {
		return high_id;
	}
	public void setHigh_id(String high_id) {
		this.high_id = high_id;
	}
	public String getCode_nm() {
		return code_nm;
	}
	public void setCode_nm(String code_nm) {
		this.code_nm = code_nm;
	}
	public String getCode_desc() {
		return code_desc;
	}
	public void setCode_desc(String code_desc) {
		this.code_desc = code_desc;
	}
	public String getCode_etc() {
		return code_etc;
	}
	public void setCode_etc(String code_etc) {
		this.code_etc = code_etc;
	}
	public String getUse_yn() {
		return use_yn;
	}
	public void setUse_yn(String use_yn) {
		this.use_yn = use_yn;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
