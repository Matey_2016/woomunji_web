package portal.dto;

import portal.dto.BaseDTO;

@SuppressWarnings("serial")
public class CultureDTO extends BaseDTO{
	
	
	//문화재 기본정보
	private String item_cd;          //종목코드      
	private String[] arrItem_cd;	 //종목코드 배열
	private String crlts_no;         //문화재지정번호   
	private String ctrd_cd;          //소재지코드     
	private String[] arrCtrd_cd;	 //소재지코드 배열
	private String age_cd;           //시대코드    
	private String age_nm;           //시대코드명   
	private String[] arrAge_cd;		 //시대코드 배열
	private String crlts_nm;         //문화재명칭     
	private String crlts_nm_chcrt;   //문화재명칭(한자) 
	private String crlts_no_nm;      //문화재지정번호명  
	private String crlts_dc;         //문화재설명     
	private String ctrd_nm;          //소재지명칭     
	private String item_nm;          //종목명칭      
	private String image_yn;         //이미지여부
	private String signgu_cd;        //소재지시군구코드  
	private String x_cnts;           //X좌표값    
	private String y_cnts;           //Y좌표값 
	private String view_cnt;         //조회수       
	private String post_yn;			 //게시여부
	                      
	//문화재 이미지 정보
	private String image_url;        //이미지경로   
	private String listimage_url;    //썸네일이미지경로
	private String signgu_nm;        //소재지시군구명칭
	private String reg_date;         //등록시간    
	private String upt_date;         //수정시간    
	                                 
	
	public String getPost_yn() {
		return post_yn;
	}
	public void setPost_yn(String post_yn) {
		this.post_yn = post_yn;
	}
	public String getAge_nm() {
		return age_nm;
	}
	public void setAge_nm(String age_nm) {
		this.age_nm = age_nm;
	}
	public String[] getArrItem_cd() {
		return arrItem_cd;
	}
	public void setArrItem_cd(String[] arrItem_cd) {
		this.arrItem_cd = arrItem_cd;
	}
	public String[] getArrCtrd_cd() {
		return arrCtrd_cd;
	}
	public void setArrCtrd_cd(String[] arrCtrd_cd) {
		this.arrCtrd_cd = arrCtrd_cd;
	}
	public String[] getArrAge_cd() {
		return arrAge_cd;
	}
	public void setArrAge_cd(String[] arrAge_cd) {
		this.arrAge_cd = arrAge_cd;
	}
	public String getItem_cd() {
		return item_cd;
	}
	public void setItem_cd(String item_cd) {
		this.item_cd = item_cd;
	}
	public String getCrlts_no() {
		return crlts_no;
	}
	public void setCrlts_no(String crlts_no) {
		this.crlts_no = crlts_no;
	}
	public String getCtrd_cd() {
		return ctrd_cd;
	}
	public void setCtrd_cd(String ctrd_cd) {
		this.ctrd_cd = ctrd_cd;
	}
	public String getAge_cd() {
		return age_cd;
	}
	public void setAge_cd(String age_cd) {
		this.age_cd = age_cd;
	}
	public String getCrlts_nm() {
		return crlts_nm;
	}
	public void setCrlts_nm(String crlts_nm) {
		this.crlts_nm = crlts_nm;
	}
	public String getCrlts_nm_chcrt() {
		return crlts_nm_chcrt;
	}
	public void setCrlts_nm_chcrt(String crlts_nm_chcrt) {
		this.crlts_nm_chcrt = crlts_nm_chcrt;
	}
	public String getCrlts_no_nm() {
		return crlts_no_nm;
	}
	public void setCrlts_no_nm(String crlts_no_nm) {
		this.crlts_no_nm = crlts_no_nm;
	}
	public String getCrlts_dc() {
		return crlts_dc;
	}
	public void setCrlts_dc(String crlts_dc) {
		this.crlts_dc = crlts_dc;
	}
	public String getCtrd_nm() {
		return ctrd_nm;
	}
	public void setCtrd_nm(String ctrd_nm) {
		this.ctrd_nm = ctrd_nm;
	}
	public String getItem_nm() {
		return item_nm;
	}
	public void setItem_nm(String item_nm) {
		this.item_nm = item_nm;
	}
	public String getImage_yn() {
		return image_yn;
	}
	public void setImage_yn(String image_yn) {
		this.image_yn = image_yn;
	}
	public String getSigngu_cd() {
		return signgu_cd;
	}
	public void setSigngu_cd(String signgu_cd) {
		this.signgu_cd = signgu_cd;
	}
	public String getX_cnts() {
		return x_cnts;
	}
	public void setX_cnts(String x_cnts) {
		this.x_cnts = x_cnts;
	}
	public String getY_cnts() {
		return y_cnts;
	}
	public void setY_cnts(String y_cnts) {
		this.y_cnts = y_cnts;
	}
	public String getView_cnt() {
		return view_cnt;
	}
	public void setView_cnt(String view_cnt) {
		this.view_cnt = view_cnt;
	}
	public String getImage_url() {
		return image_url;
	}
	public void setImage_url(String image_url) {
		this.image_url = image_url;
	}
	public String getListimage_url() {
		return listimage_url;
	}
	public void setListimage_url(String listimage_url) {
		this.listimage_url = listimage_url;
	}
	public String getSigngu_nm() {
		return signgu_nm;
	}
	public void setSigngu_nm(String signgu_nm) {
		this.signgu_nm = signgu_nm;
	}
	public String getReg_date() {
		return reg_date;
	}
	public void setReg_date(String reg_date) {
		this.reg_date = reg_date;
	}
	public String getUpt_date() {
		return upt_date;
	}
	public void setUpt_date(String upt_date) {
		this.upt_date = upt_date;
	}
	
	
	
}
