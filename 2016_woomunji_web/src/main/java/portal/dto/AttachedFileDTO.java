package portal.dto;
import java.io.Serializable;

@SuppressWarnings("serial")
public class AttachedFileDTO implements Serializable{
	
	static final long serialVersionUID = -7972145670651116477L;
	
	private String fileId;				// 파일아이디
	private String parentId;			// 게시물아이디
	private String parentId1;			// 게시물아이디1
	private String fileFlag;
	private long fileSize;				// 파일사이즈
	private String filePath;			// 저장폴더
	private String orgFileName;			// 파일명
	private String sysFileName;			// 시스템파일명
	private String fileType;			// 파일타입
	private String thumbImg;			// 썸네일 이미지
	private String[] delFileId;			// 삭제할 파일아이디
	
	
	
	public String getThumbImg() {
		return thumbImg;
	}
	public void setThumbImg(String thumbImg) {
		this.thumbImg = thumbImg;
	}
	public String getFileId() {
		return fileId;
	}
	public void setFileId(String fileId) {
		this.fileId = fileId;
	}
	public String getParentId() {
		return parentId;
	}
	public void setParentId(String parentId) {
		this.parentId = parentId;
	}	
	public String getParentId1() {
		return parentId1;
	}
	public void setParentId1(String parentId1) {
		this.parentId1 = parentId1;
	}
	public long getFileSize() {
		return fileSize;
	}
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	public String getOrgFileName() {
		return orgFileName;
	}
	public void setOrgFileName(String orgFileName) {
		this.orgFileName = orgFileName;
	}
	public String getSysFileName() {
		return sysFileName;
	}
	public void setSysFileName(String sysFileName) {
		this.sysFileName = sysFileName;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String[] getDelFileId() {
		return delFileId;
	}
	public void setDelFileId(String[] delFileId) {
		this.delFileId = delFileId;
	}
	public String getFileFlag() {
		return fileFlag;
	}
	public void setFileFlag(String fileFlag) {
		this.fileFlag = fileFlag;
	}
	
	
}
