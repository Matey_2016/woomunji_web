var gulp = require('gulp');

//sass
var sass = require('gulp-sass');
gulp.task('sass', function(){
	gulp.src('src/main/webapp/sass/**/*.scss')
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest('src/main/webapp/css/'));
});

//브라우저 싱크
var browserSync = require('browser-sync').create();
	gulp.task('bs', function(){
	browserSync.init({
		//proxy: "localhost"
		server: {
			baseDir: "./src/main/webapp/"
		}
	});
	gulp.watch("src/main/webapp/WEB-INF/jsp/html/**/*.html").on('change', browserSync.reload);
	gulp.watch("src/main/webapp/css/*.css").on('change', browserSync.reload);
	gulp.watch('src/main/webapp/sass/**/*.scss', ['sass']);
	gulp.watch("src/main/webapp/js/**/*.js").on('change', browserSync.reload);
	gulp.watch('src/main/webapp/images/sprite/*.png',['sprite']);
});

//이미지 스프라이트
var spritesmith = require('gulp.spritesmith');
gulp.task('sprite',function(){
	var spriteData = gulp.src('src/main/webapp/images/sprite/*.*')
		.pipe(spritesmith({
			imgName:'../images/sprite.png',
			cssName:'_sprite.scss',
			padding:10,
			cssTemplate: 'sprite.css.handlebars'
		}))
	spriteData.img.pipe(gulp.dest('src/main/webapp/images/'));
	spriteData.css.pipe(gulp.dest('src/main/webapp/sass/common/'));
})